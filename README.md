# Project Title

KINA Distributions

## Getting Started

This system requires VirtualBox and Vagrant to function properly. Once you have those components you can provision the box and WordPress will be fully installed and ready to go.

### Prerequisites

#### Windows

If you are on Windows and do not have VirtualBox and/or Vagrant, you can use Chocolatey to install them. Simply run the following from the node console (in Administrator Mode).

```
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%chocolateybin

choco install virtualbox

choco install vagrant
```

#### Mac or Linux

[Still have to write out steps for this]

## Domain Name

Be sure to add dev.kinadistributions.com to your host file as 192.168.50.5.

```
192.168.50.5	dev.kinadistributions.com
```

## Running the CMS

Once you have the prerequisites installed and DNS setup, clone the repo. If you don't have access to the repo request it from the main contact for this project. Everything works from the root of the repository.

Once the repo is cloned properly, provision the box with vagrant by running this in the node console:

```
vagrant up
```

This will take several minutes to provision, but once it's complete open a browser to the [MUFG CMS](http://cms.mufg.com/wp-admin) and login with:

```
Username: admin
Password: admin
```

