
# This is where the variables for the rest of the installation are created
USER=www-data
DBPREFIX=jq9vv_
DBROOTPASSWORD=
DBNAME=KINA_WP
DBUSER=KINA_WP_USER
DBPASSWORD=Jq9vV_3R3n6N6e
SITENAME=CMS
URL=dev.kinadistributions.com


echo "********************************************************"
echo "Installing PHP and disabling path info default settings..."
echo "********************************************************"
sudo apt-get -y --show-progress install unzip zip nginx php7.2 php7.2-mysql php7.2-fpm php7.2-mbstring php7.2-xml php7.2-curl > /dev/null
echo 'installed nginx and dependencies'

sudo cp /etc/php/7.2/fpm/php.ini /etc/php/7.2/fpm/php.ini.orig
sudo sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.2/fpm/php.ini
sudo sed -i 's/memory_limit = 128M/memory_limit=512M/' /etc/php/7.2/fpm/php.ini
sudo service php7.2-fpm restart

# Remove the default nginx config from sites-enabled
sudo rm /etc/nginx/sites-enabled/default

echo "*********************************"
echo "Setting up NGinx for Wordpress..."
echo "*********************************"
sudo cp /vagrant/provision/nginx/common /etc/nginx -r
sudo cp /vagrant/provision/nginx/wptemplate.nginx /etc/nginx/sites-available/$SITENAME
sed -i "s/SITENAME/$SITENAME/g" /etc/nginx/sites-available/$SITENAME
sed -i "s/SITEURL/$URL/g" /etc/nginx/sites-available/$SITENAME
sudo ln -s /etc/nginx/sites-available/$SITENAME /etc/nginx/sites-enabled/
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.orig
sudo cp /vagrant/provision/nginx/nginx.conf /etc/nginx/
sudo service nginx reload
sudo service php7.2-fpm restart
