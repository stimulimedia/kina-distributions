#!/usr/bin/env bash

# This is where the variables for the rest of the installation are created
USER=www-data
DBPREFIX=jq9vv_
DBROOTPASSWORD=
DBNAME=KINA_WP
DBUSER=KINA_WP_USER
DBPASSWORD=Jq9vV_3R3n6N6e
SITENAME=CMS
URL=dev.kinadistributions.com


echo "********************************"
echo "Installing and securing MariaDB..."
echo "********************************"

echo "mysql-server mysql-server/root_password password $DBROOTPASSWORD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBROOTPASSWORD" | debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y mariadb-server > /dev/null
echo "** MariaDB Server installed"
sudo mysql_install_db --no-defaults --force --verbose
echo "** MySql installed"
sudo mysqld --general-log
echo "** mysqld log created"


# Emulate results of mysql_secure_installation, without using 'expect' to handle input
echo "************************************"
echo "Setting up database permissions..."
echo "************************************"
mysql --user=root --password=$DBROOTPASSWORD -e "UPDATE mysql.user SET Password=PASSWORD('$DBROOTPASSWORD') WHERE User='root'"
mysql --user=root --password=$DBROOTPASSWORD -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
mysql --user=root --password=$DBROOTPASSWORD -e "DELETE FROM mysql.user WHERE User=''"
mysql --user=root --password=$DBROOTPASSWORD -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
mysql --user=root --password=$DBROOTPASSWORD -e "FLUSH PRIVILEGES"


echo "************************************"
echo "Setting up database for Wordpress..."
echo "************************************"
mysql --user=root --password=$DBROOTPASSWORD -e "CREATE DATABASE $DBNAME"
echo "** Created database"
mysql --user=root --password=$DBROOTPASSWORD -e "CREATE USER $DBUSER@localhost IDENTIFIED BY '$DBPASSWORD'"
echo "** Created user"
mysql --user=root --password=$DBROOTPASSWORD -e "GRANT ALL PRIVILEGES ON $DBNAME.* TO $DBUSER@localhost"
echo "** Granted permissions"
mysql --user=root --password=$DBROOTPASSWORD -e "FLUSH PRIVILEGES"
echo "** Flushed privileges"
