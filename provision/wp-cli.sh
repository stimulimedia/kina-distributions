#!/usr/bin/env bash

# This is where the variables for the rest of the installation are created
USER=www-data
DBPREFIX=jq9vv_
DBROOTPASSWORD=
DBNAME=KINA_WP
DBUSER=KINA_WP_USER
DBPASSWORD=Jq9vV_3R3n6N6e
SITENAME=CMS
URL=dev.kinadistributions.com

curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
php wp-cli.phar --info
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

sudo wp core install --url=$URL --title="KINA Distributions" --admin_user=admin --admin_password=admin --admin_email=admin@kinadistributions.com --skip-email --path=/var/www/$SITENAME --allow-root

sudo wp theme activate khkplrv-child --path=/var/www/$SITENAME --allow-root

sudo wp plugin activate akismet --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate codestar-framework --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate khkplrv-core --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate contact-form-7 --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate kingcomposer --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate mailchimp-for-wp --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate one-click-demo-import --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate woocommerce --path=/var/www/$SITENAME --allow-root
sudo wp plugin activate woo-product-gallery-slider --path=/var/www/$SITENAME --allow-root

#sudo wp plugin activate woocommerce-services --path=/var/www/$SITENAME --allow-root
#sudo wp plugin activate jetpack --path=/var/www/$SITENAME --allow-root
#sudo wp plugin activate facebook-for-woocommerce --path=/var/www/$SITENAME --allow-root
#sudo wp plugin activate mailchimp-for-woocommerce --path=/var/www/$SITENAME --allow-root

sudo wp plugin activate advanced-custom-fields-pro --path=/var/www/$SITENAME --allow-root
sudo wp eval 'acf_pro_update_license("b3JkZXJfaWQ9NjgxODR8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE1LTExLTA1IDE3OjQzOjAy");' --path=/var/www/$SITENAME --allow-root

sudo wp rewrite structure '/%postname%/' --path=/var/www/$SITENAME --allow-root
sudo wp rewrite flush --path=/var/www/$SITENAME --allow-root

sudo wp post create --post_type=page --post_title='Lead Tester' --post_content='[kina_lead_form]' --path=/var/www/$SITENAME --allow-root
