#!/bin/bash

# This is where the variables for the rest of the installation are created
USER=www-data
DBPREFIX=jq9vv_
DBROOTPASSWORD=
DBNAME=KINA_WP
DBUSER=KINA_WP_USER
DBPASSWORD=Jq9vV_3R3n6N6e
SITENAME=CMS
URL=dev.kinadistributions.com

echo "*******************************" 
echo "Provisioning virtual machine..."
echo "*******************************" 


echo "***********************"
echo "Updating apt sources..."
echo "***********************"
export DEBIAN_FRONTEND=noninteractive

sudo apt-get update -y > /dev/null
#sudo apt-get -y upgrade

sudo apt-get install -y software-properties-common python-software-properties
sudo apt-get update -y > /dev/null
echo "** Package software-properties-common installed"

sudo add-apt-repository -y ppa:ondrej/php
sudo add-apt-repository -y ppa:nginx/stable

sudo apt-get update -y > /dev/null
echo "** PPA repositories added"

