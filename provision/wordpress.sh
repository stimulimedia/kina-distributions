
# This is where the variables for the rest of the installation are created
USER=www-data
DBPREFIX=jq9vv_
DBROOTPASSWORD=
DBNAME=KINA_WP
DBUSER=KINA_WP_USER
DBPASSWORD=Jq9vV_3R3n6N6e
SITENAME=CMS
URL=dev.kinadistributions.com

SITEPATH=/var/www/$SITENAME
sudo rm -r /var/www/html*

echo "********************************************************"
echo "Downloading and installing Wordpress and dependencies..."
echo "********************************************************"

# Download and extract Wordpress, and delete archive
wget http://wordpress.org/latest.tar.gz -q -P /tmp
tar xzfC /tmp/latest.tar.gz /tmp
rm /tmp/latest.tar.gz


echo "***********************************************************"
echo "Setting up Wordpress configurations and file permissions..."
echo "***********************************************************"
# Setup Wordpress config file with database info
cp /tmp/wordpress/wp-config-sample.php  /tmp/wordpress/wp-config.php
sed -i "s/database_name_here/$DBNAME/"  /tmp/wordpress/wp-config.php
sed -i "s/username_here/$DBUSER/"       /tmp/wordpress/wp-config.php
sed -i "s/password_here/$DBPASSWORD/"   /tmp/wordpress/wp-config.php
sed -i "s/wp_/$DBPREFIX/"               /tmp/wordpress/wp-config.php
sed -i "s/define( 'WP_DEBUG', false );/define('WP_DEBUG', true);\l\ndefine('WP_MEMORY_LIMIT', '512M');/"	/tmp/wordpress/wp-config.php

# Add authentication salts from the Wordpress API
SALT=$(curl -L https://api.wordpress.org/secret-key/1.1/salt/)  
STRING='put your unique phrase here'  
printf '%s\n' "g/$STRING/d" a "$SALT" . w | ed -s /tmp/wordpress/wp-config.php

# Move Wordpress to appropriate directory and set file permissions
#sudo mkdir -p $SITEPATH
sudo rsync -aqP /tmp/wordpress/ $SITEPATH
sudo mkdir $SITEPATH/wp-content/uploads
sudo chown -R $USER:www-data $SITEPATH/*
rm -r /tmp/wordpress

# Setup file and directory permissions on Wordpress
sudo find $SITEPATH -type d -exec chmod 0777 {} \;
sudo find $SITEPATH -type f -exec chmod 0777 {} \;
sudo chmod 0777 $SITEPATH/wp-config.php


echo "**************************************************************"
echo "Success! Navigate to your website's URL to finish the setup..."
echo "**************************************************************"
