<?php
  // Metabox
  $khkplrv_id    = ( isset( $post ) ) ? $post->ID : 0;
  $khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
  $khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
  $khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $khkplrv_id : false;
  $khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
  $khkplrv_cart_widget  = cs_get_option( 'khkplrv_cart_widget' );

  // Header Style
  if ( $khkplrv_meta ) {
    $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
    $header_middle  = $khkplrv_meta['header_middle'];
    $khkplrv_sticky_header = isset( $khkplrv_meta['sticky_header'] ) ? $khkplrv_meta['sticky_header'] : '' ;
  } else {
    $khkplrv_header_design  = cs_get_option( 'select_header_design' );
    $khkplrv_sticky_header  = cs_get_option( 'sticky_header' );
    $header_middle  = cs_get_option( 'header_middle' );
  }

  if ( $khkplrv_header_design === 'default' ) {
    $khkplrv_header_design_actual  = cs_get_option( 'select_header_design' );
  } else {
    $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
  }
  $khkplrv_header_design_actual = $khkplrv_header_design_actual ? $khkplrv_header_design_actual : 'style_one';

  if ( $khkplrv_meta && $khkplrv_header_design !== 'default') {
    $header_middle  = $khkplrv_meta['header_middle'];
  } else {
    $header_middle  = cs_get_option( 'header_middle' );
  }

  if ( $khkplrv_header_design_actual == 'style_one' ||  $khkplrv_header_design_actual == 'style_three' ) {
    $nav_right = ' navbar-right ';
  } else {
    $nav_right = '';
  }

  if ($khkplrv_meta) {
    $khkplrv_choose_menu = $khkplrv_meta['choose_menu'];
  } else { $khkplrv_choose_menu = ''; }
  $khkplrv_choose_menu = $khkplrv_choose_menu ? $khkplrv_choose_menu : '';

  if ( !$khkplrv_cart_widget ) {
   $cart_class = ' no-header-cart ';
  } else {
  $cart_class = ' has-header-cart ';
  }
?>

<!-- Navigation & Search -->
<div class="container">
  <div class="navbar-header">
    <button type="button" class="open-btn">
      <span class="sr-only"><?php echo esc_html__( 'Toggle navigation','khkplrv' ) ?></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <?php if ( $khkplrv_header_design_actual == 'style_one' || $khkplrv_header_design_actual == 'style_three' ) {
        get_template_part( 'theme-layouts/header/logo' );
    } ?>
  </div>
  <div id="navbar" class="navbar-collapse collapse navigation-holder <?php echo esc_attr( $nav_right.$cart_class ); ?>">
    <button class="close-navbar"><i class="ti-close"></i></button>

    <?php
    wp_nav_menu(
      array(
        'menu'              => 'primary',
        'theme_location'    => 'primary',
        'container'         => '',
        'container_class'   => '',
        'container_id'      => '',
        'menu'              => $khkplrv_choose_menu,
        'menu_class'        => 'nav navbar-nav',
        'fallback_cb'       => '__return_false',
      )
    );
    ?>
  </div><!-- end of nav-collapse -->
</div><!-- end of container -->
