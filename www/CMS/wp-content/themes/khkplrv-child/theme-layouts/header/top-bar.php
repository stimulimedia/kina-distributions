<?php
// Metabox
global $post;
$khkplrv_id    = ( isset( $post ) ) ? $post->ID : false;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
$khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $khkplrv_id : false;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
  if ($khkplrv_meta) {
    $khkplrv_topbar_options = $khkplrv_meta['topbar_options'];
  } else {
    $khkplrv_topbar_options = '';
  }

// Define Theme Options and Metabox varials in right way!
if ($khkplrv_meta) {
  if ($khkplrv_topbar_options === 'custom' && $khkplrv_topbar_options !== 'default') {
    $khkplrv_hide_topbar        = $khkplrv_topbar_options;
    $khkplrv_topbar_bg          = $khkplrv_meta['topbar_bg'];
    if ($khkplrv_topbar_bg) {
      $khkplrv_topbar_bg = 'background-color: '. $khkplrv_topbar_bg .';';
    } else {$khkplrv_topbar_bg = '';}
  } else {
    $khkplrv_hide_topbar        = cs_get_option('top_bar');
    $khkplrv_topbar_bg          = '';
  }
} else {
  // Theme Options fields
  $khkplrv_hide_topbar        = cs_get_option('top_bar');
  $khkplrv_topbar_bg          = '';
}

if ( $khkplrv_meta ) {
  $khkplrv_topbar_bg = ( $khkplrv_topbar_bg ) ? $khkplrv_meta['topbar_bg'] : '';
} else {
  $khkplrv_topbar_bg = '';
}
if ( $khkplrv_topbar_bg ) {
  $khkplrv_topbar_bg = 'background-color: '. $khkplrv_topbar_bg .';';
} else { $khkplrv_topbar_bg = ''; }

?>
 <!-- header top area start -->
  <div class="topbar" style="<?php echo esc_attr( $khkplrv_topbar_bg ); ?>">
      <?php get_template_part( 'theme-layouts/header/lower-topbar' ); ?>
  </div>
<!-- header top area end -->
<?php // Hide Topbar - From Metabox