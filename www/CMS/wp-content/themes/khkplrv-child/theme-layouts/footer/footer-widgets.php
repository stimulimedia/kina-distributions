<?php if( khkplrv_footer_widgets_override() ) {
?>
<!-- Footer Widgets -->
<div class="upper-footer">
	<div class="footer-widget-area-kina">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-lg-3">
					<?php if (is_active_sidebar( 'footer-1' )) : ?>
						<?php dynamic_sidebar( 'footer-1' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-9 col-lg-9">
					<?php if (is_active_sidebar( 'footer-2' )) : ?>
						<?php dynamic_sidebar( 'footer-2' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer Widgets -->
<?php
}