<?php
add_action( 'wp_enqueue_scripts', 'khkplrv_enqueue_styles' );
function khkplrv_enqueue_styles() {
	$parent_style = 'khkplrv-style';
  wp_enqueue_style( $parent_style, get_template_directory_uri() . '/assets/css/styles.css', array('themify-icons', 'flaticon', 'bootstrap', 'animate','owl-carousel', 'owl-theme', 'slick','slick-theme','owl-transitions','fancybox','odometer','perfect-scrollbar' ) );
  wp_enqueue_style( 'khkplrv-child',
      get_stylesheet_directory_uri() . '/style.css',
      array( $parent_style ),
      wp_get_theme()->get('Version')
    );
}
if( ! function_exists( 'khkplrv_child_theme_language_setup' ) ) {
	function khkplrv_child_theme_language_setup(){
	  load_theme_textdomain( 'khkplrv-child', get_template_directory() . '/languages' );
	}
	add_action('after_setup_theme', 'khkplrv_child_theme_language_setup');
}




/* Override this function in order to fix the HTML specifically for what I need */
function khkplrv_footer_widgets_override() {

  $output = '';

  $classes = array(
    'col-lg-3 col-md-3',
    'col-lg-9 col-md-9'
  );

  for( $i = 0; $i < 2; $i++ ) {
    $index = $i + 1;
    if (is_active_sidebar( 'footer-'. $index )) {
      $output .= '<div class="TEST ' .$classes[$i]. '">';
        ob_start();
          dynamic_sidebar( 'footer-'. $index );
        $output .= ob_get_clean();
      $output .= '</div>';
    }

  }

  return $output;

}
