<?php
/* This is the original header. I need a temporary right now, but I don't want to get rid of this.
<header class="banner">
	<div class="container">
		<a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
		<nav class="nav-primary">
			<?php
			if (has_nav_menu('primary_navigation')) :
				wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
			endif;
			?>
		</nav>
	</div>
</header>
*/
?>

<header class="main-header">
	<div class="main-header__container site-wrapper">
		<div class="main-header__brand--wrapper">
			<a class="main-header__brand" href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
		</div>
		<nav class="main-header__social">
			<div class="main-header__social--item facebook">
				<a class="main-header__social--item-link" href="https://www.facebook.com/kinadistributions/" target="_blank">Facebook</a>
			</div>
			<div class="main-header__social--item instagram">
				<a class="main-header__social--item-link" href="https://www.instagram.com/kinadistributions/" target="_blank">Instagram</a>
			</div>
			<div class="main-header__social--item twitter">
				<a class="main-header__social--item-link" href="https://twitter.com/kinadist" target="_blank">Twitter</a>
			</div>
			<div class="main-header__social--item linkedin">
				<a class="main-header__social--item-link" href="https://www.linkedin.com/company/kinadistributions/" target="_blank">LinkedIn</a>
			</div>
		</nav>
	</div>
</header>