<footer class="main-footer">
	<div class="main-footer__container">
		<p class="main-footer__copyright">©Copyright <?php echo date('Y'); ?>. KINA Distributions. All rights reserved.</p>

		<div class="main-footer__language">
			<?php echo do_shortcode('[wpml_language_switcher type="footer" link_current=0 flags=1 native=1 translated=0][/wpml_language_switcher]'); ?>
		</div>
	</div>
</footer>
