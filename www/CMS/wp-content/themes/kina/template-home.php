<?php
/**
 * Template Name: Temporary Home Page
 */
?>

<?php
while (have_posts()) : the_post();
?>
	<?php
	$meta = get_post_meta($post->ID);
	$modules = get_field('sections');

	for($i = 0; $i < count($modules); $i++) :
		$layout = (gettype ($modules[$i]) === 'string') ? $modules[$i] : $modules[$i]['acf_fc_layout'];
		include(locate_template('modules/' . $layout . '.php'));
	endfor;
	?>
<?php
endwhile;
?>
