<?php if ($meta['sections_'.$i.'_items']) : ?>
<section class="kina-benefits">
	<div class="kina-benefits__wrapper site-wrapper">
		<div class="kina-benefits__row">
			<?php for ($ii = 0; $ii < intval($meta['sections_'.$i.'_items'][0]); $ii++) : ?>
				<div class="kina-benefits__col">
					<h3 class="kina-benefits__title"><?php echo $meta['sections_'.$i.'_items_'.$ii.'_title'][0]; ?></h3>
					<p class="kina-benefits__body"><?php echo $meta['sections_'.$i.'_items_'.$ii.'_body'][0]; ?></p>
				</div>
			<?php endfor; ?>
		</div>
	</div>
</section>
<?php endif; ?>