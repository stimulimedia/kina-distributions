<section class="kina-video-content">
	<div class="kina-video-content__wrapper site-wrapper">
		<div class="kina-video-content__section content">
			<header class="kina-video-content__header">
				<?php if ($meta['sections_'.$i.'_title']) : ?>
					<h2 class="kina-video-content__title"><?php echo $meta['sections_'.$i.'_title'][0]; ?></h2>
				<?php endif; ?>
				<?php if ($meta['sections_'.$i.'_subtitle']) : ?>
					<h3 class="kina-video-content__subtitle"><?php echo $meta['sections_'.$i.'_subtitle'][0]; ?></h3>
				<?php endif; ?>
			</header>

			<?php if ($meta['sections_'.$i.'_body']): ?>
				<div class="kina-video-content__body">
					<?php echo $meta['sections_'.$i.'_body'][0]; ?>
				</div>
			<?php endif; ?>
		</div>
		
		<?php if ($meta['sections_'.$i.'_youtube_link']) : ?>
		<div class="kina-video-content__section video">
			<div class="kina-video-content__video">
				<iframe class="youtube" src="<?php echo $meta['sections_'.$i.'_youtube_link'][0]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>

			<?php if ($meta['sections_'.$i.'_video_title']) : ?>
				<header class="kina-video-content__video--header">
					<h3 class="kina-video-content__video--title"><?php echo $meta['sections_'.$i.'_video_title'][0]; ?></h3>
				</header>
			<?php endif; ?>

			<?php if ($meta['sections_'.$i.'_video_description']) : ?>
				<div class="kina-video-content__description">
					<?php echo $meta['sections_'.$i.'_video_description'][0]; ?>
				</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>
</section>