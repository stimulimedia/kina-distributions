<?php
$image_id = $meta['sections_'.$i.'_image'][0];

if ($image_id) {
	$image = wp_get_attachment_metadata($image_id);
	$image = wp_upload_dir()['baseurl']. '/' .$image['file'];
}
?>

<section id="contact-divider" class="kina-contact-divider" style="background-image: url(<?php echo $image; ?>);">
	<div class="kina-contact-divider__wrapper site-wrapper">
		<header class="kina-contact-divider__header">
			<h2 class="kina-contact-divider__title"><?php echo $meta['sections_'.$i.'_headline'][0]; ?></h2>
		</header>
		<div class="kina-contact-divider__content">
			<?php echo do_shortcode('[instagram-feed]'); ?>
		</div>
	</div>
</section>