<section id="kina-funnel-results" class="kina-funnel-results">
	<div class="kina-funnel-results__wrapper site-wrapper">
		<header class="kina-funnel-results__header">
			<?php if ($meta['sections_'.$i.'_title'][0]) : ?>
				<h1 class="kina-funnel-results__title"><?php echo $meta['sections_'.$i.'_title'][0]; ?></h1>
			<?php endif; ?>
			<?php if ($meta['sections_'.$i.'_subtitle'][0]) : ?>
				<h2 class="kina-funnel-results__subtitle"><?php echo $meta['sections_'.$i.'_subtitle'][0]; ?></h2>
			<?php endif; ?>
		</header>
		<div class="kina-funnel-results__container">
			<div class="kina-funnel-results__recent result-set">
				<header class="kina-funnel-results__recent--header result-set--header">
					<h3 class="kina-funnel-results__recent--title result-set--title"><?php echo $meta['sections_'.$i.'_recent_title'][0]; ?></h3>
				</header>

				<?php
				$limit = ($meta['sections_'.$i.'_recent_limit']) ? $meta['sections_'.$i.'_recent_limit'][0] : 5;
				$recent = kina_get_funnel_results_recent($limit);
				?>

				<?php if (!$recent || count($recent) == 0) : ?>
					<p class="kina-funnel-results__none">No submissions at this time</p>
				<?php else : ?>
					<?php foreach ($recent as $item) : ?>
						<?php if ($item['ProjectSavings'] && $item['ProjectSavings'][0] > 0) : ?>
							<?php
							$prepend = (ICL_LANGUAGE_CODE === 'en') ? '$' : '';
							$append = (ICL_LANGUAGE_CODE === 'es') ? 'MX' : '';
							?>
							<div class="kina-funnel-results__item">
								<h4 class="kina-funnel-results__item--title">
									<?php echo $prepend;?>
									<?php echo number_format($item['ProjectSavings'][0], 2); ?>
									<?php echo $append;?>
								</h4>
								<p class="kina-funnel-results__item--body">
									<strong><?php echo $item['FirstName'][0] . ' ' .$item['LastName'][0]?></strong>
									from
									<em><?php echo $item['CompanyName'][0]?></em>
								</p>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="kina-funnel-results__winners result-set">
				<header class="kina-funnel-results__winners--header result-set--header">
					<h3 class="kina-funnel-results__winners--title result-set--title"><?php echo $meta['sections_'.$i.'_winners_title'][0]; ?></h3>
				</header>

				<?php
				$limit = ($meta['sections_'.$i.'_winners_limit']) ? $meta['sections_'.$i.'_winners_limit'][0] : 5;
				$winners = kina_get_funnel_results_winners($limit);
				?>

				<?php if (!$winners || count($winners) == 0) : ?>
					<p class="kina-funnel-results__none">No submissions at this time</p>
				<?php else : ?>
					<?php foreach ($winners as $item) : ?>
						<?php if ($item['ProjectSavings'] && $item['ProjectSavings'] > 0) : ?>
							<?php
							$prepend = (ICL_LANGUAGE_CODE === 'en') ? '$' : '';
							$append = (ICL_LANGUAGE_CODE === 'es') ? 'MX' : '';
							?>
							<div class="kina-funnel-results__item">
								<h4 class="kina-funnel-results__item--title">
									<?php echo $prepend;?>
									<?php echo number_format($item['ProjectSavings'][0], 2); ?>
									<?php echo $append;?>
								</h4>
								<p class="kina-funnel-results__item--body">
									<strong><?php echo $item['FirstName'][0] . ' ' .$item['LastName'][0]?></strong>
									from
									<em><?php echo $item['CompanyName'][0]?></em>
								</p>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<script>
setTimeout(function() {
	console.log('refreshing');
	location.reload();
}, 1000 * 30);
</script>