<?php
$image_id = $meta['sections_'.$i.'_image'][0];

if ($image_id) {
	$image = wp_get_attachment_metadata($image_id);
	$image = wp_upload_dir()['baseurl']. '/' .$image['file'];
}
?>

<section id="kina-hero" class="kina-hero">
	<header class="kina-hero__header">
		<?php if ($meta['sections_'.$i.'_title'][0]) : ?>
			<h1 class="kina-hero__title"><?php echo $meta['sections_'.$i.'_title'][0]; ?></h1>
		<?php endif; ?>

		<?php if ($meta['sections_'.$i.'_subtitle'][0]) : ?>
			<h2 class="kina-hero__subtitle"><?php echo $meta['sections_'.$i.'_subtitle'][0]; ?></h2>
		<?php endif; ?>
	</header>
	<?php if ($image) : ?>
		<figure class="kina-hero__figure">
			<img class="kina-hero__image" src="<?php echo $image; ?>" alt="<?php echo $meta['sections_'.$i.'_title'][0]; ?>" />
		</figure>
	<?php endif; ?>
</section>