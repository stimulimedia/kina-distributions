<?php
/*
 * Khkplrv Theme's Functions
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */

/**
 * Define - Folder Paths
 */

define( 'KHKPLRV_THEMEROOT_URI', get_template_directory_uri() );
define( 'KHKPLRV_CSS', KHKPLRV_THEMEROOT_URI . '/assets/css' );
define( 'KHKPLRV_IMAGES', KHKPLRV_THEMEROOT_URI . '/assets/images' );
define( 'KHKPLRV_SCRIPTS', KHKPLRV_THEMEROOT_URI . '/assets/js' );
define( 'KHKPLRV_FRAMEWORK', get_template_directory() . '/includes' );
define( 'KHKPLRV_LAYOUT', get_template_directory() . '/theme-layouts' );
define( 'KHKPLRV_CS_IMAGES', KHKPLRV_THEMEROOT_URI . '/includes/theme-options/framework-extend/images' );
define( 'KHKPLRV_CS_FRAMEWORK', get_template_directory() . '/includes/theme-options/framework-extend' ); // Called in Icons field *.json
define( 'KHKPLRV_ADMIN_PATH', get_template_directory() . '/includes/theme-options/cs-framework' ); // Called in Icons field *.json

/**
 * Define - Global Theme Info's
 */
if (is_child_theme()) { // If Child Theme Active
	$khkplrv_theme_child = wp_get_theme();
	$khkplrv_get_parent = $khkplrv_theme_child->Template;
	$khkplrv_theme = wp_get_theme($khkplrv_get_parent);
} else { // Parent Theme Active
	$khkplrv_theme = wp_get_theme();
}
define('KHKPLRV_NAME', $khkplrv_theme->get( 'Name' ), true);
define('KHKPLRV_VERSION', $khkplrv_theme->get( 'Version' ), true);
define('KHKPLRV_BRAND_URL', $khkplrv_theme->get( 'AuthorURI' ), true);
define('KHKPLRV_BRAND_NAME', $khkplrv_theme->get( 'Author' ), true);

/**
 * All Main Files Include
 */
require_once( KHKPLRV_FRAMEWORK . '/init.php' );