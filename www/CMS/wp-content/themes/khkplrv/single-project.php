<?php
/*
 * The template for displaying all single posts.
 * Author & Copyright: irstheme
 * URL: http://themeforest.net/user/irstheme
 */
get_header();
	// Metabox
	$khkplrv_id    = ( isset( $post ) ) ? $post->ID : 0;
	$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
	$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
	$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
	if ( $khkplrv_meta ) {
		$khkplrv_content_padding = $khkplrv_meta['content_spacings'];
	} else { $khkplrv_content_padding = ''; }
	// Padding - Metabox
	if ( $khkplrv_content_padding && $khkplrv_content_padding !== 'padding-default' ) {
		$khkplrv_content_top_spacings = $khkplrv_meta['content_top_spacings'];
		$khkplrv_content_bottom_spacings = $khkplrv_meta['content_bottom_spacings'];
		if ( $khkplrv_content_padding === 'padding-custom' ) {
			$khkplrv_content_top_spacings = $khkplrv_content_top_spacings ? 'padding-top:'. khkplrv_check_px($khkplrv_content_top_spacings) .';' : '';
			$khkplrv_content_bottom_spacings = $khkplrv_content_bottom_spacings ? 'padding-bottom:'. khkplrv_check_px($khkplrv_content_bottom_spacings) .';' : '';
			$khkplrv_custom_padding = $khkplrv_content_top_spacings . $khkplrv_content_bottom_spacings;
		} else {
			$khkplrv_custom_padding = '';
		}
	} else {
		$khkplrv_custom_padding = '';
	}
	// Theme Options
	$khkplrv_sidebar_position = cs_get_option( 'project_sidebar_position' );
	$khkplrv_single_comment = cs_get_option( 'project_comment_form' );
	$khkplrv_sidebar_position = $khkplrv_sidebar_position ? $khkplrv_sidebar_position : 'sidebar-hide';
	// Sidebar Position
	if ( $khkplrv_sidebar_position === 'sidebar-hide' ) {
		$khkplrv_layout_class = 'col-md-12';
		$khkplrv_sidebar_class = 'hide-sidebar';
	} elseif ( $khkplrv_sidebar_position === 'sidebar-left' ) {
		$khkplrv_layout_class = 'col-md-8 col-md-push-4';
		$khkplrv_sidebar_class = 'left-sidebar';
	} else {
		$khkplrv_layout_class = 'col-md-8';
		$khkplrv_sidebar_class = 'right-sidebar';
	} ?>
<div class="project-single-section section-padding <?php echo esc_attr( $khkplrv_content_padding .' '. $khkplrv_sidebar_class ); ?>" style="<?php echo esc_attr( $khkplrv_custom_padding ); ?>">
	<div class="container content-area ">
		<div class="row">
			<div class="<?php echo esc_attr( $khkplrv_layout_class ); ?>">
				<div class="project-single-content">
					<?php
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							if ( post_password_required() ) {
									echo '<div class="password-form">'.get_the_password_form().'</div>';
								} else {
									get_template_part( 'theme-layouts/post/project', 'content' );
									$khkplrv_single_comment = !$khkplrv_single_comment ? comments_template() : '';

								}
						endwhile;
					else :
						get_template_part( 'theme-layouts/post/content', 'none' );
					endif; ?>
				</div><!-- Blog Div -->
				<?php
		    khkplrv_paging_nav();
		    wp_reset_postdata(); ?>
			</div><!-- Content Area -->
				<?php
				if ( $khkplrv_sidebar_position !== 'sidebar-hide' ) {
					get_sidebar(); // Sidebar
				} ?>
		</div>
	</div>
</div>
<?php
get_footer();