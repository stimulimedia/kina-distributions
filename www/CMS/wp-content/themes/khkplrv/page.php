<?php
/*
 * The template for displaying all pages.
 * Author & Copyright: irstheme
 * URL: http://themeforest.net/user/irstheme
 */
$khkplrv_id    = ( isset( $post ) ) ? $post->ID : 0;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
if ( $khkplrv_meta ) {
	$khkplrv_content_padding = $khkplrv_meta['content_spacings'];
} else { $khkplrv_content_padding = 'section-padding'; }
// Top and Bottom Padding
if ( $khkplrv_content_padding && $khkplrv_content_padding !== 'padding-default' ) {
	$khkplrv_content_top_spacings = isset( $khkplrv_meta['content_top_spacings'] ) ? $khkplrv_meta['content_top_spacings'] : '';
	$khkplrv_content_bottom_spacings = isset( $khkplrv_meta['content_bottom_spacings'] ) ? $khkplrv_meta['content_bottom_spacings'] : '';
	if ( $khkplrv_content_padding === 'padding-custom' ) {
		$khkplrv_content_top_spacings = $khkplrv_content_top_spacings ? 'padding-top:'. khkplrv_check_px( $khkplrv_content_top_spacings ) .';' : '';
		$khkplrv_content_bottom_spacings = $khkplrv_content_bottom_spacings ? 'padding-bottom:'. khkplrv_check_px($khkplrv_content_bottom_spacings) .';' : '';
		$khkplrv_custom_padding = $khkplrv_content_top_spacings . $khkplrv_content_bottom_spacings;
	} else {
		$khkplrv_custom_padding = '';
	}
	$padding_class = '';
} else {
	$khkplrv_custom_padding = '';
	$padding_class = '';
}
// Page Layout
$khkplrv_page_layout = get_post_meta( get_the_ID(), 'page_layout_options', true );
if ( $khkplrv_page_layout ) {
	$khkplrv_page_layout = $khkplrv_page_layout['page_layout'];
} else {
	$khkplrv_page_layout = 'right-sidebar';
}
if ( $khkplrv_page_layout === 'extra-width' ) {
	$khkplrv_page_column = 'extra-width';
	$khkplrv_page_container = 'container-fluid';
} elseif ( $khkplrv_page_layout === 'full-width' ) {
	$khkplrv_page_column = 'col-md-12';
	$khkplrv_page_container = 'container ';
} elseif( $khkplrv_page_layout === 'left-sidebar' || $khkplrv_page_layout === 'right-sidebar' ) {
	if( $khkplrv_page_layout === 'left-sidebar' ){
		$khkplrv_page_column = 'col-md-8 order-12';
	} else {
		$khkplrv_page_column = 'col-md-8';
	}
	$khkplrv_page_container = 'container ';
} else {
	$khkplrv_page_column = 'col-md-8';
	$khkplrv_page_container = 'container ';
}
$khkplrv_theme_page_comments = cs_get_option( 'theme_page_comments' );
get_header();
?>
<div class="page-wrap <?php echo esc_attr( $padding_class ); ?>">
	<div class="<?php echo esc_attr( $khkplrv_page_container.''.$khkplrv_content_padding.' '.$khkplrv_page_layout ); ?>" style="<?php echo esc_attr( $khkplrv_custom_padding ); ?>">
		<div class="row">
			<div class="<?php echo esc_attr( $khkplrv_page_column ); ?>">
				<div class="page-wraper clearfix">
				<?php
				while ( have_posts() ) : the_post();
					the_content();
					khkplrv_paging_nav();
					if ( !$khkplrv_theme_page_comments && ( comments_open() || get_comments_number() ) ) :
						comments_template();
					endif;
				endwhile; // End of the loop.
				?>
				</div>
				<div class="page-link-wrap">
					<?php khkplrv_wp_link_pages(); ?>
				</div>
			</div>
			<?php
			// Sidebar
			if( $khkplrv_page_layout === 'left-sidebar' || $khkplrv_page_layout === 'right-sidebar' ) {
				get_sidebar();
			}
			?>
		</div>
	</div>
</div>
<?php
get_footer();