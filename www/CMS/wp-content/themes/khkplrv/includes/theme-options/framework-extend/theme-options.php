<?php
/*
 * All Theme Options for Khkplrv theme.
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */

function khkplrv_settings( $settings ) {

  $settings           = array(
    'menu_title'      => KHKPLRV_NAME . esc_html__(' Options', 'khkplrv'),
    'menu_slug'       => sanitize_title(KHKPLRV_NAME) . '_options',
    'menu_type'       => 'theme',
    'menu_icon'       => 'dashicons-awards',
    'menu_position'   => '4',
    'ajax_save'       => false,
    'show_reset_all'  => true,
    'framework_title' => KHKPLRV_NAME .' <small>V-'. KHKPLRV_VERSION .' by <a href="'. KHKPLRV_BRAND_URL .'" target="_blank">'. KHKPLRV_BRAND_NAME .'</a></small>',
  );

  return $settings;

}
add_filter( 'cs_framework_settings', 'khkplrv_settings' );

// Theme Framework Options
function khkplrv_options( $options ) {

  $options      = array(); // remove old options

  // ------------------------------
  // Branding
  // ------------------------------
  $options[]   = array(
    'name'     => 'khkplrv_theme_branding',
    'title'    => esc_html__('Brand', 'khkplrv'),
    'icon'     => 'fa fa-trophy',
    'sections' => array(

      // brand logo tab
      array(
        'name'     => 'brand_logo',
        'title'    => esc_html__('Logo', 'khkplrv'),
        'icon'     => 'fa fa-star',
        'fields'   => array(

          // Site Logo
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Site Logo', 'khkplrv')
          ),
          array(
            'id'    => 'khkplrv_logo',
            'type'  => 'image',
            'title' => esc_html__('Default Logo', 'khkplrv'),
            'info'  => esc_html__('Upload your default logo here. If you not upload, then site title will load in this logo location.', 'khkplrv'),
            'add_title' => esc_html__('Add Logo', 'khkplrv'),
          ),
          array(
            'id'    => 'khkplrv_trlogo',
            'type'  => 'image',
            'title' => esc_html__('Transparent Logo', 'khkplrv'),
            'info'  => esc_html__('Upload your Transparent logo here. If you not upload, then site title will load in this logo location.', 'khkplrv'),
            'add_title' => esc_html__('Add Logo', 'khkplrv'),
          ),
          array(
            'id'          => 'khkplrv_logo_top',
            'type'        => 'number',
            'title'       => esc_html__('Logo Top Space', 'khkplrv'),
            'attributes'  => array( 'placeholder' => 5 ),
            'unit'        => 'px',
          ),
          array(
            'id'          => 'khkplrv_logo_bottom',
            'type'        => 'number',
            'title'       => esc_html__('Logo Bottom Space', 'khkplrv'),
            'attributes'  => array( 'placeholder' => 5 ),
            'unit'        => 'px',
          ),
          // WordPress Admin Logo
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('WordPress Admin Logo', 'khkplrv')
          ),
          array(
            'id'    => 'brand_logo_wp',
            'type'  => 'image',
            'title' => esc_html__('Login logo', 'khkplrv'),
            'info'  => esc_html__('Upload your WordPress login page logo here.', 'khkplrv'),
            'add_title' => esc_html__('Add Login Logo', 'khkplrv'),
          ),
        ) // end: fields
      ), // end: section
    ),
  );

  // ------------------------------
  // Layout
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_layout',
    'title'  => esc_html__('Layout', 'khkplrv'),
    'icon'   => 'fa fa-file-text'
  );

  $options[]      = array(
    'name'        => 'theme_general',
    'title'       => esc_html__('General', 'khkplrv'),
    'icon'        => 'fa fa-wrench',

    // begin: fields
    'fields'      => array(

      // -----------------------------
      // Begin: Responsive
      // -----------------------------
       array(
        'id'    => 'theme_responsive',
        'off_text'  => 'No',
        'on_text'  => 'Yes',
        'type'  => 'switcher',
        'title' => esc_html__('Responsive', 'khkplrv'),
        'info' => esc_html__('Turn on if you don\'t want your site to be responsive.', 'khkplrv'),
        'default' => false,
      ),
      array(
        'id'    => 'theme_preloder',
        'off_text'  => 'No',
        'on_text'  => 'Yes',
        'type'  => 'switcher',
        'title' => esc_html__('Preloder', 'khkplrv'),
        'info' => esc_html__('Turn off if you don\'t want your site to be Preloder.', 'khkplrv'),
        'default' => true,
      ),
      array(
        'id'    => 'theme_layout_width',
        'type'  => 'image_select',
        'title' => esc_html__('Full Width & Extra Width', 'khkplrv'),
        'info' => esc_html__('Boxed or Fullwidth? Choose your site layout width. Default : Full Width', 'khkplrv'),
        'options'      => array(
          'container'    => KHKPLRV_CS_IMAGES .'/boxed-width.jpg',
          'container-fluid'    => KHKPLRV_CS_IMAGES .'/full-width.jpg',
        ),
        'default'      => 'container-fluid',
        'radio'      => true,
      ),
       array(
        'id'    => 'theme_page_comments',
        'type'  => 'switcher',
        'title' => esc_html__('Hide Page Comments?', 'khkplrv'),
        'label' => esc_html__('Yes! Hide Page Comments.', 'khkplrv'),
        'on_text' => esc_html__('Yes', 'khkplrv'),
        'off_text' => esc_html__('No', 'khkplrv'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-khkplrv-heading',
        'content' => esc_html__('Background Options', 'khkplrv'),
        'dependency' => array( 'theme_layout_width_container', '==', 'true' ),
      ),
      array(
        'id'             => 'theme_layout_bg_type',
        'type'           => 'select',
        'title'          => esc_html__('Background Type', 'khkplrv'),
        'options'        => array(
          'bg-image' => esc_html__('Image', 'khkplrv'),
          'bg-pattern' => esc_html__('Pattern', 'khkplrv'),
        ),
        'dependency' => array( 'theme_layout_width_container', '==', 'true' ),
      ),
      array(
        'id'    => 'theme_layout_bg_pattern',
        'type'  => 'image_select',
        'title' => esc_html__('Background Pattern', 'khkplrv'),
        'info' => esc_html__('Select background pattern', 'khkplrv'),
        'options'      => array(
          'pattern-1'    => KHKPLRV_CS_IMAGES . '/pattern-1.png',
          'pattern-2'    => KHKPLRV_CS_IMAGES . '/pattern-2.png',
          'pattern-3'    => KHKPLRV_CS_IMAGES . '/pattern-3.png',
          'pattern-4'    => KHKPLRV_CS_IMAGES . '/pattern-4.png',
          'custom-pattern'  => KHKPLRV_CS_IMAGES . '/pattern-5.png',
        ),
        'default'      => 'pattern-1',
        'radio'      => true,
        'dependency' => array( 'theme_layout_width_container|theme_layout_bg_type', '==|==', 'true|bg-pattern' ),
      ),
      array(
        'id'      => 'custom_bg_pattern',
        'type'    => 'upload',
        'title'   => esc_html__('Custom Pattern', 'khkplrv'),
        'dependency' => array( 'theme_layout_width_container|theme_layout_bg_type|theme_layout_bg_pattern_custom-pattern', '==|==|==', 'true|bg-pattern|true' ),
        'info' => __('Select your custom background pattern. <br />Note, background pattern image will be repeat in all x and y axis. So, please consider all repeatable area will perfectly fit your custom patterns.', 'khkplrv'),
      ),
      array(
        'id'      => 'theme_layout_bg',
        'type'    => 'background',
        'title'   => esc_html__('Background', 'khkplrv'),
        'dependency' => array( 'theme_layout_width_container|theme_layout_bg_type', '==|==', 'true|bg-image' ),
      ),

    ), // end: fields

  );

  // ------------------------------
  // Header Sections
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_header_tab',
    'title'    => esc_html__('Header', 'khkplrv'),
    'icon'     => 'fa fa-bars',
    'sections' => array(

      // header design tab
      array(
        'name'     => 'header_design_tab',
        'title'    => esc_html__('Design', 'khkplrv'),
        'icon'     => 'fa fa-magic',
        'fields'   => array(

          // Header Select
          array(
            'id'           => 'select_header_design',
            'type'         => 'image_select',
            'title'        => esc_html__('Select Header Design', 'khkplrv'),
            'options'      => array(
              'style_one'    => KHKPLRV_CS_IMAGES .'/hs-1.png',
              'style_two'    => KHKPLRV_CS_IMAGES .'/hs-2.png',
              'style_three'    => KHKPLRV_CS_IMAGES .'/hs-2.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'header_design',
            ),
            'radio'        => true,
            'default'   => 'style_one',
            'info' => esc_html__('Select your header design, following options will may differ based on your selection of header design.', 'khkplrv'),
          ),
          // Header Select

          // Extra's
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Extra\'s', 'khkplrv'),
          ),
          array(
            'id'    => 'header_middle',
            'type'  => 'textarea',
            'title' => esc_html__('Header Middle', 'khkplrv'),
            'info' => esc_html__('Header Middle Content for Header style One and Four.', 'khkplrv'),
            'shortcode'   => true,
            'dependency'   => array('header_design', 'any', 'style_two'),
          ),
          array(
            'id'    => 'sticky_header',
            'type'  => 'switcher',
            'title' => esc_html__('Sticky Header', 'khkplrv'),
            'info' => esc_html__('Turn On if you want your naviagtion bar on sticky.', 'khkplrv'),
            'default' => true,
          ),
          array(
            'id'    => 'khkplrv_cart_widget',
            'type'  => 'switcher',
            'title' => esc_html__('Header Cart', 'khkplrv'),
            'info' => esc_html__('Turn On if you want to Show Header Cart .', 'khkplrv'),
            'default' => false,
          ),
           array(
            'id'    => 'khkplrv_search',
            'type'  => 'switcher',
            'title' => esc_html__('Header Search', 'khkplrv'),
            'info' => esc_html__('Turn On if you want to hide Header Search .', 'khkplrv'),
            'default' => false,
          ),
        )
      ),

      // header top bar
      array(
        'name'     => 'header_top_bar_tab',
        'title'    => esc_html__('Top Bar', 'khkplrv'),
        'icon'     => 'fa fa-minus',
        'fields'   => array(

          array(
            'id'          => 'top_bar',
            'type'        => 'switcher',
            'title'       => esc_html__('Hide Top Bar', 'khkplrv'),
            'on_text'     => esc_html__('Yes', 'khkplrv'),
            'off_text'    => esc_html__('No', 'khkplrv'),
            'default'     => true,
          ),
          array(
            'id'          => 'top_left',
            'title'       => esc_html__('Top left Block', 'khkplrv'),
            'desc'        => esc_html__('Top bar left block.', 'khkplrv'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),
          array(
            'id'          => 'top_right',
            'title'       => esc_html__('Top Right Block', 'khkplrv'),
            'desc'        => esc_html__('Top bar right block.', 'khkplrv'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),
        )
      ),

      // header banner
      array(
        'name'     => 'header_banner_tab',
        'title'    => esc_html__('Title Bar (or) Banner', 'khkplrv'),
        'icon'     => 'fa fa-bullhorn',
        'fields'   => array(

          // Title Area
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Title Area', 'khkplrv')
          ),
          array(
            'id'      => 'need_title_bar',
            'type'    => 'switcher',
            'title'   => esc_html__('Title Bar ?', 'khkplrv'),
            'label'   => esc_html__('If you want to Hide title bar in your sub-pages, please turn this ON.', 'khkplrv'),
            'default'    => false,
          ),
          array(
            'id'             => 'title_bar_padding',
            'type'           => 'select',
            'title'          => esc_html__('Padding Spaces Top & Bottom', 'khkplrv'),
            'options'        => array(
              'padding-default' => esc_html__('Default Spacing', 'khkplrv'),
              'padding-custom' => esc_html__('Custom Padding', 'khkplrv'),
            ),
            'dependency'   => array( 'need_title_bar', '==', 'false' ),
          ),
          array(
            'id'             => 'titlebar_top_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Top', 'khkplrv'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding', '==', 'padding-custom' ),
          ),
          array(
            'id'             => 'titlebar_bottom_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Bottom', 'khkplrv'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding', '==', 'padding-custom' ),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Background Options', 'khkplrv'),
            'dependency' => array( 'need_title_bar', '==', 'false' ),
          ),
          array(
            'id'      => 'titlebar_bg_overlay_color',
            'type'    => 'color_picker',
            'title'   => esc_html__('Overlay Color', 'khkplrv'),
            'dependency' => array( 'need_title_bar', '==', 'false' ),
          ),
          array(
            'id'    => 'title_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Title Color', 'khkplrv'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Breadcrumbs', 'khkplrv'),
          ),
         array(
            'id'      => 'need_breadcrumbs',
            'type'    => 'switcher',
            'title'   => esc_html__('Hide Breadcrumbs', 'khkplrv'),
            'label'   => esc_html__('If you want to hide breadcrumbs in your banner, please turn this ON.', 'khkplrv'),
            'default'    => false,
          ),
        )
      ),

    ),
  );

  // ------------------------------
  // Footer Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'footer_section',
    'title'    => esc_html__('Footer', 'khkplrv'),
    'icon'     => 'fa fa-ellipsis-h',
    'sections' => array(

      // footer widgets
      array(
        'name'     => 'footer_widgets_tab',
        'title'    => esc_html__('Widget Area', 'khkplrv'),
        'icon'     => 'fa fa-th',
        'fields'   => array(

          // Footer Widget Block
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Footer Widget Block', 'khkplrv')
          ),
          array(
            'id'    => 'footer_widget_block',
            'type'  => 'switcher',
            'title' => esc_html__('Enable Widget Block', 'khkplrv'),
            'info' => __('If you turn this ON, then Goto : Appearance > Widgets. There you can see <strong>Footer Widget 1,2,3 or 4</strong> Widget Area, add your widgets there.', 'khkplrv'),
            'default' => true,
          ),
          array(
            'id'    => 'footer_widget_layout',
            'type'  => 'image_select',
            'title' => esc_html__('Widget Layouts', 'khkplrv'),
            'info' => esc_html__('Choose your footer widget theme-layouts.', 'khkplrv'),
            'default' => 4,
            'options' => array(
              1   => KHKPLRV_CS_IMAGES . '/footer/footer-1.png',
              2   => KHKPLRV_CS_IMAGES . '/footer/footer-2.png',
              3   => KHKPLRV_CS_IMAGES . '/footer/footer-3.png',
              4   => KHKPLRV_CS_IMAGES . '/footer/footer-4.png',
              5   => KHKPLRV_CS_IMAGES . '/footer/footer-5.png',
              6   => KHKPLRV_CS_IMAGES . '/footer/footer-6.png',
              7   => KHKPLRV_CS_IMAGES . '/footer/footer-7.png',
              8   => KHKPLRV_CS_IMAGES . '/footer/footer-8.png',
              9   => KHKPLRV_CS_IMAGES . '/footer/footer-9.png',
            ),
            'radio'       => true,
            'dependency'  => array('footer_widget_block', '==', true),
          ),
           array(
            'id'    => 'khkplrv_ft_bg',
            'type'  => 'image',
            'title' => esc_html__('Footer Background', 'khkplrv'),
            'info'  => esc_html__('Upload your footer background.', 'khkplrv'),
            'add_title' => esc_html__('footer background', 'khkplrv'),
            'dependency'  => array('footer_widget_block', '==', true),
          ),

        )
      ),

      // footer copyright
      array(
        'name'     => 'footer_copyright_tab',
        'title'    => esc_html__('Copyright Bar', 'khkplrv'),
        'icon'     => 'fa fa-copyright',
        'fields'   => array(

          // Copyright
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Copyright Layout', 'khkplrv'),
          ),
         array(
            'id'    => 'hide_copyright',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Copyright?', 'khkplrv'),
            'default' => false,
            'on_text' => esc_html__('Yes', 'khkplrv'),
            'off_text' => esc_html__('No', 'khkplrv'),
            'label' => esc_html__('Yes, do it!', 'khkplrv'),
          ),
          array(
            'id'    => 'footer_copyright_layout',
            'type'  => 'image_select',
            'title' => esc_html__('Select Copyright Layout', 'khkplrv'),
            'info' => esc_html__('In above image, blue box is copyright text and yellow box is secondary text.', 'khkplrv'),
            'default'      => 'copyright-3',
            'options'      => array(
              'copyright-1'    => KHKPLRV_CS_IMAGES .'/footer/copyright-1.png',
              ),
            'radio'        => true,
            'dependency'     => array('hide_copyright', '!=', true),
          ),
          array(
            'id'    => 'copyright_text',
            'type'  => 'textarea',
            'title' => esc_html__('Copyright Text', 'khkplrv'),
            'shortcode' => true,
            'dependency' => array('hide_copyright', '!=', true),
            'after'       => 'Helpful shortcodes: [current_year] [home_url] or any shortcode.',
          ),

          // Copyright Another Text
          array(
            'type'    => 'notice',
            'class'   => 'warning cs-khkplrv-heading',
            'content' => esc_html__('Copyright Secondary Text', 'khkplrv'),
             'dependency'     => array('hide_copyright', '!=', true),
          ),
          array(
            'id'    => 'secondary_text',
            'type'  => 'textarea',
            'title' => esc_html__('Secondary Text', 'khkplrv'),
            'shortcode' => true,
            'dependency'     => array('hide_copyright', '!=', true),
          ),

        )
      ),

    ),
  );

  // ------------------------------
  // Design
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_design',
    'title'  => esc_html__('Design', 'khkplrv'),
    'icon'   => 'fa fa-magic'
  );

  // ------------------------------
  // color section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_color_section',
    'title'    => esc_html__('Colors', 'khkplrv'),
    'icon'     => 'fa fa-eyedropper',
    'fields' => array(

      array(
        'type'    => 'heading',
        'content' => esc_html__('Color Options', 'khkplrv'),
      ),
      array(
        'type'    => 'subheading',
        'wrap_class' => 'color-tab-content',
        'content' => esc_html__('All color options are available in our theme customizer. The reason of we used customizer options for color section is because, you can choose each part of color from there and see the changes instantly using customizer. Highly customizable colors are in Appearance > Customize', 'khkplrv'),
      ),

    ),
  );

  // ------------------------------
  // Typography section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_typo_section',
    'title'    => esc_html__('Typography', 'khkplrv'),
    'icon'     => 'fa fa-header',
    'fields' => array(

      // Start fields
      array(
        'id'                  => 'typography',
        'type'                => 'group',
        'fields'              => array(
          array(
            'id'              => 'title',
            'type'            => 'text',
            'title'           => esc_html__('Title', 'khkplrv'),
          ),
          array(
            'id'              => 'selector',
            'type'            => 'textarea',
            'title'           => esc_html__('Selector', 'khkplrv'),
            'info'           => wp_kses( __('Enter css selectors like : <strong>body, .custom-class</strong>', 'khkplrv'), array( 'strong' => array() ) ),
          ),
          array(
            'id'              => 'font',
            'type'            => 'typography',
            'title'           => esc_html__('Font Family', 'khkplrv'),
          ),
          array(
            'id'              => 'size',
            'type'            => 'text',
            'title'           => esc_html__('Font Size', 'khkplrv'),
          ),
          array(
            'id'              => 'line_height',
            'type'            => 'text',
            'title'           => esc_html__('Line-Height', 'khkplrv'),
          ),
          array(
            'id'              => 'css',
            'type'            => 'textarea',
            'title'           => esc_html__('Custom CSS', 'khkplrv'),
          ),
        ),
        'button_title'        => esc_html__('Add New Typography', 'khkplrv'),
        'accordion_title'     => esc_html__('New Typography', 'khkplrv'),
      ),

      // Subset
      array(
        'id'                  => 'subsets',
        'type'                => 'select',
        'title'               => esc_html__('Subsets', 'khkplrv'),
        'class'               => 'chosen',
        'options'             => array(
          'latin'             => 'latin',
          'latin-ext'         => 'latin-ext',
          'cyrillic'          => 'cyrillic',
          'cyrillic-ext'      => 'cyrillic-ext',
          'greek'             => 'greek',
          'greek-ext'         => 'greek-ext',
          'vietnamese'        => 'vietnamese',
          'devanagari'        => 'devanagari',
          'khmer'             => 'khmer',
        ),
        'attributes'         => array(
          'data-placeholder' => 'Subsets',
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( 'latin' ),
      ),

      array(
        'id'                  => 'font_weight',
        'type'                => 'select',
        'title'               => esc_html__('Font Weights', 'khkplrv'),
        'class'               => 'chosen',
        'options'             => array(
          '100'   => esc_html__('Thin 100', 'khkplrv'),
          '100i'  => esc_html__('Thin 100 Italic', 'khkplrv'),
          '200'   => esc_html__('Extra Light 200', 'khkplrv'),
          '200i'  => esc_html__('Extra Light 200 Italic', 'khkplrv'),
          '300'   => esc_html__('Light 300', 'khkplrv'),
          '300i'  => esc_html__('Light 300 Italic', 'khkplrv'),
          '400'   => esc_html__('Regular 400', 'khkplrv'),
          '400i'  => esc_html__('Regular 400 Italic', 'khkplrv'),
          '500'   => esc_html__('Medium 500', 'khkplrv'),
          '500i'  => esc_html__('Medium 500 Italic', 'khkplrv'),
          '600'   => esc_html__('Semi Bold 600', 'khkplrv'),
          '600i'  => esc_html__('Semi Bold 600 Italic', 'khkplrv'),
          '700'   => esc_html__('Bold 700', 'khkplrv'),
          '700i'  => esc_html__('Bold 700 Italic', 'khkplrv'),
          '800'   => esc_html__('Extra Bold 800', 'khkplrv'),
          '800i'  => esc_html__('Extra Bold 800 Italic', 'khkplrv'),
          '900'   => esc_html__('Black 900', 'khkplrv'),
          '900i'  => esc_html__('Black 900 Italic', 'khkplrv'),
        ),
        'attributes'         => array(
          'data-placeholder' => esc_html__('Font Weight', 'khkplrv'),
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( '400' ),
      ),

      // Custom Fonts Upload
      array(
        'id'                 => 'font_family',
        'type'               => 'group',
        'title'              => esc_html__('Upload Custom Fonts', 'khkplrv'),
        'button_title'       => esc_html__('Add New Custom Font', 'khkplrv'),
        'accordion_title'    => esc_html__('Adding New Font', 'khkplrv'),
        'accordion'          => true,
        'desc'               => esc_html__('It is simple. Only add your custom fonts and click to save. After you can check "Font Family" selector. Do not forget to Save!', 'khkplrv'),
        'fields'             => array(

          array(
            'id'             => 'name',
            'type'           => 'text',
            'title'          => esc_html__('Font-Family Name', 'khkplrv'),
            'attributes'     => array(
              'placeholder'  => esc_html__('for eg. Arial', 'khkplrv')
            ),
          ),

          array(
            'id'             => 'ttf',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .ttf <small><i>(optional)</i></small>', 'khkplrv'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'khkplrv'),
              'button_title' => wp_kses(__('Upload <i>.ttf</i>', 'khkplrv'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'eot',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .eot <small><i>(optional)</i></small>', 'khkplrv'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'khkplrv'),
              'button_title' => wp_kses(__('Upload <i>.eot</i>', 'khkplrv'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'svg',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .svg <small><i>(optional)</i></small>', 'khkplrv'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'khkplrv'),
              'button_title' => wp_kses(__('Upload <i>.svg</i>', 'khkplrv'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'otf',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .otf <small><i>(optional)</i></small>', 'khkplrv'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'khkplrv'),
              'button_title' => wp_kses(__('Upload <i>.otf</i>', 'khkplrv'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'woff',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .woff <small><i>(optional)</i></small>', 'khkplrv'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'khkplrv'),
              'button_title' =>wp_kses(__('Upload <i>.woff</i>', 'khkplrv'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'css',
            'type'           => 'textarea',
            'title'          => wp_kses(__('Extra CSS Style <small><i>(optional)</i></small>', 'khkplrv'), array( 'small' => array(), 'i' => array() )),
            'attributes'     => array(
              'placeholder'  => esc_html__('for eg. font-weight: normal;', 'khkplrv'),
            ),
          ),

        ),
      ),
      // End All field

    ),
  );

  // ------------------------------
  // Pages
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_pages',
    'title'  => esc_html__('Pages', 'khkplrv'),
    'icon'   => 'fa fa-files-o'
  );


  // ------------------------------
  // Service Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'service_section',
    'title'    => esc_html__('Service', 'khkplrv'),
    'icon'     => 'fa fa-th-list',
    'fields' => array(

      // Team Start
      array(
        'type'    => 'notice',
        'class'   => 'info cs-khkplrv-heading',
        'content' => esc_html__('Service Single', 'khkplrv')
      ),
      array(
          'id'             => 'service_sidebar_position',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Position', 'khkplrv'),
          'options'        => array(
            'sidebar-right' => esc_html__('Right', 'khkplrv'),
            'sidebar-left' => esc_html__('Left', 'khkplrv'),
            'sidebar-hide' => esc_html__('Hide', 'khkplrv'),
          ),
          'default_option' => 'Select sidebar position',
          'info'          => esc_html__('Default option : Right', 'khkplrv'),
        ),
        array(
          'id'             => 'single_service_widget',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Widget', 'khkplrv'),
          'options'        => khkplrv_registered_sidebars(),
          'default_option' => esc_html__('Select Widget', 'khkplrv'),
          'dependency'     => array('service_sidebar_position', '!=', 'sidebar-hide'),
          'info'          => esc_html__('Default option : Main Widget Area', 'khkplrv'),
        ),
        array(
          'id'    => 'service_comment_form',
          'type'  => 'switcher',
          'title' => esc_html__('Comment Area/Form', 'khkplrv'),
          'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this OFF.', 'khkplrv'),
          'default' => true,
        ),
    ),
  );

   // ------------------------------
  // Project Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'project_section',
    'title'    => esc_html__('Project', 'khkplrv'),
    'icon'     => 'fa fa-medkit',
    'fields' => array(

      // Team Start
      array(
        'type'    => 'notice',
        'class'   => 'info cs-khkplrv-heading',
        'content' => esc_html__('Project Single', 'khkplrv')
      ),
      array(
          'id'             => 'project_sidebar_position',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Position', 'khkplrv'),
          'options'        => array(
            'sidebar-right' => esc_html__('Right', 'khkplrv'),
            'sidebar-left' => esc_html__('Left', 'khkplrv'),
            'sidebar-hide' => esc_html__('Hide', 'khkplrv'),
          ),
          'default_option' => 'Select sidebar position',
          'info'          => esc_html__('Default option : Right', 'khkplrv'),
        ),
        array(
          'id'             => 'single_project_widget',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Widget', 'khkplrv'),
          'options'        => khkplrv_registered_sidebars(),
          'default_option' => esc_html__('Select Widget', 'khkplrv'),
          'dependency'     => array('project_sidebar_position', '!=', 'sidebar-hide'),
          'info'          => esc_html__('Default option : Main Widget Area', 'khkplrv'),
        ),
        array(
          'id'    => 'project_comment_form',
          'type'  => 'switcher',
          'title' => esc_html__('Comment Area/Form', 'khkplrv'),
          'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this OFF.', 'khkplrv'),
          'default' => true,
        ),
    ),
  );

  // ------------------------------
  // Blog Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'blog_section',
    'title'    => esc_html__('Blog', 'khkplrv'),
    'icon'     => 'fa fa-edit',
    'sections' => array(

      // blog general section
      array(
        'name'     => 'blog_general_tab',
        'title'    => esc_html__('General', 'khkplrv'),
        'icon'     => 'fa fa-cog',
        'fields'   => array(

          // Layout
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Layout', 'khkplrv')
          ),
          array(
            'id'             => 'blog_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'khkplrv'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'khkplrv'),
              'sidebar-left' => esc_html__('Left', 'khkplrv'),
              'sidebar-hide' => esc_html__('Hide', 'khkplrv'),
            ),
            'default_option' => 'Select sidebar position',
            'help'          => esc_html__('This style will apply, default blog pages - Like : Archive, Category, Tags, Search & Author.', 'khkplrv'),
            'info'          => esc_html__('Default option : Right', 'khkplrv'),
          ),
          array(
            'id'             => 'blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'khkplrv'),
            'options'        => khkplrv_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'khkplrv'),
            'dependency'     => array('blog_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'khkplrv'),
          ),
          // Layout
          // Global Options
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Global Options', 'khkplrv')
          ),
          array(
            'id'         => 'theme_exclude_categories',
            'type'       => 'checkbox',
            'title'      => esc_html__('Exclude Categories', 'khkplrv'),
            'info'      => esc_html__('Select categories you want to exclude from blog page.', 'khkplrv'),
            'options'    => 'categories',
          ),
          array(
            'id'      => 'theme_blog_excerpt',
            'type'    => 'text',
            'title'   => esc_html__('Excerpt Length', 'khkplrv'),
            'info'   => esc_html__('Blog short content length, in blog listing pages.', 'khkplrv'),
            'default' => '55',
          ),
          array(
            'id'      => 'theme_metas_hide',
            'type'    => 'checkbox',
            'title'   => esc_html__('Meta\'s to hide', 'khkplrv'),
            'info'    => esc_html__('Check items you want to hide from blog/post meta field.', 'khkplrv'),
            'class'      => 'horizontal',
            'options'    => array(
              'category'   => esc_html__('Category', 'khkplrv'),
              'date'    => esc_html__('Date', 'khkplrv'),
              'author'     => esc_html__('Author', 'khkplrv'),
              'comments'      => esc_html__('Comments', 'khkplrv'),
            ),
            // 'default' => '30',
          ),
          // End fields

        )
      ),

      // blog single section
      array(
        'name'     => 'blog_single_tab',
        'title'    => esc_html__('Single', 'khkplrv'),
        'icon'     => 'fa fa-sticky-note',
        'fields'   => array(

          // Start fields
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Enable / Disable', 'khkplrv')
          ),
          array(
            'id'    => 'single_featured_image',
            'type'  => 'switcher',
            'title' => esc_html__('Featured Image', 'khkplrv'),
            'info' => esc_html__('If need to hide featured image from single blog post page, please turn this OFF.', 'khkplrv'),
            'default' => true,
          ),
           array(
            'id'    => 'single_author_info',
            'type'  => 'switcher',
            'title' => esc_html__('Author Info', 'khkplrv'),
            'info' => esc_html__('If need to hide author info on single blog page, please turn this On.', 'khkplrv'),
            'default' => false,
          ),
          array(
            'id'    => 'single_share_option',
            'type'  => 'switcher',
            'title' => esc_html__('Share Option', 'khkplrv'),
            'info' => esc_html__('If need to hide share option on single blog page, please turn this OFF.', 'khkplrv'),
            'default' => true,
          ),
          array(
            'id'    => 'single_comment_form',
            'type'  => 'switcher',
            'title' => esc_html__('Comment Area/Form ?', 'khkplrv'),
            'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this On.', 'khkplrv'),
            'default' => false,
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Sidebar', 'khkplrv')
          ),
          array(
            'id'             => 'single_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'khkplrv'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'khkplrv'),
              'sidebar-left' => esc_html__('Left', 'khkplrv'),
              'sidebar-hide' => esc_html__('Hide', 'khkplrv'),
            ),
            'default_option' => 'Select sidebar position',
            'info'          => esc_html__('Default option : Right', 'khkplrv'),
          ),
          array(
            'id'             => 'single_blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'khkplrv'),
            'options'        => khkplrv_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'khkplrv'),
            'dependency'     => array('single_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'khkplrv'),
          ),
          // End fields

        )
      ),

    ),
  );

if (class_exists( 'WooCommerce' )){
  // ------------------------------
  // WooCommerce Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'woocommerce_section',
    'title'    => esc_html__('WooCommerce', 'khkplrv'),
    'icon'     => 'fa fa-shopping-cart',
    'fields' => array(

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-khkplrv-heading',
        'content' => esc_html__('Layout', 'khkplrv')
      ),
     array(
        'id'             => 'woo_product_columns',
        'type'           => 'select',
        'title'          => esc_html__('Product Column', 'khkplrv'),
        'options'        => array(
          2 => esc_html__('Two Column', 'khkplrv'),
          3 => esc_html__('Three Column', 'khkplrv'),
          4 => esc_html__('Four Column', 'khkplrv'),
        ),
        'default_option' => esc_html__('Select Product Columns', 'khkplrv'),
        'help'          => esc_html__('This style will apply, default woocommerce shop and archive pages.', 'khkplrv'),
      ),
      array(
        'id'             => 'woo_sidebar_position',
        'type'           => 'select',
        'title'          => esc_html__('Sidebar Position', 'khkplrv'),
        'options'        => array(
          'right-sidebar' => esc_html__('Right', 'khkplrv'),
          'left-sidebar' => esc_html__('Left', 'khkplrv'),
          'sidebar-hide' => esc_html__('Hide', 'khkplrv'),
        ),
        'default_option' => esc_html__('Select sidebar position', 'khkplrv'),
        'info'          => esc_html__('Default option : Right', 'khkplrv'),
      ),
      array(
        'id'             => 'woo_widget',
        'type'           => 'select',
        'title'          => esc_html__('Sidebar Widget', 'khkplrv'),
        'options'        => khkplrv_registered_sidebars(),
        'default_option' => esc_html__('Select Widget', 'khkplrv'),
        'dependency'     => array('woo_sidebar_position', '!=', 'sidebar-hide'),
        'info'          => esc_html__('Default option : Shop Page', 'khkplrv'),
      ),

      array(
        'type'    => 'notice',
        'class'   => 'info cs-khkplrv-heading',
        'content' => esc_html__('Listing', 'khkplrv')
      ),
      array(
        'id'      => 'theme_woo_limit',
        'type'    => 'text',
        'title'   => esc_html__('Product Limit', 'khkplrv'),
        'info'   => esc_html__('Enter the number value for per page products limit.', 'khkplrv'),
      ),
      // End fields

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-khkplrv-heading',
        'content' => esc_html__('Single Product', 'khkplrv')
      ),
      array(
        'id'             => 'woo_related_limit',
        'type'           => 'text',
        'title'          => esc_html__('Related Products Limit', 'khkplrv'),
      ),
      array(
        'id'    => 'woo_single_upsell',
        'type'  => 'switcher',
        'title' => esc_html__('You May Also Like', 'khkplrv'),
        'info' => esc_html__('If you don\'t want \'You May Also Like\' products in single product page, please turn this ON.', 'khkplrv'),
        'default' => false,
      ),
      array(
        'id'    => 'woo_single_related',
        'type'  => 'switcher',
        'title' => esc_html__('Related Products', 'khkplrv'),
        'info' => esc_html__('If you don\'t want \'Related Products\' in single product page, please turn this ON.', 'khkplrv'),
        'default' => false,
      ),
      // End fields

    ),
  );
}

  // ------------------------------
  // Extra Pages
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_extra_pages',
    'title'    => esc_html__('Extra Pages', 'khkplrv'),
    'icon'     => 'fa fa-clone',
    'sections' => array(

      // error 404 page
      array(
        'name'     => 'error_page_section',
        'title'    => esc_html__('404 Page', 'khkplrv'),
        'icon'     => 'fa fa-exclamation-triangle',
        'fields'   => array(

          // Start 404 Page
          array(
            'id'    => 'error_heading',
            'type'  => 'text',
            'title' => esc_html__('404 Page Heading', 'khkplrv'),
            'info'  => esc_html__('Enter 404 page heading.', 'khkplrv'),
          ),
          array(
            'id'    => 'error_subheading',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Sub Heading', 'khkplrv'),
            'info'  => esc_html__('Enter 404 page Sub heading.', 'khkplrv'),
          ),
          array(
            'id'    => 'error_page_content',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Content', 'khkplrv'),
            'info'  => esc_html__('Enter 404 page content.', 'khkplrv'),
            'shortcode' => true,
          ),
          array(
            'id'    => 'error_page_bg',
            'type'  => 'image',
            'title' => esc_html__('404 Page Background', 'khkplrv'),
            'info'  => esc_html__('Choose 404 page background styles.', 'khkplrv'),
            'add_title' => esc_html__('Add 404 Image', 'khkplrv'),
          ),
          array(
            'id'    => 'error_btn_text',
            'type'  => 'text',
            'title' => esc_html__('Button Text', 'khkplrv'),
            'info'  => esc_html__('Enter BACK TO HOME button text. If you want to change it.', 'khkplrv'),
          ),
          // End 404 Page

        ) // end: fields
      ), // end: fields section

      // maintenance mode page
      array(
        'name'     => 'maintenance_mode_section',
        'title'    => esc_html__('Maintenance Mode', 'khkplrv'),
        'icon'     => 'fa fa-hourglass-half',
        'fields'   => array(

          // Start Maintenance Mode
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('If you turn this ON : Only Logged in users will see your pages. All other visiters will see, selected page of : <strong>Maintenance Mode Page</strong>', 'khkplrv')
          ),
          array(
            'id'             => 'enable_maintenance_mode',
            'type'           => 'switcher',
            'title'          => esc_html__('Maintenance Mode', 'khkplrv'),
            'default'        => false,
          ),
          array(
            'id'             => 'maintenance_mode_page',
            'type'           => 'select',
            'title'          => esc_html__('Maintenance Mode Page', 'khkplrv'),
            'options'        => 'pages',
            'default_option' => esc_html__('Select a page', 'khkplrv'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          array(
            'id'             => 'maintenance_mode_title',
            'type'           => 'text',
            'title'          => esc_html__('Maintenance Mode Text', 'khkplrv'),
          ),
          array(
            'id'             => 'maintenance_mode_text',
            'type'           => 'textarea',
            'title'          => esc_html__('Maintenance Mode Text', 'khkplrv'),
          ),
          array(
            'id'             => 'maintenance_mode_bg',
            'type'           => 'background',
            'title'          => esc_html__('Page Background', 'khkplrv'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          // End Maintenance Mode

        ) // end: fields
      ), // end: fields section

    )
  );

  // ------------------------------
  // Advanced
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_advanced',
    'title'  => esc_html__('Advanced', 'khkplrv'),
    'icon'   => 'fa fa-cog'
  );

  // ------------------------------
  // Misc Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'misc_section',
    'title'    => esc_html__('Misc', 'khkplrv'),
    'icon'     => 'fa fa-recycle',
    'sections' => array(

      // custom sidebar section
      array(
        'name'     => 'custom_sidebar_section',
        'title'    => esc_html__('Custom Sidebar', 'khkplrv'),
        'icon'     => 'fa fa-reorder',
        'fields'   => array(

          // start fields
          array(
            'id'              => 'custom_sidebar',
            'title'           => esc_html__('Sidebars', 'khkplrv'),
            'desc'            => esc_html__('Go to Appearance -> Widgets after create sidebars', 'khkplrv'),
            'type'            => 'group',
            'fields'          => array(
              array(
                'id'          => 'sidebar_name',
                'type'        => 'text',
                'title'       => esc_html__('Sidebar Name', 'khkplrv'),
              ),
              array(
                'id'          => 'sidebar_desc',
                'type'        => 'text',
                'title'       => esc_html__('Custom Description', 'khkplrv'),
              )
            ),
            'accordion'       => true,
            'button_title'    => esc_html__('Add New Sidebar', 'khkplrv'),
            'accordion_title' => esc_html__('New Sidebar', 'khkplrv'),
          ),
          // end fields

        )
      ),
      // custom sidebar section

      // Custom CSS/JS
      array(
        'name'        => 'custom_css_js_section',
        'title'       => esc_html__('Custom Codes', 'khkplrv'),
        'icon'        => 'fa fa-code',

        // begin: fields
        'fields'      => array(
          // Start Custom CSS/JS
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Custom JS', 'khkplrv')
          ),
          array(
            'id'             => 'theme_custom_js',
            'type'           => 'textarea',
            'attributes' => array(
              'rows'     => 10,
              'placeholder'     => esc_html__('Enter your JS code here...', 'khkplrv'),
            ),
          ),
          // End Custom CSS/JS

        ) // end: fields
      ),

      // Translation
      array(
        'name'        => 'theme_translation_section',
        'title'       => esc_html__('Translation', 'khkplrv'),
        'icon'        => 'fa fa-language',

        // begin: fields
        'fields'      => array(

          // Start Translation
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Common Texts', 'khkplrv')
          ),
          array(
            'id'          => 'read_more_text',
            'type'        => 'text',
            'title'       => esc_html__('Read More Text', 'khkplrv'),
          ),
          array(
            'id'          => 'view_more_text',
            'type'        => 'text',
            'title'       => esc_html__('View More Text', 'khkplrv'),
          ),
          array(
            'id'          => 'share_text',
            'type'        => 'text',
            'title'       => esc_html__('Share Text', 'khkplrv'),
          ),
          array(
            'id'          => 'share_on_text',
            'type'        => 'text',
            'title'       => esc_html__('Share On Tooltip Text', 'khkplrv'),
          ),
          array(
            'id'          => 'author_text',
            'type'        => 'text',
            'title'       => esc_html__('Author Text', 'khkplrv'),
          ),
          array(
            'id'          => 'post_comment_text',
            'type'        => 'text',
            'title'       => esc_html__('Post Comment Text [Submit Button]', 'khkplrv'),
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('WooCommerce', 'khkplrv')
          ),
          array(
            'id'          => 'add_to_cart_text',
            'type'        => 'text',
            'title'       => esc_html__('Add to Cart Text', 'khkplrv'),
          ),
          array(
            'id'          => 'details_text',
            'type'        => 'text',
            'title'       => esc_html__('Details Text', 'khkplrv'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Pagination', 'khkplrv')
          ),
          array(
            'id'          => 'older_post',
            'type'        => 'text',
            'title'       => esc_html__('Older Posts Text', 'khkplrv'),
          ),
          array(
            'id'          => 'newer_post',
            'type'        => 'text',
            'title'       => esc_html__('Newer Posts Text', 'khkplrv'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Single Portfolio Pagination', 'khkplrv')
          ),
          array(
            'id'          => 'prev_port',
            'type'        => 'text',
            'title'       => esc_html__('Prev Case Text', 'khkplrv'),
          ),
          array(
            'id'          => 'next_port',
            'type'        => 'text',
            'title'       => esc_html__('Next Case Text', 'khkplrv'),
          ),
          // End Translation

        ) // end: fields
      ),

    ),
  );

  // ------------------------------
  // envato account
  // ------------------------------
  $options[]   = array(
    'name'     => 'envato_account_section',
    'title'    => esc_html__('Envato Account', 'khkplrv'),
    'icon'     => 'fa fa-link',
    'fields'   => array(

      array(
        'type'    => 'notice',
        'class'   => 'warning',
        'content' => esc_html__('Enter your Username and API key. You can get update our themes from WordPress admin itself.', 'khkplrv'),
      ),
      array(
        'id'      => 'themeforest_irstheme',
        'type'    => 'text',
        'title'   => esc_html__('Envato Username', 'khkplrv'),
      ),
      array(
        'id'      => 'themeforest_api',
        'type'    => 'text',
        'title'   => esc_html__('Envato API Key', 'khkplrv'),
        'class'   => 'text-security',
        'after'   => __('<p>This is not a password field. Enter your Envato API key, which is located in : <strong>http://themeforest.net/user/[YOUR-USER-NAME]/api_keys/edit</strong></p>', 'khkplrv')
      ),

    )
  );

  // ------------------------------
  // backup                       -
  // ------------------------------
  $options[]   = array(
    'name'     => 'backup_section',
    'title'    => 'Backup',
    'icon'     => 'fa fa-shield',
    'fields'   => array(

      array(
        'type'    => 'notice',
        'class'   => 'warning',
        'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'khkplrv'),
      ),

      array(
        'type'    => 'backup',
      ),

    )
  );

  return $options;

}
add_filter( 'cs_framework_options', 'khkplrv_options' );