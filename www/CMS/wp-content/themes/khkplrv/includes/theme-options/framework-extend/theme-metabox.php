<?php
/*
 * All Metabox related options for Khkplrv theme.
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */

function khkplrv_metabox_options( $options ) {

 $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );
    $contact_forms = array();
    if ( $cf7 ) {
      foreach ( $cf7 as $cform ) {
        $contact_forms[ $cform->ID ] = $cform->post_title;
      }
    } else {
      $contact_forms[ esc_html__( 'No contact forms found', 'khkplrv' ) ] = 0;
    }
  $options      = array();

  // -----------------------------------------
  // Post Metabox Options                    -
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'post_type_metabox',
    'title'     => esc_html__('Post Options', 'khkplrv'),
    'post_type' => 'post',
    'context'   => 'normal',
    'priority'  => 'default',
    'sections'  => array(

      // All Post Formats
      array(
        'name'   => 'section_post_formats',
        'fields' => array(

          // Standard, Image
          array(
            'title' => 'Standard Image',
            'type'  => 'subheading',
            'content' => esc_html__('There is no Extra Option for this Post Format!', 'khkplrv'),
            'wrap_class' => 'khkplrv-minimal-heading hide-title',
          ),
          // Standard, Image

          // Gallery
          array(
            'type'    => 'notice',
            'title'   => 'Gallery Format',
            'wrap_class' => 'hide-title',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Gallery Format', 'khkplrv')
          ),
          array(
            'id'          => 'gallery_post_format',
            'type'        => 'gallery',
            'title'       => esc_html__('Add Gallery', 'khkplrv'),
            'add_title'   => esc_html__('Add Image(s)', 'khkplrv'),
            'edit_title'  => esc_html__('Edit Image(s)', 'khkplrv'),
            'clear_title' => esc_html__('Clear Image(s)', 'khkplrv'),
          ),
          array(
            'type'    => 'text',
            'title'   => esc_html__('Add Video URL', 'khkplrv' ),
            'id'   => 'video_post_format',
            'desc' => esc_html__('Add youtube or vimeo video link', 'khkplrv' ),
            'wrap_class' => 'video_post_format',
          ),
          array(
            'type'    => 'icon',
            'title'   => esc_html__('Add Quote Icon', 'khkplrv' ),
            'id'   => 'quote_post_format',
            'desc' => esc_html__('Add Quote Icon here', 'khkplrv' ),
            'wrap_class' => 'quote_post_format',
          ),
          // Gallery

        ),
      ),

    ),
  );

  // -----------------------------------------
  // Page Metabox Options                    -
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'page_type_metabox',
    'title'     => esc_html__('Page Custom Options', 'khkplrv'),
    'post_type' => array('post', 'page'),
    'context'   => 'normal',
    'priority'  => 'default',
    'sections'  => array(

      // Title Section
      array(
        'name'  => 'page_topbar_section',
        'title' => esc_html__('Top Bar', 'khkplrv'),
        'icon'  => 'fa fa-minus',

        // Fields Start
        'fields' => array(

          array(
            'id'           => 'topbar_options',
            'type'         => 'image_select',
            'title'        => esc_html__('Topbar', 'khkplrv'),
            'options'      => array(
              'default'     => KHKPLRV_CS_IMAGES .'/topbar-default.png',
              'custom'      => KHKPLRV_CS_IMAGES .'/topbar-custom.png',
              'hide_topbar' => KHKPLRV_CS_IMAGES .'/topbar-hide.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'hide_topbar_select',
            ),
            'radio'     => true,
            'default'   => 'default',
          ),
          array(
            'id'          => 'top_left',
            'type'        => 'textarea',
            'title'       => esc_html__('Top Left', 'khkplrv'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
            'shortcode'       => true,
          ),
          array(
            'id'          => 'top_right',
            'type'        => 'textarea',
            'title'       => esc_html__('Top Right', 'khkplrv'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
            'shortcode'       => true,
          ),
          array(
            'id'    => 'topbar_bg',
            'type'  => 'color_picker',
            'title' => esc_html__('Topbar Background Color', 'khkplrv'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
          ),
          array(
            'id'    => 'topbar_border',
            'type'  => 'color_picker',
            'title' => esc_html__('Topbar Border Color', 'khkplrv'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
          ),

        ), // End : Fields

      ), // Title Section

      // Header
      array(
        'name'  => 'header_section',
        'title' => esc_html__('Header', 'khkplrv'),
        'icon'  => 'fa fa-bars',
        'fields' => array(

          array(
            'id'           => 'select_header_design',
            'type'         => 'image_select',
            'title'        => esc_html__('Select Header Design', 'khkplrv'),
            'options'      => array(
              'default'     => KHKPLRV_CS_IMAGES .'/hs-0.png',
              'style_one'   => KHKPLRV_CS_IMAGES .'/hs-1.png',
              'style_two'   => KHKPLRV_CS_IMAGES .'/hs-2.png',
              'style_three'   => KHKPLRV_CS_IMAGES .'/hs-2.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'header_design',
            ),
            'radio'     => true,
            'default'   => 'default',
            'info'      => esc_html__('Select your header design, following options will may differ based on your selection of header design.', 'khkplrv'),
          ),
         array(
            'id'    => 'header_middle',
            'type'  => 'textarea',
            'title' => esc_html__('Header Middle', 'khkplrv'),
            'info' => esc_html__('Header Middle Content for Header style One and Four.', 'khkplrv'),
            'shortcode'   => true,
            'dependency'   => array('header_design', 'any', 'style_two'),
          ),
           array(
            'id'    => 'khkplrv_cart_widget',
            'type'  => 'switcher',
            'title' => esc_html__('Header Cart', 'khkplrv'),
            'info' => esc_html__('Turn On if you want to Show Header Cart .', 'khkplrv'),
            'default' => false,
          ),
          array(
            'id'    => 'khkplrv_search',
            'type'  => 'switcher',
            'title' => esc_html__('Header Search', 'khkplrv'),
            'info' => esc_html__('Turn On if you want to hide Header Search .', 'khkplrv'),
            'default' => false,
          ),
          array(
            'id'    => 'transparency_header',
            'type'  => 'switcher',
            'title' => esc_html__('Transparent Header', 'khkplrv'),
            'info' => esc_html__('Use Transparent Method', 'khkplrv'),
          ),
          array(
            'id'    => 'transparent_menu_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Menu Color', 'khkplrv'),
            'info' => esc_html__('Pick your menu color. This color will only apply for non-sticky header mode.', 'khkplrv'),
            'dependency'   => array('transparency_header', '==', 'true'),
          ),
          array(
            'id'    => 'transparent_menu_hover_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Menu Hover Color', 'khkplrv'),
            'info' => esc_html__('Pick your menu hover color. This color will only apply for non-sticky header mode.', 'khkplrv'),
            'dependency'   => array('transparency_header', '==', 'true'),
          ),
          array(
            'id'             => 'choose_menu',
            'type'           => 'select',
            'title'          => esc_html__('Choose Menu', 'khkplrv'),
            'desc'          => esc_html__('Choose custom menus for this page.', 'khkplrv'),
            'options'        => 'menus',
            'default_option' => esc_html__('Select your menu', 'khkplrv'),
          ),

          // Enable & Disable
          array(
            'type'    => 'notice',
            'class'   => 'info cs-khkplrv-heading',
            'content' => esc_html__('Enable & Disable', 'khkplrv'),
            'dependency' => array('header_design', '!=', 'default'),
          ),
          array(
            'id'    => 'sticky_header',
            'type'  => 'switcher',
            'title' => esc_html__('Sticky Header', 'khkplrv'),
            'info' => esc_html__('Turn On if you want your naviagtion bar on sticky.', 'khkplrv'),
            'default' => true,
            'dependency' => array('header_design', '!=', 'default'),
          ),
        ),
      ),
      // Header

      // Banner & Title Area
      array(
        'name'  => 'banner_title_section',
        'title' => esc_html__('Banner & Title Area', 'khkplrv'),
        'icon'  => 'fa fa-bullhorn',
        'fields' => array(

          array(
            'id'        => 'banner_type',
            'type'      => 'select',
            'title'     => esc_html__('Choose Banner Type', 'khkplrv'),
            'options'   => array(
              'default-title'    => 'Default Title',
              'revolution-slider' => 'Shortcode [Rev Slider]',
              'hide-title-area'   => 'Hide Title/Banner Area',
            ),
          ),
          array(
            'id'    => 'page_revslider',
            'type'  => 'textarea',
            'title' => esc_html__('Revolution Slider or Any Shortcodes', 'khkplrv'),
            'desc' => __('Enter any shortcodes that you want to show in this page title area. <br />Eg : Revolution Slider shortcode.', 'khkplrv'),
            'attributes' => array(
              'placeholder' => esc_html__('Enter your shortcode...', 'khkplrv'),
            ),
            'dependency'   => array('banner_type', '==', 'revolution-slider'),
          ),
          array(
            'id'    => 'page_custom_title',
            'type'  => 'text',
            'title' => esc_html__('Custom Title', 'khkplrv'),
            'attributes' => array(
              'placeholder' => esc_html__('Enter your custom title...', 'khkplrv'),
            ),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'        => 'title_area_spacings',
            'type'      => 'select',
            'title'     => esc_html__('Title Area Spacings', 'khkplrv'),
            'options'   => array(
              'padding-default' => esc_html__('Default Spacing', 'khkplrv'),
              'padding-custom' => esc_html__('Custom Padding', 'khkplrv'),
            ),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'    => 'title_top_spacings',
            'type'  => 'text',
            'title' => esc_html__('Top Spacing', 'khkplrv'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('banner_type|title_area_spacings', '==|==', 'default-title|padding-custom'),
          ),
          array(
            'id'    => 'title_bottom_spacings',
            'type'  => 'text',
            'title' => esc_html__('Bottom Spacing', 'khkplrv'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('banner_type|title_area_spacings', '==|==', 'default-title|padding-custom'),
          ),
          array(
            'id'    => 'title_area_bg',
            'type'  => 'background',
            'title' => esc_html__('Background', 'khkplrv'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'    => 'titlebar_bg_overlay_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Overlay Color', 'khkplrv'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'    => 'title_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Title Color', 'khkplrv'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),

        ),
      ),
      // Banner & Title Area

      // Content Section
      array(
        'name'  => 'page_content_options',
        'title' => esc_html__('Content Options', 'khkplrv'),
        'icon'  => 'fa fa-file',

        'fields' => array(

          array(
            'id'        => 'content_spacings',
            'type'      => 'select',
            'title'     => esc_html__('Content Spacings', 'khkplrv'),
            'options'   => array(
              'padding-default' => esc_html__('Default Spacing', 'khkplrv'),
              'padding-custom' => esc_html__('Custom Padding', 'khkplrv'),
            ),
            'desc' => esc_html__('Content area top and bottom spacings.', 'khkplrv'),
          ),
          array(
            'id'    => 'content_top_spacings',
            'type'  => 'text',
            'title' => esc_html__('Top Spacing', 'khkplrv'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('content_spacings', '==', 'padding-custom'),
          ),
          array(
            'id'    => 'content_bottom_spacings',
            'type'  => 'text',
            'title' => esc_html__('Bottom Spacing', 'khkplrv'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('content_spacings', '==', 'padding-custom'),
          ),
        ), // End Fields
      ), // Content Section

      // Enable & Disable
      array(
        'name'  => 'hide_show_section',
        'title' => esc_html__('Enable & Disable', 'khkplrv'),
        'icon'  => 'fa fa-toggle-on',
        'fields' => array(

          array(
            'id'    => 'hide_header',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Header', 'khkplrv'),
            'label' => esc_html__('Yes, Please do it.', 'khkplrv'),
          ),
          array(
            'id'    => 'hide_breadcrumbs',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Breadcrumbs', 'khkplrv'),
            'label' => esc_html__('Yes, Please do it.', 'khkplrv'),
          ),
          array(
            'id'    => 'hide_footer',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Footer', 'khkplrv'),
            'label' => esc_html__('Yes, Please do it.', 'khkplrv'),
          ),
          array(
            'id'    => 'box_style_page',
            'type'  => 'switcher',
            'title' => esc_html__('Box Style', 'khkplrv'),
            'label' => esc_html__('Yes, Please do it.', 'khkplrv'),
          ),

        ),
      ),
      // Enable & Disable

    ),
  );

  // -----------------------------------------
  // Page Layout
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'page_layout_options',
    'title'     => esc_html__('Page Layout', 'khkplrv'),
    'post_type' => 'page',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'page_layout_section',
        'fields' => array(

          array(
            'id'        => 'page_layout',
            'type'      => 'image_select',
            'options'   => array(
              'full-width'    => KHKPLRV_CS_IMAGES . '/page-1.png',
              'extra-width'   => KHKPLRV_CS_IMAGES . '/page-2.png',
              'left-sidebar'  => KHKPLRV_CS_IMAGES . '/page-3.png',
              'right-sidebar' => KHKPLRV_CS_IMAGES . '/page-4.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'page_layout',
            ),
            'default'    => 'full-width',
            'radio'      => true,
            'wrap_class' => 'text-center',
          ),
          array(
            'id'            => 'page_sidebar_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'khkplrv'),
            'options'        => khkplrv_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'khkplrv'),
            'dependency'   => array('page_layout', 'any', 'left-sidebar,right-sidebar'),
          ),

        ),
      ),

    ),
  );


  // -----------------------------------------
  // Testimonial
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'testimonial_options',
    'title'     => esc_html__('Testimonial Client', 'khkplrv'),
    'post_type' => 'testimonial',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'testimonial_option_section',
        'fields' => array(

          array(
            'id'      => 'testi_name',
            'type'    => 'text',
            'title'   => esc_html__('Name', 'khkplrv'),
            'info'    => esc_html__('Enter client name', 'khkplrv'),
          ),
          array(
            'id'      => 'testi_name_link',
            'type'    => 'text',
            'title'   => esc_html__('Name Link', 'khkplrv'),
            'info'    => esc_html__('Enter client name link, if you want', 'khkplrv'),
          ),
          array(
            'id'      => 'testi_pro',
            'type'    => 'text',
            'title'   => esc_html__('Profession', 'khkplrv'),
            'info'    => esc_html__('Enter client profession', 'khkplrv'),
          ),
          array(
            'id'      => 'testi_pro_link',
            'type'    => 'text',
            'title'   => esc_html__('Profession Link', 'khkplrv'),
            'info'    => esc_html__('Enter client profession link', 'khkplrv'),
          ),

        ),
      ),

    ),
  );

 // -----------------------------------------
  // Service
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'service_options',
    'title'     => esc_html__('Service Extra Options', 'khkplrv'),
    'post_type' => 'service',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'service_option_section',
        'fields' => array(
         array(
            'id'    => 'service_title',
            'type'  => 'text',
            'title' => esc_html__('Service Title', 'khkplrv'),
            'info'    => esc_html__('Enter Service Title for Service Item.', 'khkplrv'),
          ),
         array(
            'id'    => 'service_number',
            'type'  => 'text',
            'title' => esc_html__('Service Number', 'khkplrv'),
            'info'    => esc_html__('Enter Service Number for Service Item.', 'khkplrv'),
          ),
         array(
            'id'    => 'service_icon',
            'type'  => 'icon',
            'title' => esc_html__('Service icon', 'khkplrv'),
            'info'    => esc_html__('Enter Service icon for Service Item.', 'khkplrv'),
          ),
          array(
            'id'           => 'service_image',
            'type'         => 'image',
            'title'        => esc_html__('Service Image', 'khkplrv'),
            'add_title' => esc_html__('Add Service Image', 'khkplrv'),
          ),
        ),
      ),

    ),
  );


  // -----------------------------------------
  // Project
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'project_options',
    'title'     => esc_html__('Project Extra Options', 'khkplrv'),
    'post_type' => 'project',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'project_option_section',
        'fields' => array(
          array(
            'id'           => 'project_title',
            'type'         => 'text',
            'title'        => esc_html__('Project title', 'khkplrv'),
            'add_title' => esc_html__('Add Project title', 'khkplrv'),
            'attributes' => array(
              'placeholder' => esc_html__('Project Title', 'khkplrv'),
            ),
            'info'    => esc_html__('Write Project Title.', 'khkplrv'),
          ),
          array(
            'id'      => 'project_subtitle',
            'type'    => 'text',
            'attributes' => array(
              'placeholder' => esc_html__('Project : Sub Title', 'khkplrv'),
            ),
            'info'    => esc_html__('Write Project Sub Title.', 'khkplrv'),
          ),
          array(
            'id'           => 'project_image',
            'type'         => 'image',
            'title'        => esc_html__('Project Grid Image', 'khkplrv'),
            'add_title' => esc_html__('Add Project Grid Image', 'khkplrv'),
          ),
          array(
            'id'           => 'project_gallery',
            'type'         => 'gallery',
            'title'        => esc_html__('Project Single gallery', 'khkplrv'),
            'add_title' => esc_html__('Add Project Single gallery', 'khkplrv'),
          ),
        ),
      ),

    ),
  );

  // -----------------------------------------
  // post options
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'post_options',
    'title'     => esc_html__('Grid Image', 'khkplrv'),
    'post_type' => 'post',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(
      array(
        'name'   => 'post_option_section',
        'fields' => array(
          array(
            'id'           => 'widget_post_title',
            'type'         => 'text',
            'title'        => esc_html__('Widget Post Title', 'khkplrv'),
            'add_title' => esc_html__('Add Widget Post Title here', 'khkplrv'),
          ),
          array(
            'id'           => 'grid_image',
            'type'         => 'image',
            'title'        => esc_html__('Post Grid Image', 'khkplrv'),
            'add_title' => esc_html__('Add Post Grid Image', 'khkplrv'),
          ),
        ),
      ),

    ),
  );


  return $options;

}
add_filter( 'cs_metabox_options', 'khkplrv_metabox_options' );