<?php
/*
 * All CSS and JS files are enqueued from this file
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */

/**
 * Enqueue Files for FrontEnd
 */
function khkplrv_google_font_url() {
    $font_url = '';
    if ( 'off' !== esc_html__( 'on', 'khkplrv' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Roboto:400,400i,500|Barlow Condensed:500,600,700' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}

if ( ! function_exists( 'khkplrv_scripts_styles' ) ) {
  function khkplrv_scripts_styles() {

    // Styles
    wp_enqueue_style( 'themify-icons', KHKPLRV_CSS .'/themify-icons.css', array(), '4.6.3', 'all' );
    wp_enqueue_style( 'flaticon', KHKPLRV_CSS .'/flaticon.css', array(), '1.0.0', 'all' );
    wp_enqueue_style( 'bootstrap', KHKPLRV_CSS .'/bootstrap.min.css', array(), '3.3.7', 'all' );
    wp_enqueue_style( 'animate', KHKPLRV_CSS .'/animate.css', array(), '3.5.1', 'all' );
    wp_enqueue_style( 'owl-carousel', KHKPLRV_CSS .'/owl.carousel.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'owl-theme', KHKPLRV_CSS .'/owl.theme.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'slick', KHKPLRV_CSS .'/slick.css', array(), '1.6.0', 'all' );
    wp_enqueue_style( 'slick-theme', KHKPLRV_CSS .'/slick-theme.css', array(), '1.6.0', 'all' );
    wp_enqueue_style( 'owl-transitions', KHKPLRV_CSS .'/owl.transitions.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'fancybox', KHKPLRV_CSS .'/fancybox.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'odometer', KHKPLRV_CSS .'/odometer.css', array(), '0.4.8', 'all' );
    wp_enqueue_style( 'perfect-scrollbar', KHKPLRV_CSS .'/perfect-scrollbar.css', array(), '1.4.0', 'all' );
    wp_enqueue_style( 'khkplrv-style', KHKPLRV_CSS .'/styles.css', array(), KHKPLRV_VERSION, 'all' );
    wp_enqueue_style( 'element', KHKPLRV_CSS .'/elements.css', array(), KHKPLRV_VERSION, 'all' );
    wp_style_add_data( 'rtel-style', 'rtl', 'replace' );
    if ( !function_exists('cs_framework_init') ) {
      wp_enqueue_style('khkplrv-default-style', get_template_directory_uri() . '/style.css', array(),  KHKPLRV_VERSION, 'all' );
    }
    wp_enqueue_style( 'khkplrv-default-google-fonts', khkplrv_google_font_url(), array(), KHKPLRV_VERSION, 'all' );
    // Scripts
    wp_enqueue_script( 'bootstrap', KHKPLRV_SCRIPTS . '/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );
    wp_enqueue_script( 'imagesloaded');
    wp_enqueue_script( 'isotope', KHKPLRV_SCRIPTS . '/isotope.min.js', array( 'jquery' ), '2.2.2', true );
    wp_enqueue_script( 'fancybox', KHKPLRV_SCRIPTS . '/fancybox.min.js', array( 'jquery' ), '2.1.5', true );
    wp_enqueue_script( 'masonry');
    wp_enqueue_script( 'owl-carousel', KHKPLRV_SCRIPTS . '/owl-carousel.js', array( 'jquery' ), '2.0.0', true );
    wp_enqueue_script( 'jquery-easing', KHKPLRV_SCRIPTS . '/jquery-easing.js', array( 'jquery' ), '1.4.0', true );
    wp_enqueue_script( 'wow', KHKPLRV_SCRIPTS . '/wow.min.js', array( 'jquery' ), '1.4.0', true );
    wp_enqueue_script( 'magnific-popup', KHKPLRV_SCRIPTS . '/magnific-popup.js', array( 'jquery' ), '1.1.0', true );
    wp_enqueue_script( 'slick-slider', KHKPLRV_SCRIPTS . '/slick-slider.js', array( 'jquery' ), '1.6.0', true );
    wp_enqueue_script( 'wc-quantity-increment', KHKPLRV_SCRIPTS . '/wc-quantity-increment.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'perfect-scrollbar', KHKPLRV_SCRIPTS . '/perfect-scrollbar.js', array( 'jquery' ), '1.4.0', true );
    wp_enqueue_script( 'odometer', KHKPLRV_SCRIPTS . '/odometer.min.js', array( 'jquery' ), '1.4.0', true );
    wp_enqueue_script( 'khkplrv-scripts', KHKPLRV_SCRIPTS . '/scripts.js', array( 'jquery' ), KHKPLRV_VERSION, true );
    // Comments
    wp_enqueue_script( 'khkplrv-validate-js', KHKPLRV_SCRIPTS . '/jquery.validate.min.js', array( 'jquery' ), '1.9.0', true );
    wp_add_inline_script( 'khkplrv-validate-js', 'jQuery(document).ready(function($) {$("#commentform").validate({rules: {author: {required: true,minlength: 2},email: {required: true,email: true},comment: {required: true,minlength: 10}}});});' );

    // Responsive Active
    $khkplrv_viewport = cs_get_option('theme_responsive');
    if( !$khkplrv_viewport ) {
      wp_enqueue_style( 'khkplrv-responsive', KHKPLRV_CSS .'/responsive.css', array(), KHKPLRV_VERSION, 'all' );
    }

    // Adds support for pages with threaded comments
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }

  }
  add_action( 'wp_enqueue_scripts', 'khkplrv_scripts_styles' );
}

/**
 * Enqueue Files for BackEnd
 */
if ( ! function_exists( 'khkplrv_admin_scripts_styles' ) ) {
  function khkplrv_admin_scripts_styles() {

    wp_enqueue_style( 'khkplrv-admin-main', KHKPLRV_CSS . '/admin-styles.css', true );
    wp_enqueue_style( 'flaticon', KHKPLRV_CSS . '/flaticon.css', true );
    wp_enqueue_script( 'khkplrv-admin-scripts', KHKPLRV_SCRIPTS . '/admin-scripts.js', true );

  }
  add_action( 'admin_enqueue_scripts', 'khkplrv_admin_scripts_styles' );
}
