<?php
/*
 * All Khkplrv Theme Related Functions Files are Linked here
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */

/* Theme All Khkplrv Setup */
require_once( KHKPLRV_FRAMEWORK . '/theme-support.php' );
require_once( KHKPLRV_FRAMEWORK . '/backend-functions.php' );
require_once( KHKPLRV_FRAMEWORK . '/frontend-functions.php' );
require_once( KHKPLRV_FRAMEWORK . '/enqueue-files.php' );
require_once( KHKPLRV_CS_FRAMEWORK . '/custom-style.php' );
require_once( KHKPLRV_CS_FRAMEWORK . '/config.php' );

/* Install Plugins */
require_once( KHKPLRV_FRAMEWORK . '/theme-options/plugins/activation.php' );

/* Breadcrumbs */
require_once( KHKPLRV_FRAMEWORK . '/theme-options/plugins/breadcrumb-trail.php' );

/* Aqua Resizer */
require_once( KHKPLRV_FRAMEWORK . '/theme-options/plugins/aq_resizer.php' );

/* Bootstrap Menu Walker */
require_once( KHKPLRV_FRAMEWORK . '/core/wp_bootstrap_navwalker.php' );

/* Sidebars */
require_once( KHKPLRV_FRAMEWORK . '/core/sidebars.php' );

if ( class_exists( 'WooCommerce' ) ) :
	require_once( KHKPLRV_FRAMEWORK . '/woocommerce-extend.php' );
endif;