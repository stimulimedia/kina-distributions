<?php
/*
 * The sidebar containing the main widget area.
 * Author & Copyright: irstheme
 * URL: http://themeforest.net/user/irstheme
 */
$khkplrv_blog_widget = cs_get_option( 'blog_widget' );
$khkplrv_single_blog_widget = cs_get_option( 'single_blog_widget' );
$khkplrv_service_widget = cs_get_option( 'single_service_widget' );
$khkplrv_service_sidebar_position = cs_get_option( 'service_sidebar_position' );
$khkplrv_project_sidebar_position = cs_get_option( 'project_sidebar_position' );
$khkplrv_project_widget = cs_get_option( 'single_project_widget' );
$khkplrv_blog_sidebar_position = cs_get_option( 'blog_sidebar_position' );
$khkplrv_sidebar_position = cs_get_option( 'single_sidebar_position' );
$woo_widget = cs_get_option('woo_widget');
$khkplrv_page_layout_shop = cs_get_option( 'woo_sidebar_position' );
$shop_sidebar_position = ( is_woocommerce_shop() ) ? $khkplrv_page_layout_shop : '';
if ( is_home() || is_archive() || is_search() ) {
	$khkplrv_blog_sidebar_position = $khkplrv_blog_sidebar_position;
} else {
	$khkplrv_blog_sidebar_position = '';
}
if ( is_single() ) {
	$khkplrv_sidebar_position = $khkplrv_sidebar_position;
} else {
	$khkplrv_sidebar_position = '';
}

if ( is_singular( 'service' ) ) {
	$khkplrv_service_sidebar_position = $khkplrv_service_sidebar_position;
} else {
	$khkplrv_service_sidebar_position = '';
}
if ( is_singular( 'project' ) ) {
	$khkplrv_project_sidebar_position = $khkplrv_project_sidebar_position;
} else {
	$khkplrv_project_sidebar_position = '';
}
if ( is_page() ) {
	// Page Layout Options
	$khkplrv_page_layout = get_post_meta( get_the_ID(), 'page_layout_options', true );
	if ( $khkplrv_page_layout ) {
		$khkplrv_page_sidebar_pos = $khkplrv_page_layout['page_layout'];
	} else {
		$khkplrv_page_sidebar_pos = '';
	}
} else {
	$khkplrv_page_sidebar_pos = '';
}
if (isset($_GET['sidebar'])) {
  $khkplrv_blog_sidebar_position = $_GET['sidebar'];
}
// sidebar class
if ( $khkplrv_sidebar_position === 'sidebar-left' || $khkplrv_page_sidebar_pos == 'left-sidebar' || $khkplrv_blog_sidebar_position === 'sidebar-left' || $khkplrv_project_sidebar_position === 'sidebar-left' ||  $khkplrv_service_sidebar_position === 'sidebar-left' ) {
	$col_class = 'col-md-pull-8';
} else {
	$col_class = '';
}

if ( $khkplrv_service_sidebar_position === 'sidebar-left' ) {
	$sv_push_class = ' col-lg-pull-9';
} else {
	$sv_push_class = '';
}
if (  $shop_sidebar_position == 'left-sidebar' ) {
	$shop_push_class = ' col-lg-pull-9';
} else {
	$shop_push_class = '';
}

if ( is_singular( 'service' ) ) {
	$service_col = ' service-sidebar col-lg-3';
} else {
	$service_col = '';
}
if (  class_exists( 'WooCommerce' ) && is_shop() ) {
	$shop_col = ' shop-sidebar col-lg-3';
} else {
	$shop_col = '';
}
?>
<div class="blog-sidebar col-md-4 <?php echo esc_attr( $col_class.$service_col.$sv_push_class.$shop_col.$shop_push_class ); ?>">
	<?php
	if (is_page() && isset( $khkplrv_page_layout['page_sidebar_widget'] ) && !empty( $khkplrv_page_layout['page_sidebar_widget'] ) ) {
		if ( is_active_sidebar( $khkplrv_page_layout['page_sidebar_widget'] ) ) {
			dynamic_sidebar( $khkplrv_page_layout['page_sidebar_widget'] );
		}
	} elseif (!is_page() && $khkplrv_blog_widget && !$khkplrv_single_blog_widget) {
		if ( is_active_sidebar( $khkplrv_blog_widget ) ) {
			dynamic_sidebar( $khkplrv_blog_widget );
		}
	}  elseif ( $khkplrv_service_widget && is_singular( 'service' ) ) {
		if ( is_active_sidebar( $khkplrv_service_widget ) ) {
			dynamic_sidebar( $khkplrv_service_widget );
		}
	} elseif ( $khkplrv_project_widget && is_singular( 'project' ) ) {
		if ( is_active_sidebar( $khkplrv_project_widget ) ) {
			dynamic_sidebar( $khkplrv_project_widget );
		}
	}  elseif (is_woocommerce_shop() && $woo_widget) {
		if (is_active_sidebar($woo_widget)) {
			dynamic_sidebar($woo_widget);
		}
	} elseif ( is_single() && $khkplrv_single_blog_widget ) {
		if ( is_active_sidebar( $khkplrv_single_blog_widget ) ) {
			dynamic_sidebar( $khkplrv_single_blog_widget );
		}
	} else {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			dynamic_sidebar( 'sidebar-1' );
		}
	} ?>
</div><!-- #secondary -->
