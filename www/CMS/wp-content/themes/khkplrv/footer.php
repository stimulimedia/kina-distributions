<?php
/*
 * The template for displaying the footer.
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */

$khkplrv_id    = ( isset( $post ) ) ? $post->ID : 0;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
$khkplrv_ft_bg = cs_get_option('khkplrv_ft_bg');
$khkplrv_attachment = wp_get_attachment_image_src( $khkplrv_ft_bg , 'full' );

if ( $khkplrv_meta ) {
	$khkplrv_hide_footer  = $khkplrv_meta['hide_footer'];
} else { $khkplrv_hide_footer = ''; }
if ( !$khkplrv_hide_footer ) { // Hide Footer Metabox
	$hide_copyright = cs_get_option('hide_copyright');
?>
	<!-- Footer -->
	<footer class="site-footer" style="background-image: url( <?php echo esc_url( $khkplrv_attachment[0] ); ?>);">
		<?php
			$footer_widget_block = cs_get_option( 'footer_widget_block' );
			if ( $footer_widget_block ) {
	      get_template_part( 'theme-layouts/footer/footer', 'widgets' );
	    }
			if ( !$hide_copyright ) {
      	get_template_part( 'theme-layouts/footer/footer', 'copyright' );
	    }
    ?>
	</footer>
	<!-- Footer -->
<?php } // Hide Footer Metabox ?>
</div><!--khkplrv-theme-wrapper -->
<?php wp_footer(); ?>
</body>
</html>
