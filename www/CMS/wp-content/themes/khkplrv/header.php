<?php
/*
 * The header for our theme.
 * Author & Copyright:IRS Theme
 * URL: http://themeforest.net/user/irstheme
 */
?><!DOCTYPE html>
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php
$khkplrv_viewport = cs_get_option( 'theme_responsive' );
if( !$khkplrv_viewport ) { ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php } $khkplrv_all_element_color  = cs_get_customize_option( 'all_element_colors' ); ?>
<meta name="msapplication-TileColor" content="<?php echo esc_attr( $khkplrv_all_element_color ); ?>">
<meta name="theme-color" content="<?php echo esc_attr( $khkplrv_all_element_color ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php
// Metabox
global $post;
$khkplrv_id    = ( isset( $post ) ) ? $post->ID : false;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
$khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $khkplrv_id : false;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
// Theme Layout Width
$khkplrv_layout_width  = cs_get_option( 'theme_layout_width' );
$theme_preloder  = cs_get_option( 'theme_preloder' );
$khkplrv_layout_width_class = ( $khkplrv_layout_width === 'container' ) ? 'layout-boxed' : 'layout-full';
// Header Style
if ( $khkplrv_meta ) {
  $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
  $khkplrv_sticky_header = isset( $khkplrv_meta['sticky_header'] ) ? $khkplrv_meta['sticky_header'] : '' ;
} else {
  $khkplrv_header_design  = cs_get_option( 'select_header_design' );
  $khkplrv_sticky_header  = cs_get_option( 'sticky_header' );
}

if ( $khkplrv_header_design === 'default' ) {
  $khkplrv_header_design_actual  = cs_get_option( 'select_header_design' );
} else {
  $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
}

$khkplrv_header_design_actual = $khkplrv_header_design_actual ? $khkplrv_header_design_actual : 'style_one';

if ( $khkplrv_header_design_actual == 'style_one' ) {
  $header_class = 'header-style-1';
} elseif ( $khkplrv_header_design_actual == 'style_two' ) {
  $header_class = 'header-style-2';
}  else {
  $header_class = 'header-style-3';
}


$box_style_page = isset( $khkplrv_meta['box_style_page'] ) ? $khkplrv_meta['box_style_page'] : '';

if ( $box_style_page ) {
  add_filter( 'body_class','khkplrv_body_classes' );
  function khkplrv_body_classes( $classes ) {
      $classes[] = 'box-style';
      return $classes;
  }
}

if ( $khkplrv_sticky_header ) {
  $khkplrv_sticky_header = $khkplrv_sticky_header ? ' sticky-menu-on ' : '';
} else {
  $khkplrv_sticky_header = '';
}
// Header Transparent
if ( $khkplrv_meta ) {
  $khkplrv_transparent_header = $khkplrv_meta['transparency_header'];
  $khkplrv_transparent_header = $khkplrv_transparent_header ? ' header-transparent' : ' dont-transparent';
  // Shortcode Banner Type
  $khkplrv_banner_type = ' '. $khkplrv_meta['banner_type'];
} else { $khkplrv_transparent_header = ' dont-transparent'; $khkplrv_banner_type = ''; }
wp_head();
?>
</head>
<body <?php body_class(); ?>>
<div class="page-wrapper <?php echo esc_attr( $khkplrv_layout_width_class ); ?>"> <!-- #khkplrv-theme-wrapper -->
<?php if( $theme_preloder ) {
 get_template_part( 'theme-layouts/header/preloder' );
 } ?>
 <header id="header" class="site-header <?php echo esc_attr( $header_class ); ?>">
      <?php  get_template_part( 'theme-layouts/header/top','bar' ); ?>
      <nav class="navigation <?php echo esc_attr( $khkplrv_sticky_header ); ?> navbar navbar-default">
        <?php get_template_part( 'theme-layouts/header/menu','bar' ); ?>
      </nav>
  </header>
  <?php
  // Title Area
  $khkplrv_need_title_bar = cs_get_option('need_title_bar');
  if ( !$khkplrv_need_title_bar ) {
    get_template_part( 'theme-layouts/header/title', 'bar' );
  }
