<?php if( khkplrv_footer_widgets() ) {
?>
<!-- Footer Widgets -->
<div class="upper-footer">
	<div class="footer-widget-area">
		<div class="container">
			<div class="row">
				<?php echo khkplrv_footer_widgets(); ?>
			</div>
		</div>
	</div>
</div>
<!-- Footer Widgets -->
<?php
}