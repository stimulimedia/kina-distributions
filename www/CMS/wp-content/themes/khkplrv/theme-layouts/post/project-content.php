<?php
/**
 * Single Project.
 */
$khkplrv_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$khkplrv_large_image = $khkplrv_large_image[0];
$image_alt = get_post_meta( $khkplrv_large_image, '_wp_attachment_image_alt', true);
$project_options = get_post_meta( get_the_ID(), 'project_options', true );
$project_title = isset($project_options['project_title']) ? $project_options['project_title'] : '';
$project_infos = isset($project_options['project_infos']) ? $project_options['project_infos'] : '';
$project_gallery = isset($project_options['project_gallery']) ? $project_options['project_gallery'] : '';
?>
<div class="project-single-section">
    <div class="row">
        <div class="col col-xs-12">
            <div class="project-single-slider">
            <?php
                $khkplrv_ids = explode( ',', $project_options['project_gallery']);
                foreach ( $khkplrv_ids as $id ) {
                    $khkplrv_attachment = wp_get_attachment_image_src( $id, 'full' );
                    $khkplrv_alt = get_post_meta($id, '_wp_attachment_image_alt', true);
                    echo '<img src="'. $khkplrv_attachment[0] .'" alt="'. esc_attr( $khkplrv_alt ) .'" />';
                }
              ?>
            </div>
        </div>
    </div>
    <div class="row content">
        <div class="col col-xs-12">
            <div class="overview-info">
                <?php the_content();?>
            </div>
        </div>
    </div>
</div>