<?php
/**
 * Single Post.
 */
$khkplrv_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$khkplrv_large_image = $khkplrv_large_image[0];
$image_alt = get_post_meta( $khkplrv_large_image, '_wp_attachment_image_alt', true);
$khkplrv_post_type = get_post_meta( get_the_ID(), 'post_type_metabox', true );
// Single Theme Option
$khkplrv_post_pagination_option = cs_get_option('single_post_pagination');
$khkplrv_single_featured_image = cs_get_option('single_featured_image');
$khkplrv_single_author_info = cs_get_option('single_author_info');
$khkplrv_single_share_option = cs_get_option('single_share_option');
?>
  <div <?php post_class('post clearfix'); ?>>
  	<?php if ( $khkplrv_large_image ) { ?>
  	  <div class="entry-media">
        <img src="<?php echo esc_url( $khkplrv_large_image ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
   		</div>
  	<?php	} ?>
    <ul class="meta">
        <li>
						<i class="ti-user"></i><?php printf(
						   '<span>'. esc_html__(' by','khkplrv') .' <a href="%1$s" rel="author">%2$s</a></span>',
						  esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
						  get_the_author()
						); ?>
					</li>
        <li><i class="ti-calendar"></i><a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a></li>
        <li>
					<i class="ti-comment"></i><a class="khkplrv-comment" href="<?php echo esc_url( get_comments_link() ); ?>">
						<?php printf( esc_html( _nx( 'Comment (%1$s)', 'Comments (%1$s)', get_comments_number(), 'comments title', 'khkplrv' ) ), number_format_i18n( get_comments_number() ),'<span>' . get_the_title() . '</span>' ); ?>
					</a>
        </li>
    </ul>
    <div class="entry-details">
	     <?php
				the_content();
				echo khkplrv_wp_link_pages();
			 ?>
    </div>
</div>
<?php if( has_tag() || ( $khkplrv_single_share_option && function_exists('khkplrv_wp_share_option') ) ) { ?>
<div class="tag-share">
    <div class="tag">
        <i class="ti-folder"></i>
        <?php
					$tag_list = get_the_tags();
				  if($tag_list) {
				  	echo the_tags( ' <ul><li>', '</li><li>', '</li></ul>' );
				 } ?>
    </div>
	  	<?php
	  		if ( $khkplrv_single_share_option && function_exists('khkplrv_wp_share_option') ) {
							echo khkplrv_wp_share_option();
					}
			 ?>
</div>
<?php
}
if( !$khkplrv_single_author_info ) {
	khkplrv_author_info();
	}
 if( !$khkplrv_post_pagination_option ){
		khkplrv_single_pagination();
} ?>

