<?php
	// Metabox
	$khkplrv_id    = ( isset( $post ) ) ? $post->ID : 0;
	$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
	$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
	$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
	if ($khkplrv_meta && is_page()) {
		$khkplrv_title_bar_padding = $khkplrv_meta['title_area_spacings'];
	} else { $khkplrv_title_bar_padding = ''; }
	// Padding - Theme Options
	if ($khkplrv_title_bar_padding && $khkplrv_title_bar_padding !== 'padding-default') {
		$khkplrv_title_top_spacings = $khkplrv_meta['title_top_spacings'];
		$khkplrv_title_bottom_spacings = $khkplrv_meta['title_bottom_spacings'];
		if ($khkplrv_title_bar_padding === 'padding-custom') {
			$khkplrv_title_top_spacings = $khkplrv_title_top_spacings ? 'padding-top:'. khkplrv_check_px($khkplrv_title_top_spacings) .';' : '';
			$khkplrv_title_bottom_spacings = $khkplrv_title_bottom_spacings ? 'padding-bottom:'. khkplrv_check_px($khkplrv_title_bottom_spacings) .';' : '';
			$khkplrv_custom_padding = $khkplrv_title_top_spacings . $khkplrv_title_bottom_spacings;
		} else {
			$khkplrv_custom_padding = '';
		}
	} else {
		$khkplrv_title_bar_padding = cs_get_option('title_bar_padding');
		$khkplrv_titlebar_top_padding = cs_get_option('titlebar_top_padding');
		$khkplrv_titlebar_bottom_padding = cs_get_option('titlebar_bottom_padding');
		if ($khkplrv_title_bar_padding === 'padding-custom') {
			$khkplrv_titlebar_top_padding = $khkplrv_titlebar_top_padding ? 'padding-top:'. khkplrv_check_px($khkplrv_titlebar_top_padding) .';' : '';
			$khkplrv_titlebar_bottom_padding = $khkplrv_titlebar_bottom_padding ? 'padding-bottom:'. khkplrv_check_px($khkplrv_titlebar_bottom_padding) .';' : '';
			$khkplrv_custom_padding = $khkplrv_titlebar_top_padding . $khkplrv_titlebar_bottom_padding;
		} else {
			$khkplrv_custom_padding = '';
		}
	}
	// Banner Type - Meta Box
	if ($khkplrv_meta && is_page()) {
		$khkplrv_banner_type = $khkplrv_meta['banner_type'];
	} else { $khkplrv_banner_type = ''; }
	// Header Style
	if ($khkplrv_meta) {
	  $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
	  $khkplrv_hide_breadcrumbs  = $khkplrv_meta['hide_breadcrumbs'];
	} else {
	  $khkplrv_header_design  = cs_get_option('select_header_design');
	  $khkplrv_hide_breadcrumbs = cs_get_option('need_breadcrumbs');
	}
	if ( $khkplrv_header_design === 'default') {
	  $khkplrv_header_design_actual  = cs_get_option('select_header_design');
	} else {
	  $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
	}
	if ( $khkplrv_header_design_actual == 'style_three') {
		$overly_class = ' overly';
	} else {
		$overly_class = ' ';
	}
	// Overlay Color - Theme Options
		if ($khkplrv_meta && is_page()) {
			$khkplrv_bg_overlay_color = $khkplrv_meta['titlebar_bg_overlay_color'];
			$title_color = isset( $khkplrv_meta['title_color'] );
		} else { $khkplrv_bg_overlay_color = ''; }
		if (!empty($khkplrv_bg_overlay_color)) {
			$khkplrv_bg_overlay_color = $khkplrv_bg_overlay_color;
			$title_color = $title_color;
		} else {
			$khkplrv_bg_overlay_color = cs_get_option('titlebar_bg_overlay_color');
			$title_color = cs_get_option('title_color');
		}
		$e_uniqid        = uniqid();
		$inline_style  = '';
		if ( $khkplrv_bg_overlay_color ) {
		 $inline_style .= '.crumbs-area-'.$e_uniqid .'.crumbs-area.overly:after  {';
		 $inline_style .= ( $khkplrv_bg_overlay_color ) ? 'background-color:'. $khkplrv_bg_overlay_color.';' : '';
		 $inline_style .= '}';
		}
		if ( $title_color ) {
		 $inline_style .= '.crumbs-area-'.$e_uniqid .'.crumbs-area.overly .banner-content h2, .crumbs-area-'.$e_uniqid .'.crumbs-area.overly .crumbs ul li span, .crumbs-area-'.$e_uniqid .'.crumbs-area.overly .crumbs ul li a {';
		 $inline_style .= ( $title_color ) ? 'color:'. $title_color.';' : '';
		 $inline_style .= '}';
		}
		// add inline style
		add_inline_style( $inline_style );
		$styled_class  = ' crumbs-area-'.$e_uniqid;
	// Background - Type
	if( $khkplrv_meta ) {
		$title_bar_bg = $khkplrv_meta['title_area_bg'];
	} else {
		$title_bar_bg = '';
	}
	$khkplrv_custom_header = get_custom_header();
	$header_text_color = get_theme_mod( 'header_textcolor' );
	$background_color = get_theme_mod( 'background_color' );
	if( isset( $title_bar_bg['image'] ) && ( $title_bar_bg['image'] ||  $title_bar_bg['color'] ) ) {
	  extract( $title_bar_bg );
	  $khkplrv_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . esc_url($image) . ');' : '';
	  $khkplrv_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . esc_attr( $repeat) . ';' : '';
	  $khkplrv_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . esc_attr($position) . ';' : '';
	  $khkplrv_background_size    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . esc_attr($size) . ';' : '';
	  $khkplrv_background_attachment    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . esc_attr( $attachment ) . ';' : '';
	  $khkplrv_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . esc_attr( $color ) . ';' : '';
	  $khkplrv_background_style       = ( ! empty( $image ) ) ? $khkplrv_background_image . $khkplrv_background_repeat . $khkplrv_background_position . $khkplrv_background_size . $khkplrv_background_attachment : '';
	  $khkplrv_title_bg = ( ! empty( $khkplrv_background_style ) || ! empty( $khkplrv_background_color ) ) ? $khkplrv_background_style . $khkplrv_background_color : '';
	} elseif( $khkplrv_custom_header->url ) {
		$khkplrv_title_bg = 'background-image:  url('. esc_url( $khkplrv_custom_header->url ) .');';
	} else {
		$khkplrv_title_bg = '';
	}
	if($khkplrv_banner_type === 'hide-title-area') { // Hide Title Area
	} elseif($khkplrv_meta && $khkplrv_banner_type === 'revolution-slider') { // Hide Title Area
		echo do_shortcode($khkplrv_meta['page_revslider']);
	} else {
	?>
 <!-- start page-title -->
  <section class="page-title <?php echo esc_attr( $overly_class.$styled_class.' '.$khkplrv_banner_type ); ?>" style="<?php echo esc_attr( $khkplrv_title_bg ); ?>">
      <div class="container">
          <div class="row">
              <div class="col col-xs-12" style="<?php echo esc_attr( $khkplrv_custom_padding ); ?>" >
                  <div class="title">
                      <h2><?php echo khkplrv_title_area(); ?></h2>
                  </div>
                   <?php if ( !$khkplrv_hide_breadcrumbs && function_exists( 'breadcrumb_trail' )) { breadcrumb_trail();  } ?>
              </div>
          </div> <!-- end row -->
      </div> <!-- end container -->
  </section>
  <!-- end page-title -->
<?php } ?>