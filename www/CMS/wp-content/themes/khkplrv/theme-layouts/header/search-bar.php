<?php
  // Metabox
  $khkplrv_id    = ( isset( $post ) ) ? $post->ID : 0;
  $khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
  $khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
  $khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $khkplrv_id : false;
  $khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );

  // Header Style
  if ( $khkplrv_meta ) {
    $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
    $khkplrv_cart_widget = isset( $khkplrv_meta['khkplrv_cart_widget'] ) ? $khkplrv_meta['khkplrv_cart_widget'] : '';
    $khkplrv_search = isset( $khkplrv_meta['khkplrv_search'] ) ? $khkplrv_meta['khkplrv_search'] : '';
  } else {
    $khkplrv_header_design  = cs_get_option( 'select_header_design' );
    $khkplrv_cart_widget  = cs_get_option( 'khkplrv_cart_widget' );
    $khkplrv_search  = cs_get_option( 'khkplrv_search' );
  }
  if ( $khkplrv_header_design === 'default' ) {
    $khkplrv_header_design_actual  = cs_get_option( 'select_header_design' );
  } else {
    $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
  }
  $khkplrv_header_design_actual = $khkplrv_header_design_actual ? $khkplrv_header_design_actual : 'style_two';

  if ( $khkplrv_meta && $khkplrv_header_design !== 'default') {
   $khkplrv_cart_widget = isset( $khkplrv_meta['khkplrv_cart_widget'] ) ? $khkplrv_meta['khkplrv_cart_widget'] : '';
   $khkplrv_search = isset( $khkplrv_meta['khkplrv_search'] ) ? $khkplrv_meta['khkplrv_search'] : '';
  } else {
    $khkplrv_cart_widget  = cs_get_option( 'khkplrv_cart_widget' );
    $khkplrv_search  = cs_get_option( 'khkplrv_search' );
  }
?>
  <div class="cart-search-contact">
    <?php if ( !$khkplrv_search ) { ?>
      <div class="header-search-form-wrapper">
          <button class="search-toggle-btn"><i class="fi flaticon-search"></i></button>
          <div class="header-search-form">
              <form method="get" class="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                  <div>
                      <input type="text" name="s" id="s" class="form-control" placeholder="<?php echo esc_attr__('Search...','khkplrv'); ?>">
                      <button type="submit"><i class="fi flaticon-search"></i></button>
                  </div>
              </form>
          </div>
      </div>
    <?php }
      if ( $khkplrv_cart_widget &&  class_exists( 'WooCommerce' ) ) {
        get_template_part( 'theme-layouts/header/top','cart' );
      }
    ?>
</div>