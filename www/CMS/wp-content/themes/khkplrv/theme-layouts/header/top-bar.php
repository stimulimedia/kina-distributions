<?php
// Metabox
global $post;
$khkplrv_id    = ( isset( $post ) ) ? $post->ID : false;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
$khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $khkplrv_id : false;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
  if ($khkplrv_meta) {
    $khkplrv_topbar_options = $khkplrv_meta['topbar_options'];
  } else {
    $khkplrv_topbar_options = '';
  }

  if ( $khkplrv_meta ) {
    $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
  } else {
    $khkplrv_header_design  = cs_get_option( 'select_header_design' );
  }

 if ( $khkplrv_header_design === 'default' ) {
    $khkplrv_header_design_actual  = cs_get_option( 'select_header_design' );
  } else {
    $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
  }

// Define Theme Options and Metabox varials in right way!
if ($khkplrv_meta) {
  if ($khkplrv_topbar_options === 'custom' && $khkplrv_topbar_options !== 'default') {
    $khkplrv_top_left          = $khkplrv_meta['top_left'];
    $khkplrv_top_right          = $khkplrv_meta['top_right'];
    $khkplrv_hide_topbar        = $khkplrv_topbar_options;
    $khkplrv_topbar_bg          = $khkplrv_meta['topbar_bg'];
    if ($khkplrv_topbar_bg) {
      $khkplrv_topbar_bg = 'background-color: '. $khkplrv_topbar_bg .';';
    } else {$khkplrv_topbar_bg = '';}
  } else {
    $khkplrv_top_left          = cs_get_option('top_left');
    $khkplrv_top_right          = cs_get_option('top_right');
    $khkplrv_hide_topbar        = cs_get_option('top_bar');
    $khkplrv_topbar_bg          = '';
  }
} else {
  // Theme Options fields
  $khkplrv_top_left         = cs_get_option('top_left');
  $khkplrv_top_right          = cs_get_option('top_right');
  $khkplrv_hide_topbar        = cs_get_option('top_bar');
  $khkplrv_topbar_bg          = '';
}
// All options
if ( $khkplrv_meta && $khkplrv_topbar_options === 'custom' && $khkplrv_topbar_options !== 'default' ) {
  $khkplrv_top_right = ( $khkplrv_top_right ) ? $khkplrv_meta['top_right'] : cs_get_option('top_right');
  $khkplrv_top_left = ( $khkplrv_top_left ) ? $khkplrv_meta['top_left'] : cs_get_option('top_left');
} else {
  $khkplrv_top_right = cs_get_option('top_right');
  $khkplrv_top_left = cs_get_option('top_left');
}
if ( $khkplrv_meta && $khkplrv_topbar_options !== 'default' ) {
  if ( $khkplrv_topbar_options === 'hide_topbar' ) {
    $khkplrv_hide_topbar = 'hide';
  } else {
    $khkplrv_hide_topbar = 'show';
  }
} else {
  $khkplrv_hide_topbar_check = cs_get_option( 'top_bar' );
  if ( $khkplrv_hide_topbar_check === true ) {
     $khkplrv_hide_topbar = 'hide';
  } else {
     $khkplrv_hide_topbar = 'show';
  }
}
if ( $khkplrv_meta ) {
  $khkplrv_topbar_bg = ( $khkplrv_topbar_bg ) ? $khkplrv_meta['topbar_bg'] : '';
} else {
  $khkplrv_topbar_bg = '';
}
if ( $khkplrv_topbar_bg ) {
  $khkplrv_topbar_bg = 'background-color: '. $khkplrv_topbar_bg .';';
} else { $khkplrv_topbar_bg = ''; }

if( $khkplrv_hide_topbar === 'show' && ( $khkplrv_top_left || $khkplrv_top_right ) ) {
?>
 <!-- header top area start -->
  <div class="topbar" style="<?php echo esc_attr( $khkplrv_topbar_bg ); ?>">
      <div class="upper-topbar">
        <div class="container">
          <?php if ( $khkplrv_header_design_actual == 'style_three' ) { ?>
            <div class="separator"></div>
          <?php } ?>
            <div class="row">
                <div class="col col-md-10">
                   <?php echo do_shortcode( $khkplrv_top_left ); ?>
                </div>
                <div class="col col-md-2">
                    <?php echo do_shortcode( $khkplrv_top_right ); ?>
                </div>
            </div>
        </div> <!-- end container -->
      </div> <!-- end upper-topbar -->
      <?php get_template_part( 'theme-layouts/header/lower-topbar' ); ?>
  </div>
<!-- header top area end -->
<?php } // Hide Topbar - From Metabox