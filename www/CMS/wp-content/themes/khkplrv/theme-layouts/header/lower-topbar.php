<?php
// Metabox
global $post;
$khkplrv_id    = ( isset( $post ) ) ? $post->ID : false;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
$khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() ) ? $khkplrv_id : false;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );

  if ( $khkplrv_meta ) {
    $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
    $header_middle  = $khkplrv_meta['header_middle'];
  } else {
    $khkplrv_header_design  = cs_get_option( 'select_header_design' );
    $header_middle  = cs_get_option( 'header_middle' );
  }

  if ( $khkplrv_header_design === 'default' ) {
    $khkplrv_header_design_actual  = cs_get_option( 'select_header_design' );
  } else {
    $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
  }
  $khkplrv_header_design_actual = $khkplrv_header_design_actual ? $khkplrv_header_design_actual : 'style_one';

  if ( $khkplrv_meta && $khkplrv_header_design !== 'default') {
    $header_middle  = $khkplrv_meta['header_middle'];
  } else {
    $header_middle  = cs_get_option( 'header_middle' );
  }

if ( $khkplrv_header_design_actual == 'style_two' ) { ?>
  <div class="lower-topbar">
      <div class="container">
          <div class="border-bottom"></div>
          <div class="border-left"></div>
          <div class="row">
              <div class="col col-md-3">
                  <?php get_template_part( 'theme-layouts/header/logo' ); ?>
              </div>
              <div class="col col-md-9">
                  <?php echo do_shortcode( $header_middle ); ?>
              </div>
          </div>
      </div>
  </div> <!-- end lower-topbar -->
<?php } ?>
