<?php
// Metabox
global $post;
$khkplrv_id    = ( isset( $post ) ) ? $post->ID : false;
$khkplrv_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $khkplrv_id;
$khkplrv_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $khkplrv_id;
$khkplrv_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('service') ) ? $khkplrv_id : false;
$khkplrv_meta  = get_post_meta( $khkplrv_id, 'page_type_metabox', true );
// Header Style
if ( $khkplrv_meta ) {
  $khkplrv_header_design  = $khkplrv_meta['select_header_design'];
} else {
  $khkplrv_header_design  = cs_get_option( 'select_header_design' );
}

if ( $khkplrv_header_design === 'default' ) {
  $khkplrv_header_design_actual  = cs_get_option( 'select_header_design' );
} else {
  $khkplrv_header_design_actual = ( $khkplrv_header_design ) ? $khkplrv_header_design : cs_get_option('select_header_design');
}
$khkplrv_header_design_actual = $khkplrv_header_design_actual ? $khkplrv_header_design_actual : 'style_two';

$khkplrv_logo = cs_get_option( 'khkplrv_logo' );
$khkplrv_trlogo = cs_get_option( 'khkplrv_trlogo' );
$logo_url = wp_get_attachment_url( $khkplrv_logo );
$logo_alt = get_post_meta( $khkplrv_logo, '_wp_attachment_image_alt', true );
$trlogo_url = wp_get_attachment_url( $khkplrv_trlogo );
$trlogo_alt = get_post_meta( $khkplrv_trlogo, '_wp_attachment_image_alt', true );

if ( $logo_url ) {
  $logo_url = $logo_url;
} else {
 $logo_url = KHKPLRV_IMAGES.'/logo.png';
}

if ( $trlogo_url ) {
  $trlogo_url = $trlogo_url;
} else {
 $trlogo_url = KHKPLRV_IMAGES.'/tr-logo.png';
}

if ( $khkplrv_header_design_actual == 'style_three' ) {
  $khkplrv_logo_url = $trlogo_url;
  $khkplrv_logo_alt = $trlogo_alt;
} else {
  $khkplrv_logo_url = $logo_url;
  $khkplrv_logo_alt = $logo_alt;
}

if ( has_nav_menu( 'primary' ) ) {
  $logo_padding = ' has_menu ';
}
else {
   $logo_padding = ' dont_has_menu ';
}


// Logo Spacings
// Logo Spacings
$khkplrv_brand_logo_top = cs_get_option( 'khkplrv_logo_top' );
$khkplrv_brand_logo_bottom = cs_get_option( 'khkplrv_logo_bottom' );
if ( $khkplrv_brand_logo_top ) {
  $khkplrv_brand_logo_top = 'padding-top:'. khkplrv_check_px( $khkplrv_brand_logo_top ) .';';
} else { $khkplrv_brand_logo_top = ''; }
if ( $khkplrv_brand_logo_bottom ) {
  $khkplrv_brand_logo_bottom = 'padding-bottom:'. khkplrv_check_px( $khkplrv_brand_logo_bottom ) .';';
} else { $khkplrv_brand_logo_bottom = ''; }
?>
<div class="site-logo <?php echo esc_attr( $logo_padding ); ?>"  style="<?php echo esc_attr( $khkplrv_brand_logo_top ); echo esc_attr( $khkplrv_brand_logo_bottom ); ?>">
   <?php if ( $khkplrv_logo ) {
    ?>
      <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
       <img src="<?php echo esc_url( $khkplrv_logo_url ); ?>" alt=" <?php echo esc_attr( $khkplrv_logo_alt ); ?>">
     </a>
   <?php } elseif( has_custom_logo() ) {
      the_custom_logo();
    } else {
    ?>
    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
       <img src="<?php echo esc_url( $khkplrv_logo_url ); ?>" alt="<?php echo get_bloginfo('name'); ?>">
     </a>
   <?php
  } ?>
</div>