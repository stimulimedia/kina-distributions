<?php

function kina_acf_field_group_prospects() {
	acf_add_local_field_group(array(
		'key' => 'kina_prospects',
		'title' => 'KINA Prospect Fields',
		'fields' => kina_acf_fields_prospects(),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'kina_prospects',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'hide_on_screen' => array(
			0 => 'the_content',
		),
	));
}


function kina_acf_fields_prospects() {
	$fields = array();

	kina_acf_fields_contact($fields);
	kina_acf_fields_project_basics($fields);
	kina_acf_fields_project_equipment($fields);
	kina_acf_fields_project_final($fields);
	kina_acf_fields_project_calculations($fields);

	return $fields;
}

	function kina_acf_fields_contact(&$fields) {
		$fields[] = array(
			'key' => 'kina_prospects_company',
			'label' => 'Company Name',
			'name' => 'CompanyName',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_firstName',
			'label' => 'First Name',
			'name' => 'FirstName',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_lastName',
			'label' => 'Last Name',
			'name' => 'LastName',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_phoneNumber',
			'label' => 'Phone Number',
			'name' => 'PhoneNumber',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_emailAddress',
			'label' => 'Email Address',
			'name' => 'EmailAddress',
			'type' => 'text',
		);
	}

	function kina_acf_fields_project_basics(&$fields) {
		$fields[] = array(
			'key' => 'kina_prospects_project_urgency',
			'label' => 'Project Urgency',
			'name' => 'ProjectUrgency',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_city',
			'label' => 'Project City',
			'name' => 'ProjectCity',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_months',
			'label' => 'Project Months',
			'name' => 'ProjectMonths',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_floors',
			'label' => 'Project Floors',
			'name' => 'ProjectFloors',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_budget',
			'label' => 'Project Budget',
			'name' => 'ProjectBudget',
			'type' => 'number',
		);
	}

	function kina_acf_fields_project_equipment(&$fields) {
		$fields[] = array(
			'key' => 'kina_prospects_project_equipment',
			'label' => 'Project Equipment',
			'name' => 'ProjectEquipment',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_equipment_purchase',
			'label' => 'Project Equipment Purchase',
			'name' => 'ProjectEquipmentPurchase',
			'type' => 'text',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_equipment_purchase_price',
			'label' => 'Project Equipment Purchase Price',
			'name' => 'ProjectEquipmentPurchasePrice',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_equipment_monthly_cost',
			'label' => 'Project Equipment Monthly Cost',
			'name' => 'ProjectEquipmentMonthlyCost',
			'type' => 'number',
		);
	}

	function kina_acf_fields_project_final(&$fields) {
		$fields[] = array(
			'key' => 'kina_prospects_project_picks_per_day',
			'label' => 'Project Picks Per Day',
			'name' => 'ProjectPicksPerDay',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_picks_per_day_need',
			'label' => 'Project Picks Per Day Need',
			'name' => 'ProjectPicksPerDayNeed',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_fewer_months',
			'label' => 'Project Fewer Months',
			'name' => 'ProjectFewerMonths',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_concurrent_work_floors',
			'label' => 'Project Concurrent Work Floors',
			'name' => 'ProjectConcurrentWorkFloors',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_projects_per_year',
			'label' => 'Projects Per Year',
			'name' => 'ProjectsPerYear',
			'type' => 'number',
		);
	}

	function kina_acf_fields_project_calculations(&$fields) {
		// Pricing base
		$fields[] = array(
			'key' => 'kina_prospects_monthly_cost_tc',
			'label' => 'Total Monthly Cost - Tower Crane',
			'name' => 'TowerCraneMonthlyCost',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_total_project_cost',
			'label' => 'Total Project Tower Crane Cost',
			'name' => 'TotalProjectCost',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_project_fewer_months',
			'label' => 'Fewer Months - Project',
			'name' => 'ProjectFewerMonths',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_fewer_months_tc',
			'label' => 'Fewer Months - Tower Crane',
			'name' => 'TowerCraneFewerMonths',
			'type' => 'number',
		);


		// Recommended Order
		$fields[] = array(
			'key' => 'kina_prospects_number_decks',
			'label' => 'Number of Decks',
			'name' => 'NumberOfDecks',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_price_of_set',
			'label' => 'Price of Set',
			'name' => 'PriceOfSet',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_monthly_cost_ltd',
			'label' => 'Estimated Monthly Payment - LTD',
			'name' => 'MonthlyCostLTD',
			'type' => 'number',
		);


		// Savings and cost per pick
		$fields[] = array(
			'key' => 'kina_prospects_savings',
			'label' => 'Project Savings',
			'name' => 'ProjectSavings',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_cpp_tc',
			'label' => 'Cost Per Pick - Tower Crane',
			'name' => 'CostPerPickTC',
			'type' => 'number',
		);

		$fields[] = array(
			'key' => 'kina_prospects_cpp_ltd',
			'label' => 'Cost Per Pick - LTD',
			'name' => 'CostPerPickLTD',
			'type' => 'number',
		);
	}