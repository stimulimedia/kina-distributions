(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"kina-funnel\">\n\t<funnel [language]=\"language\"></funnel>\n</div>\n\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent(el) {
        this.el = el;
        this.language = 'en';
        if (el.nativeElement.getAttribute('language')) {
            this.language = el.nativeElement.getAttribute('language');
        }
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kina-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _funnel_funnel_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./funnel/funnel.component */ "./src/app/funnel/funnel.component.ts");
/* harmony import */ var _question_selection_question_selection_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./question-selection/question-selection.component */ "./src/app/question-selection/question-selection.component.ts");
/* harmony import */ var _customer_info_customer_info_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./customer-info/customer-info.component */ "./src/app/customer-info/customer-info.component.ts");
/* harmony import */ var _question_text_question_text_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./question-text/question-text.component */ "./src/app/question-text/question-text.component.ts");
/* harmony import */ var _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./thank-you/thank-you.component */ "./src/app/thank-you/thank-you.component.ts");
/* harmony import */ var _forward_forward_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./forward/forward.component */ "./src/app/forward/forward.component.ts");
/* harmony import */ var _finished_finished_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./finished/finished.component */ "./src/app/finished/finished.component.ts");













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _funnel_funnel_component__WEBPACK_IMPORTED_MODULE_6__["FunnelComponent"],
                _question_selection_question_selection_component__WEBPACK_IMPORTED_MODULE_7__["QuestionSelectionComponent"],
                _customer_info_customer_info_component__WEBPACK_IMPORTED_MODULE_8__["CustomerInfoComponent"],
                _question_text_question_text_component__WEBPACK_IMPORTED_MODULE_9__["QuestionTextComponent"],
                _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_10__["ThankYouComponent"],
                _forward_forward_component__WEBPACK_IMPORTED_MODULE_11__["ForwardComponent"],
                _finished_finished_component__WEBPACK_IMPORTED_MODULE_12__["FinishedComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/config.ts":
/*!***************************!*\
  !*** ./src/app/config.ts ***!
  \***************************/
/*! exports provided: Config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return Config; });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.SETTINGS = {
        'en': {
            'title': 'KINA Distributions LTD® Series',
            'introduction': 'How would you feel if the LTD<sup>®</sup> could save you <strong>over $100k</strong> on your high rise construction project?',
            'subIntroduction': 'Find out in less than 5 minutes.',
            'instructions': 'Before we get started, tell us who you are.',
            'customerInfo': {
                'fields': {
                    'company': 'Company Name',
                    'firstName': 'First Name',
                    'lastName': 'Last Name',
                    'phoneNumber': 'Phone Number',
                    'emailAddress': 'Email Address'
                },
                'buttonText': 'Let\'s Get Started!',
                'successText': 'Awesome! Let\'s save you some money!'
            },
            'nextButton': {
                'next': 'Next',
                'finish': 'Finish'
            },
            'thankYou': {
                'mailOnly': {
                    'body': 'Awesome! Thank you for your interest in the LTD<sup>®</sup> Series. Please click below to be sent our detailed spec and receive information, offers, events, and more.',
                    'buttonText': 'Click to subscribe and request info',
                },
                'renter': {
                    'body': 'Thank you for your interest in the LTD<sup>®</sup> Series! Please click below to receive your results and receive information, offers, events, and more.',
                    'buttonText': 'Click to subscribe and receive your results'
                },
                'buyer': {
                    'body': 'Thank you for your interest in the LTD<sup>®</sup> Series! Please click below to receive your results and receive information, offers, events, and more.',
                    'buttonText': 'Click to subscribe and receive your results'
                }
            },
            'finish': {
                'title': 'Thank you for your time!',
                'subtitle': 'We will contact you shortly.'
            },
            'forwardInfo': {
                'title': 'Great! We\'ll let them know. Enter their info below.',
                'fields': {
                    'company': 'Company Name',
                    'firstName': 'First Name',
                    'lastName': 'Last Name',
                    'phoneNumber': 'Phone Number',
                    'emailAddress': 'Email Address'
                },
                'buttonText': 'Send!'
            },
            'questions': {
                'root': [
                    {
                        'key': 'urgency',
                        'type': 'selection',
                        'style': '100',
                        'label': 'How soon will your high rise construction project start?',
                        'field': 'ProjectUrgency',
                        'answers': [
                            {
                                'answer': 'Within 1 year',
                                'value': '1 year',
                                'actions': ['label:BuyerInHeat', 'add:construction_project']
                            },
                            {
                                'answer': 'Between 1 to 3 years',
                                'value': '1-3 years',
                                'actions': ['label:BuyerInPower', 'add:construction_project']
                            },
                            {
                                'answer': 'More than 3 years',
                                'value': '3+ years',
                                'actions': ['label:PotentialBuyerInPower', 'add:construction_project']
                            },
                            {
                                'answer': 'I do not have a project at this time',
                                'value': 'None',
                                'actions': ['add:no_construction_project']
                            }
                        ]
                    }
                ],
                'construction_project': [
                    {
                        'key': 'city',
                        'type': 'text',
                        'style': '100',
                        'label': 'In what city is your high rise construction project site?',
                        'field': 'ProjectCity',
                        'prepend': '',
                        'append': '',
                        'actions': []
                    },
                    {
                        'key': 'months',
                        'type': 'text',
                        'style': '25',
                        'label': 'How long will it take to complete your project?',
                        'field': 'ProjectMonths',
                        'prepend': '',
                        'append': 'months',
                        'validator': { 'format': 'number', 'min': 1, 'max': 120 },
                        'actions': []
                    },
                    {
                        'key': 'floors',
                        'type': 'text',
                        'style': '25',
                        'label': 'How many floors will your project have?',
                        'field': 'ProjectFloors',
                        'prepend': '',
                        'append': 'floors',
                        'validator': { 'format': 'number', 'min': 0, 'max': 200 },
                        'actions': ['add:high_rise_project']
                    }
                ],
                'high_rise_project': [
                    {
                        'key': 'budget',
                        'type': 'text',
                        'style': '25',
                        'label': 'In order to best estimate your savings, please tell us the total estimated project budget in millions (eg. $1.2 Million)',
                        'prepend': '$',
                        'append': 'Million',
                        'field': 'ProjectBudget',
                        'validator': { 'format': 'currency', 'min': 1, 'max': 200, 'step': .1 },
                        'actions': []
                    },
                    {
                        'key': 'equipmentPurchase',
                        'type': 'selection',
                        'style': '100',
                        'label': 'Are you planning to purchase or rent a tower crane?',
                        'field': 'ProjectEquipmentPurchase',
                        'answers': [
                            {
                                'answer': 'Purchase',
                                'value': 'purchase',
                                'actions': ['store', 'set:ProjectEquipment:tower crane', 'add:equipment_purchase,high_rise_remaining', 'setNext:finish']
                            },
                            {
                                'answer': 'Rent',
                                'value': 'rent',
                                'actions': ['store', 'set:ProjectEquipment:tower crane', 'add:equipment_rent,high_rise_remaining', 'setNext:finish']
                            },
                            {
                                'answer': 'I don\'t need a tower crane',
                                'value': 'none',
                                'actions': ['store', 'add:different_equipment']
                            }
                        ]
                    }
                ],
                'equipment_purchase': [
                    {
                        'key': 'equipmentPurchasePrice',
                        'type': 'text',
                        'style': '25',
                        'label': 'How much will the {{ProjectEquipment}} cost?',
                        'prepend': '$',
                        'append': '',
                        'field': 'ProjectEquipmentPurchasePrice',
                        'validator': { 'format': 'currency', 'min': 0, 'max': 1000000 },
                        'actions': []
                    }
                ],
                'equipment_rent': [
                    {
                        'key': 'equipmentMonthlyCost',
                        'type': 'text',
                        'style': '25',
                        'label': 'How much will the {{ProjectEquipment}} cost on a monthly basis?',
                        'prepend': '$',
                        'append': 'per month',
                        'field': 'ProjectEquipmentMonthlyCost',
                        'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
                        'actions': []
                    }
                ],
                'different_equipment': [
                    {
                        'key': 'equipment',
                        'type': 'text',
                        'style': '100',
                        'label': 'What equipment do you plan to use to move materials between floors?',
                        'prepend': '',
                        'append': '',
                        'field': 'ProjectEquipment',
                        'actions': ['add:different_equipment_cost,high_rise_remaining', 'setNext:finish']
                    }
                ],
                'different_equipment_cost': [
                    {
                        'key': 'equipmentPurchasePrice',
                        'type': 'text',
                        'style': '25',
                        'label': 'How much will the {{ProjectEquipment}} cost on a monthly basis?',
                        'prepend': '$',
                        'append': 'per month',
                        'field': 'ProjectEquipmentPurchasePrice',
                        'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
                        'actions': []
                    }
                ],
                'high_rise_remaining': [
                    {
                        'key': 'picksPerDay',
                        'type': 'text',
                        'style': '25',
                        'label': 'How many loads of materials are you realistically planning to move per day with the {{ProjectEquipment}}?',
                        'field': 'ProjectPicksPerDay',
                        'prepend': '',
                        'append': 'loads of material',
                        'validator': { 'format': 'number', 'min': 0, 'max': 500 },
                        'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                    },
                    {
                        'key': 'picksPerDayNeeded',
                        'type': 'selection',
                        'style': '25',
                        'label': 'How many times more loads of materials would <em>you like</em> to move per day?',
                        'prepend': '',
                        'append': 'more loads',
                        'field': 'ProjectPicksPerDayNeedMultiplier',
                        'answers': [
                            {
                                'answer': '2 times',
                                'value': '2',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '3 times',
                                'value': '3',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '4 times',
                                'value': '4',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '5 times',
                                'value': '5',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '6 times',
                                'value': '6',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '7 times',
                                'value': '7',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '8 times',
                                'value': '8',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '9 times',
                                'value': '9',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '10 times',
                                'value': '10',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            }
                        ]
                    },
                    {
                        'key': 'concurrentWorkFloors',
                        'type': 'text',
                        'style': '25',
                        'label': 'On how many floors could you potentially have work being done at the same time?',
                        'prepend': '',
                        'append': 'floors',
                        'field': 'ProjectConcurrentWorkFloors',
                        'validator': { 'format': 'number', 'min': 0, 'max': 50 },
                        'actions': []
                    },
                    {
                        'key': 'projectsPerYear',
                        'type': 'text',
                        'style': '25',
                        'label': 'How many projects do you do per year on average?',
                        'prepend': '',
                        'append': 'projects per year',
                        'field': 'ProjectsPerYear',
                        'validator': { 'format': 'number', 'min': 1, 'max': 50 },
                        'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
                    }
                ],
                'decision_maker': [
                    {
                        'key': 'decisionMaker',
                        'type': 'selection',
                        'style': '100',
                        'label': 'For your convienience, should we forward the results to someone who has final say on the project plan?',
                        'answers': [
                            {
                                'answer': 'No thanks!',
                                'value': 'No',
                                'actions': ['store', 'thank_you:buyer']
                            },
                            {
                                'answer': 'It\'s me',
                                'value': 'Me',
                                'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
                            },
                            {
                                'answer': 'Yes please!',
                                'value': 'Yes',
                                'actions': ['store', 'label:Influencer', 'forward_to_decision_maker']
                            }
                        ]
                    }
                ],
                'no_construction_project': [
                    {
                        'key': 'whatYouDo',
                        'type': 'selection',
                        'style': '100',
                        'label': 'Please tell us more about yourself. What do you do?',
                        'field': 'AboutProspect',
                        'answers': [
                            {
                                'answer': 'I rent construction equipment',
                                'value': 'None',
                                'actions': ['store', 'label:RentalCompany,DecisionMaker', 'thank_you:renter']
                            },
                            {
                                'answer': 'I am a commercial real estate developer',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            },
                            {
                                'answer': 'I am an architect',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            },
                            {
                                'answer': 'I am a civil (or other) engineer',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            },
                            {
                                'answer': 'Other',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            }
                        ]
                    }
                ],
                'equipment_renter': [
                    {
                        'type': 'selection',
                        'style': '100',
                        'label': 'Test question?',
                        'answers': [
                            {
                                'answer': 'Test answer',
                                'value': 'None',
                                'actions': ['store', 'equipment_renter']
                            }
                        ]
                    }
                ]
            }
        },
        'es': {
            'title': 'KINA Distributions LTD® Series',
            'introduction': '¿Qué opinarías de que el LTD<sup>®</sup> te pudiera ahorrar <strong>más de $100k USD</strong> en tu proyecto de construcción de torres?',
            'subIntroduction': 'Averígualo en menos  de 5 minutos.',
            'instructions': 'Antes de empezar, platícanos sobre ti.',
            'customerInfo': {
                'fields': {
                    'company': 'Nombre de la Compañía ',
                    'firstName': 'Nombre (s)',
                    'lastName': 'Apellidos',
                    'phoneNumber': 'Teléfono',
                    'emailAddress': 'Dirección Email'
                },
                'buttonText': '¡Comencemos!',
                'successText': '¡Genial! ¡Vamos a ahorrarte dinero!'
            },
            'nextButton': {
                'next': 'Siguiente',
                'finish': 'Terminar'
            },
            'thankYou': {
                'mailOnly': {
                    'body': '¡Genial! Gracias por tu interés en la Serie LTD<sup>®</sup>. Favor de dar click abajo para enviarte las especificaciones y recibir información, ofertas, eventos y más.',
                    'buttonText': 'Suscribirme y solicitar info',
                },
                'renter': {
                    'body': '¡Gracias por tu interés en la Serie  LTD<sup>®</sup>! Favor de dar click abajo para recibir tus resultados y recibir información, ofertas, eventos y más.',
                    'buttonText': 'Suscríbete y recibe tus resultados'
                },
                'buyer': {
                    'body': '¡Gracias por tu interés en la Serie  LTD<sup>®</sup>! Favor de dar click abajo para recibir tus resultados y recibir información, ofertas, eventos y más.',
                    'buttonText': 'Suscríbete y recibe tus resultados'
                }
            },
            'finish': {
                'title': '¡Gracias por tu tiempo!',
                'subtitle': 'Lo contactaremos pronto.'
            },
            'forwardInfo': {
                'title': 'Great! We\'ll let them know. Enter their info below.',
                'fields': {
                    'company': 'Company Name',
                    'firstName': 'First Name',
                    'lastName': 'Last Name',
                    'phoneNumber': 'Phone Number',
                    'emailAddress': 'Email Address'
                },
                'buttonText': 'Send!'
            },
            'questions': {
                'root': [
                    {
                        'key': 'urgency',
                        'type': 'selection',
                        'style': '100',
                        'label': '¿Qué tan pronto comenzará tu proyecto de construcción de torres?',
                        'field': 'ProjectUrgency',
                        'answers': [
                            {
                                'answer': 'En los siguientes 12 meses',
                                'value': '1 year',
                                'actions': ['label:BuyerInHeat', 'add:construction_project']
                            },
                            {
                                'answer': 'Entre 1 y 3 años',
                                'value': '1-3 years',
                                'actions': ['label:BuyerInPower', 'add:construction_project']
                            },
                            {
                                'answer': 'Más de 3 años',
                                'value': '3+ years',
                                'actions': ['label:PotentialBuyerInPower', 'add:construction_project']
                            },
                            {
                                'answer': 'No cuento con un proyecto en este momento',
                                'value': 'None',
                                'actions': ['add:no_construction_project']
                            }
                        ]
                    }
                ],
                'construction_project': [
                    {
                        'key': 'city',
                        'type': 'text',
                        'style': '100',
                        'label': '¿En qué ciudad vas a desarrollar tu proyecto de construccion de torres?',
                        'field': 'ProjectCity',
                        'prepend': '',
                        'append': '',
                        'actions': []
                    },
                    {
                        'key': 'months',
                        'type': 'text',
                        'style': '25',
                        'label': '¿Cuánto tiempo está programado que se tomará tu proyecto de construcción de torres para tenerlo finalizado?',
                        'field': 'ProjectMonths',
                        'prepend': '',
                        'append': 'meses',
                        'validator': { 'format': 'number', 'min': 1, 'max': 120 },
                        'actions': []
                    },
                    {
                        'key': 'floors',
                        'type': 'text',
                        'style': '25',
                        'label': '¿Cuántos pisos tendrá tu proyecto?',
                        'field': 'ProjectFloors',
                        'prepend': '',
                        'append': 'pisos',
                        'validator': { 'format': 'number', 'min': 0, 'max': 200 },
                        'actions': ['add:high_rise_project']
                    }
                ],
                'high_rise_project': [
                    {
                        'key': 'budget',
                        'type': 'text',
                        'style': '25',
                        'label': 'Con el objetivo de ser más precisos en lo que te puedes ahorrar, por favor compártenos el presupuesto total estimado de tu proyecto en millones de USD (por ejemplo. $1.2 Millones)',
                        'prepend': '$',
                        'append': 'Millones',
                        'field': 'ProjectBudget',
                        'validator': { 'format': 'currency', 'min': 1, 'max': 200, 'step': .1 },
                        'actions': []
                    },
                    {
                        'key': 'equipmentPurchase',
                        'type': 'selection',
                        'style': '100',
                        'label': '¿Estás planeando comprar o rentar una grúa torre?',
                        'field': 'ProjectEquipmentPurchase',
                        'answers': [
                            {
                                'answer': 'Comprar',
                                'value': 'purchase',
                                'actions': ['store', 'set:ProjectEquipment:grúa torre', 'add:equipment_purchase,high_rise_remaining', 'setNext:finish']
                            },
                            {
                                'answer': 'Rentar',
                                'value': 'rent',
                                'actions': ['store', 'set:ProjectEquipment:grúa torre', 'add:equipment_rent,high_rise_remaining', 'setNext:finish']
                            },
                            {
                                'answer': 'No necesito una grúa torre',
                                'value': 'none',
                                'actions': ['store', 'add:different_equipment']
                            }
                        ]
                    }
                ],
                'equipment_purchase': [
                    {
                        'key': 'equipmentPurchasePrice',
                        'type': 'text',
                        'style': '25',
                        'label': '¿Cuánto  cuesta el {{ProjectEquipment}}?',
                        'prepend': '$',
                        'append': '',
                        'field': 'ProjectEquipmentPurchasePrice',
                        'validator': { 'format': 'currency', 'min': 0, 'max': 1000000 },
                        'actions': []
                    }
                ],
                'equipment_rent': [
                    {
                        'key': 'equipmentMonthlyCost',
                        'type': 'text',
                        'style': '25',
                        'label': '¿Cuánto costaría el {{ProjectEquipment}} mensualmente?',
                        'prepend': '$',
                        'append': 'por mes',
                        'field': 'ProjectEquipmentMonthlyCost',
                        'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
                        'actions': []
                    }
                ],
                'different_equipment': [
                    {
                        'key': 'equipment',
                        'type': 'text',
                        'style': '100',
                        'label': '¿Qué equipo planeas usar para mover los materiales entre pisos?',
                        'prepend': '',
                        'append': '',
                        'field': 'ProjectEquipment',
                        'actions': ['add:different_equipment_cost,high_rise_remaining', 'setNext:finish']
                    }
                ],
                'different_equipment_cost': [
                    {
                        'key': 'equipmentPurchasePrice',
                        'type': 'text',
                        'style': '25',
                        'label': '¿Cuánto costaría el {{ProjectEquipment}} mensualmente?',
                        'prepend': '$',
                        'append': 'por mes',
                        'field': 'ProjectEquipmentPurchasePrice',
                        'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
                        'actions': []
                    }
                ],
                'high_rise_remaining': [
                    {
                        'key': 'picksPerDay',
                        'type': 'text',
                        'style': '25',
                        'label': '¿Cuántas cargas de materiales estás planeando mover (de forma realista) con el {{ProjectEquipment}}?',
                        'field': 'ProjectPicksPerDay',
                        'prepend': '',
                        'append': 'cargas de materiales',
                        'validator': { 'format': 'number', 'min': 0, 'max': 500 },
                        'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                    },
                    {
                        'key': 'picksPerDayNeeded',
                        'type': 'selection',
                        'style': '25',
                        'label': '¿Cuántas cargas de materiales más <em>te gustaría</em> mover por día?',
                        'prepend': '',
                        'append': 'mas cargas',
                        'field': 'ProjectPicksPerDayNeedMultiplier',
                        'answers': [
                            {
                                'answer': '2 veces',
                                'value': '2',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '3 veces',
                                'value': '3',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '4 veces',
                                'value': '4',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '5 veces',
                                'value': '5',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '6 veces',
                                'value': '6',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '7 veces',
                                'value': '7',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '8 veces',
                                'value': '8',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '9 veces',
                                'value': '9',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            },
                            {
                                'answer': '10 veces',
                                'value': '10',
                                'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
                            }
                        ]
                    },
                    {
                        'key': 'concurrentWorkFloors',
                        'type': 'text',
                        'style': '25',
                        'label': '¿En cuántos pisos podrías potencialmente estar haciendo trabajos al mismo tiempo?',
                        'prepend': '',
                        'append': 'pisos',
                        'field': 'ProjectConcurrentWorkFloors',
                        'validator': { 'format': 'number', 'min': 0, 'max': 50 },
                        'actions': []
                    },
                    {
                        'key': 'projectsPerYear',
                        'type': 'text',
                        'style': '25',
                        'label': 'En promedio, ¿cuántos proyectos haces por año?',
                        'prepend': '',
                        'append': 'proyectos por año',
                        'field': 'ProjectsPerYear',
                        'validator': { 'format': 'number', 'min': 1, 'max': 50 },
                        'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
                    }
                ],
                'decision_maker': [
                    {
                        'key': 'decisionMaker',
                        'type': 'selection',
                        'style': '100',
                        'label': 'For your convienience, should we forward the results to someone who has final say on the project plan?',
                        'answers': [
                            {
                                'answer': 'No thanks!',
                                'value': 'No',
                                'actions': ['store', 'thank_you:buyer']
                            },
                            {
                                'answer': 'It\'s me',
                                'value': 'Me',
                                'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
                            },
                            {
                                'answer': 'Yes please!',
                                'value': 'Yes',
                                'actions': ['store', 'label:Influencer', 'forward_to_decision_maker']
                            }
                        ]
                    }
                ],
                'no_construction_project': [
                    {
                        'key': 'whatYouDo',
                        'type': 'selection',
                        'style': '100',
                        'label': 'Nos gustaría conocerte mejor, dinos por favor  ¿a qué te dedicas?',
                        'field': 'AboutProspect',
                        'answers': [
                            {
                                'answer': 'Rento a mis clientes equipo de construcción',
                                'value': 'None',
                                'actions': ['store', 'label:RentalCompany,DecisionMaker', 'thank_you:renter']
                            },
                            {
                                'answer': 'Soy un desarrollador comercial de bienes raíces ',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            },
                            {
                                'answer': 'Soy un arquitecto',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            },
                            {
                                'answer': 'Soy un ingeniero civil (o de otro tipo)',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            },
                            {
                                'answer': 'Otro',
                                'value': 'None',
                                'actions': ['store', 'thank_you:mailOnly']
                            }
                        ]
                    }
                ],
                'equipment_renter': [
                    {
                        'type': 'selection',
                        'style': '100',
                        'label': 'Test question?',
                        'answers': [
                            {
                                'answer': 'Test answer',
                                'value': 'None',
                                'actions': ['store', 'equipment_renter']
                            }
                        ]
                    }
                ]
            }
        }
    };
    return Config;
}());



/***/ }),

/***/ "./src/app/customer-info/customer-info.component.html":
/*!************************************************************!*\
  !*** ./src/app/customer-info/customer-info.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"customer-info form__field--set\" *ngIf=\"!isSubmitted\">\n\t<div class=\"customer-info__row form__field--row\">\n\t\t<div class=\"question form__field--col form-col-100 company text\" [class.error]=\"company.invalid && (company.dirty || company.touched)\">\n\t\t\t<label for=\"{{key}}_company\" class=\"customer-info__label form__field--label\">{{labels.company}}</label>\n\t\t\t<div class=\"question__field form__field--input-container\">\n\t\t\t\t<input type=\"text\" id=\"{{key}}_company\" name=\"{{key}}_company\" class=\"customer-info__text form__field--input\" [(ngModel)]=\"prospect.CompanyName\" required #company=\"ngModel\" />\n\t\t\t</div>\n\t\t\t<div *ngIf=\"company.invalid && (company.dirty || company.touched)\" class=\"form__field--error\">\n\t\t\t\t<p *ngIf=\"company.errors.required\">* Required</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class=\"question form__field--row\">\n\t\t<div class=\"question form__field--col form-col-50 first-name text\" [class.error]=\"firstName.invalid && (firstName.dirty || firstName.touched)\">\n\t\t\t<label for=\"{{key}}_firstName\" class=\"customer-info__label form__field--label\">{{labels.firstName}}</label>\n\t\t\t<div class=\"question__field form__field--input-container\">\n\t\t\t\t<input type=\"text\" id=\"{{key}}_firstName\" name=\"{{key}}_firstName\" class=\"customer-info__text form__field--input\" [(ngModel)]=\"prospect.FirstName\" required #firstName=\"ngModel\" />\n\t\t\t</div>\n\t\t\t<div *ngIf=\"firstName.invalid && (firstName.dirty || firstName.touched)\" class=\"form__field--error\">\n\t\t\t\t<p *ngIf=\"firstName.errors.required\">* Required</p>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"question form__field--col form-col-50 last-name text\" [class.error]=\"lastName.invalid && (lastName.dirty || lastName.touched)\">\n\t\t\t<label for=\"{{key}}_name\" class=\"customer-info__label form__field--label\">{{labels.lastName}}</label>\n\t\t\t<div class=\"question__field form__field--input-container\">\n\t\t\t\t<input type=\"text\" id=\"{{key}}_lastName\" name=\"{{key}}_lastName\" class=\"customer-info__text form__field--input\" [(ngModel)]=\"prospect.LastName\" required #lastName=\"ngModel\" />\n\t\t\t</div>\n\t\t\t<div *ngIf=\"lastName.invalid && (lastName.dirty || lastName.touched)\" class=\"form__field--error\">\n\t\t\t\t<p *ngIf=\"lastName.errors.required\">* Required</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class=\"customer-info__row form__field--row\">\n\t\t<div class=\"question form__field--col form-col-50  phone-number text\" [class.error]=\"phoneNumber.invalid && (phoneNumber.dirty || phoneNumber.touched)\">\n\t\t\t<label for=\"{{key}}_phoneNumber\" class=\"customer-info__label form__field--label\">{{labels.phoneNumber}}</label>\n\t\t\t<div class=\"question__field form__field--input-container\">\n\t\t\t\t<input type=\"text\" id=\"{{key}}_phoneNumber\" name=\"{{key}}_phoneNumber\" class=\"customer-info__text form__field--input\" [(ngModel)]=\"prospect.PhoneNumber\" required #phoneNumber=\"ngModel\" />\n\t\t\t</div>\n\t\t\t<div *ngIf=\"(phoneNumber.invalid || !isValidPhoneNumber(prospect.PhoneNumber)) && (phoneNumber.dirty || phoneNumber.touched)\" class=\"form__field--error\">\n\t\t\t\t<p *ngIf=\"phoneNumber.errors && phoneNumber.errors.required\">* Required</p>\n\t\t\t\t<p *ngIf=\"!phoneNumber.errors && !isValidPhoneNumber(prospect.PhoneNumber)\">* Invalid</p>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"question form__field--col form-col-50 email-address text\" [class.error]=\"emailAddress.invalid && (emailAddress.dirty || emailAddress.touched)\">\n\t\t\t<label for=\"{{key}}_emailAddress\" class=\"customer-info__label form__field--label\">{{labels.emailAddress}}</label>\n\t\t\t<div class=\"question__field form__field--input-container\">\n\t\t\t\t<input type=\"text\" id=\"{{key}}_emailAddress\" name=\"{{key}}_emailAddress\" class=\"customer-info__text form__field--input\" [(ngModel)]=\"prospect.EmailAddress\" required #emailAddress=\"ngModel\" />\n\t\t\t</div>\n\t\t\t<div *ngIf=\"(emailAddress.invalid || !isValidEmailAddress(prospect.EmailAddress)) && (emailAddress.dirty || emailAddress.touched)\" class=\"form__field--error\">\n\t\t\t\t<p *ngIf=\"emailAddress.errors && emailAddress.errors.required\">* Required</p>\n\t\t\t\t<p *ngIf=\"!emailAddress.errors && !isValidEmailAddress(prospect.EmailAddress)\">* Invalid</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class=\"customer-info__row form__field--row button\">\n\t\t<input type=\"button\" class=\"customer-info__button form__field--button\" value=\"{{buttonText}}\" [disabled]=\"!isFormValid()\" (click)=\"onSubmit()\" />\n\t</div>\n</div>\n<div class=\"customer-info form__field--set\" *ngIf=\"isSubmitted && successText\">\n\t<p class=\"customer-info__success\">{{successText}}</p>\n</div>\n"

/***/ }),

/***/ "./src/app/customer-info/customer-info.component.scss":
/*!************************************************************!*\
  !*** ./src/app/customer-info/customer-info.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVyLWluZm8vY3VzdG9tZXItaW5mby5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/customer-info/customer-info.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/customer-info/customer-info.component.ts ***!
  \**********************************************************/
/*! exports provided: CustomerInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerInfoComponent", function() { return CustomerInfoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../config */ "./src/app/config.ts");
/* harmony import */ var _prospect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../prospect */ "./src/app/prospect.ts");




var CustomerInfoComponent = /** @class */ (function () {
    function CustomerInfoComponent() {
        this.submitted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.language = null;
        this.key = null;
        this.title = null;
        this.buttonText = null;
        this.successText = null;
        this.config = null;
        this.labels = null;
        this.prospect = new _prospect__WEBPACK_IMPORTED_MODULE_3__["Prospect"]();
        this.isSubmitted = false;
    }
    CustomerInfoComponent.prototype.ngOnInit = function () {
        this.config = _config__WEBPACK_IMPORTED_MODULE_2__["Config"].SETTINGS[this.language];
        this.labels = this.config.customerInfo.fields;
    };
    CustomerInfoComponent.prototype.isValidPhoneNumber = function (phone) {
        // const PHONE_REGEXP = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        // phone = phone.match(/\d+/g).map(Number);
        // return PHONE_REGEXP.test(phone);
        // TODO: I have to come in and validate phone number, but it needs to
        // support Mexico and US at the moment.
        return true;
    };
    CustomerInfoComponent.prototype.isValidEmailAddress = function (email) {
        var REGEXP = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        return REGEXP.test(email);
    };
    CustomerInfoComponent.prototype.isFormValid = function () {
        return this.prospect.CompanyName &&
            this.prospect.FirstName &&
            this.prospect.LastName &&
            this.prospect.PhoneNumber &&
            this.isValidPhoneNumber(this.prospect.PhoneNumber) &&
            this.prospect.EmailAddress &&
            this.isValidEmailAddress(this.prospect.EmailAddress);
    };
    CustomerInfoComponent.prototype.onSubmit = function () {
        if (this.isFormValid()) {
            this.isSubmitted = true;
            this.submitted.emit({ 'prospect': this.prospect });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('submitted'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CustomerInfoComponent.prototype, "submitted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CustomerInfoComponent.prototype, "language", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CustomerInfoComponent.prototype, "key", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CustomerInfoComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CustomerInfoComponent.prototype, "buttonText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CustomerInfoComponent.prototype, "successText", void 0);
    CustomerInfoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'customer-info',
            template: __webpack_require__(/*! ./customer-info.component.html */ "./src/app/customer-info/customer-info.component.html"),
            styles: [__webpack_require__(/*! ./customer-info.component.scss */ "./src/app/customer-info/customer-info.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CustomerInfoComponent);
    return CustomerInfoComponent;
}());



/***/ }),

/***/ "./src/app/finished/finished.component.html":
/*!**************************************************!*\
  !*** ./src/app/finished/finished.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kina-funnel__finished\">\n\t<header class=\"kina-funnel__finished--header\">\n<!--\n\t\t<h1 class=\"kina-funnel__finished--title\">Thank You!</h1>\n\t\t<h2 class=\"kina-funnel__finished--subtitle\">You will receive your results shortly!</h2>\n-->\n\t\t<h1 class=\"kina-funnel__finished--title\">Muchas Gracias!</h1>\n\t\t<h2 class=\"kina-funnel__finished--subtitle\">Vas a recibir tus resultados proximamente.</h2>\n\n\t</header>\n</div>\n"

/***/ }),

/***/ "./src/app/finished/finished.component.scss":
/*!**************************************************!*\
  !*** ./src/app/finished/finished.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmlzaGVkL2ZpbmlzaGVkLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finished/finished.component.ts":
/*!************************************************!*\
  !*** ./src/app/finished/finished.component.ts ***!
  \************************************************/
/*! exports provided: FinishedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinishedComponent", function() { return FinishedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FinishedComponent = /** @class */ (function () {
    function FinishedComponent() {
        this.finish = null;
    }
    FinishedComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FinishedComponent.prototype, "finish", void 0);
    FinishedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'finished',
            template: __webpack_require__(/*! ./finished.component.html */ "./src/app/finished/finished.component.html"),
            styles: [__webpack_require__(/*! ./finished.component.scss */ "./src/app/finished/finished.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FinishedComponent);
    return FinishedComponent;
}());



/***/ }),

/***/ "./src/app/forward/forward.component.html":
/*!************************************************!*\
  !*** ./src/app/forward/forward.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"forward\">\n\t<customer-info key=\"forward\" title=\"\" buttonText=\"\"></customer-info>\n</div>"

/***/ }),

/***/ "./src/app/forward/forward.component.scss":
/*!************************************************!*\
  !*** ./src/app/forward/forward.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZvcndhcmQvZm9yd2FyZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/forward/forward.component.ts":
/*!**********************************************!*\
  !*** ./src/app/forward/forward.component.ts ***!
  \**********************************************/
/*! exports provided: ForwardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForwardComponent", function() { return ForwardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ForwardComponent = /** @class */ (function () {
    function ForwardComponent() {
    }
    ForwardComponent.prototype.ngOnInit = function () {
    };
    ForwardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forward',
            template: __webpack_require__(/*! ./forward.component.html */ "./src/app/forward/forward.component.html"),
            styles: [__webpack_require__(/*! ./forward.component.scss */ "./src/app/forward/forward.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ForwardComponent);
    return ForwardComponent;
}());



/***/ }),

/***/ "./src/app/funnel.service.ts":
/*!***********************************!*\
  !*** ./src/app/funnel.service.ts ***!
  \***********************************/
/*! exports provided: FunnelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FunnelService", function() { return FunnelService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./config */ "./src/app/config.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");





var FunnelService = /** @class */ (function () {
    function FunnelService(httpClient) {
        this.httpClient = httpClient;
    }
    FunnelService.prototype.getQuestions = function (root, language) {
        console.log(language);
        return _config__WEBPACK_IMPORTED_MODULE_3__["Config"].SETTINGS[language].questions[root];
    };
    FunnelService.prototype.createProspect = function (prospect) {
        return this.httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].api + "/prospect/create/", prospect);
    };
    FunnelService.prototype.updateProspect = function (prospect, response) {
        return this.httpClient.put(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].api + "/prospect/update/", { 'prospect': prospect, 'response': response });
    };
    FunnelService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], FunnelService);
    return FunnelService;
}());



/***/ }),

/***/ "./src/app/funnel/funnel.component.html":
/*!**********************************************!*\
  !*** ./src/app/funnel/funnel.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kina-funnel\" *ngIf=\"!finish\">\n\t<header class=\"kina-funnel__header\">\n\t\t<h1 class=\"kina-funnel__title\" [innerHTML]=\"config.introduction\"></h1>\n\t\t<h2 class=\"kina-funnel__subtitle\" [innerHTML]=\"config.subIntroduction\"></h2>\n\t</header>\n\t<div class=\"kina-funnel__form\">\n\t\t<div class=\"kina-funnel__instructions\">\n\t\t\t<p [innerHTML]=\"config.instructions\"></p>\n\t\t</div>\n\n\t\t<div class=\"kina-funnel__customer\">\n\t\t\t<customer-info key=\"prospect\" [language]=\"language\" [title]=\"config.customerInfo.title\" [buttonText]=\"config.customerInfo.buttonText\" [successText]=\"config.customerInfo.successText\" (submitted)=\"onProspectSubmitted($event)\"></customer-info>\n\t\t</div>\n\n\t\t<div class=\"kina-funnel__questions form__field--set\" *ngIf=\"prospect\">\n\t\t\t<div class=\"kina-funnel__question--group\" *ngFor=\"let group of objectKeys(questionGroups)\">\n\t\t\t\t<div class=\"kina-funnel__question\" *ngFor=\"let question of questionGroups[group]\">\n\t\t\t\t\t<question-selection *ngIf=\"question.type === 'selection'\" [question]=\"question\" (question-answered)=\"onQuestionAnswered($event)\"></question-selection>\n\t\t\t\t\t<question-text *ngIf=\"question.type === 'text'\" [question]=\"question\" (question-answered)=\"onQuestionAnswered($event)\"></question-text>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"kina-funnel__next\" *ngIf=\"nextButton && !thankYou\">\n\t\t\t\t<button class=\"kina-funnel__next--button\" type=\"button\" (click)=\"onNextClick()\">{{nextButton}}</button>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"kina-funnel__thankyou\" *ngIf=\"thankYou\">\n\t\t\t<thank-you [thankYou]=\"thankYou\" (submitted)=\"onSubmitted($event)\"></thank-you>\n\t\t</div>\n\t</div>\n</div>\n<finished [finish]=\"finish\" *ngIf=\"finish\"></finished>\n"

/***/ }),

/***/ "./src/app/funnel/funnel.component.scss":
/*!**********************************************!*\
  !*** ./src/app/funnel/funnel.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Z1bm5lbC9mdW5uZWwuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/funnel/funnel.component.ts":
/*!********************************************!*\
  !*** ./src/app/funnel/funnel.component.ts ***!
  \********************************************/
/*! exports provided: FunnelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FunnelComponent", function() { return FunnelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _funnel_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../funnel.service */ "./src/app/funnel.service.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../config */ "./src/app/config.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");





var FunnelComponent = /** @class */ (function () {
    // *******************************************************************
    // Initialization
    // *******************************************************************
    function FunnelComponent(funnelService) {
        this.funnelService = funnelService;
        // This allows us to iterate over object keys in the view
        this.objectKeys = Object.keys;
        // *******************************************************************
        // Properties
        // *******************************************************************
        this.config = null;
        this.prospect = null;
        this.answers = null;
        this.questionGroups = {};
        this.nextButton = '';
        this.thankYou = null;
        this.finish = null;
        this.response = {};
    }
    FunnelComponent.prototype.ngOnInit = function () {
        if (!this.language) {
            this.language = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].lang;
        }
        this.config = _config__WEBPACK_IMPORTED_MODULE_3__["Config"].SETTINGS[this.language];
        var questions = this.funnelService.getQuestions('root', this.language);
        this.questionGroups['root'] = questions;
        this.nextButton = this.config.nextButton.next;
    };
    // *******************************************************************
    // Methods
    // *******************************************************************
    FunnelComponent.prototype.onQuestionAnswered = function (response) {
        if (response.question.field) {
            this.response[response.question.field] = response.answer;
        }
        switch (response.question.type) {
            case 'selection':
                for (var answer in response.question.answers) {
                    if (response.question.answers[answer].value === response.answer) {
                        this.processActions(response.question, response.question.answers[answer], response.question.answers[answer].actions);
                        return;
                    }
                }
                break;
            default:
                this.processActions(response.question, response.answer, response.question.actions);
                break;
        }
    };
    FunnelComponent.prototype.processActions = function (question, answer, actions) {
        //console.log('processAction', question, answer, actions);
        for (var action in actions) {
            var a = actions[action].split(':');
            switch (a[0]) {
                case 'store':
                    this.storeOption(question, answer);
                    break;
                case 'label':
                    this.labelOption(question, answer, a[1]);
                    break;
                case 'set':
                    this.setProperty(question, answer, a);
                    break;
                case 'setNext':
                    this.setNext(a[1]);
                    break;
                case 'multiply':
                    this.multiply(question, answer, a);
                    break;
                case 'forward_to_decision_maker':
                    this.forwardToDecisionMaker();
                    break;
                case 'thank_you':
                    this.showThankYou(a[1]);
                    break;
                case 'add':
                    this.addQuestionSet(question, a[1]);
                    break;
            }
        }
    };
    FunnelComponent.prototype.storeOption = function (question, answer) {
        console.log('storeOption', this.prospect, this.response);
        // TODO: Call the createContact service method
        this.funnelService.updateProspect(this.prospect, this.response).subscribe(function (res) {
            console.log("Updated a prospect");
        });
    };
    FunnelComponent.prototype.labelOption = function (question, answer, label) {
        // TODO: Add labels to the prospect profile. This is comma delimited.
        //console.log('labelOption', question, answer);
    };
    FunnelComponent.prototype.setProperty = function (question, answer, property) {
        this.response[property[1]] = property[2];
    };
    FunnelComponent.prototype.setNext = function (next) {
        // NOTE: I decided to not do this, because it's confusing if the
        // user clicks "Finish", then there's another step after. If this
        // works out, I'll come back in and remove all this.
        //this.nextButton = this.config.nextButton[next];
    };
    FunnelComponent.prototype.multiply = function (question, answer, property) {
        if (!this.response[property[2]] || !this.response[property[3]]) {
            return;
        }
        this.response[property[1]] = this.response[property[2]] * this.response[property[3]];
    };
    FunnelComponent.prototype.forwardToDecisionMaker = function () {
        console.log('Displaying forward to decision maker');
    };
    FunnelComponent.prototype.showThankYou = function (thankYou) {
        this.thankYou = this.config.thankYou[thankYou];
        console.log(this.thankYou);
    };
    FunnelComponent.prototype.addQuestionSet = function (originalQuestion, root) {
        // Check the original question for the root and see if the
        // user went back and answered a question they already answered.
        // If they did, we have to remove any questions added after
        // that and then we can add the new question set.
        this.questionGroups = this.findQuestionByKey(originalQuestion.key);
        var roots = root.split(',');
        for (var index in roots) {
            var root = roots[index];
            var questions = this.funnelService.getQuestions(root, this.language);
            // Replace any variables in the questin labels
            // TODO: Move this to its own method
            for (var question in questions) {
                var regex = /{{([a-zA-Z0-9]*)}}/g, matches = questions[question].label.match(regex);
                for (var match in matches) {
                    // TODO: Add support for any propery.
                    questions[question].label = questions[question].label
                        .replace(/{{ProjectEquipment}}/g, this.response['ProjectEquipment']);
                }
            }
            this.questionGroups[root] = questions;
        }
    };
    FunnelComponent.prototype.findQuestionByKey = function (key) {
        var questionGroups = {};
        for (var root in this.questionGroups) {
            questionGroups[root] = this.questionGroups[root];
            for (var index in this.questionGroups[root]) {
                if (this.questionGroups[root][index].key === key) {
                    return questionGroups;
                }
            }
        }
        return false;
    };
    // This is the initial contact creation handler
    FunnelComponent.prototype.onProspectSubmitted = function (prospect) {
        this.prospect = prospect;
        // TODO: Call the createContact service method
        this.funnelService.createProspect(this.prospect).subscribe(function (res) {
            console.log("Created a prospect");
        });
    };
    // This button doesn't really do anything. It just serves as
    // bait to make the user blur any text field that might be at
    // the end of a question group. LOL
    FunnelComponent.prototype.onNextClick = function () {
        console.log('Thanks for clicking next! :)');
        // TODO: Log an event to GA
    };
    // This is the final submission button
    FunnelComponent.prototype.onSubmitted = function () {
        // TODO: Do the final save
        console.log('submitted', this.prospect, this.response);
        // Show the finished message
        this.finish = this.config.finish;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FunnelComponent.prototype, "language", void 0);
    FunnelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'funnel',
            template: __webpack_require__(/*! ./funnel.component.html */ "./src/app/funnel/funnel.component.html"),
            styles: [__webpack_require__(/*! ./funnel.component.scss */ "./src/app/funnel/funnel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_funnel_service__WEBPACK_IMPORTED_MODULE_2__["FunnelService"]])
    ], FunnelComponent);
    return FunnelComponent;
}());



/***/ }),

/***/ "./src/app/prospect.ts":
/*!*****************************!*\
  !*** ./src/app/prospect.ts ***!
  \*****************************/
/*! exports provided: Prospect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Prospect", function() { return Prospect; });
var Prospect = /** @class */ (function () {
    function Prospect() {
    }
    return Prospect;
}());



/***/ }),

/***/ "./src/app/question-selection/question-selection.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/question-selection/question-selection.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"question form__field--row selection\" [class.error]=\"field.invalid && (field.dirty || field.touched)\">\n\t<div class=\"question form__field--col form-col-{{question.style}}\">\n\t\t<label for=\"{{question.key}}_answer\" class=\"question-selection__label form__field--label\" [innerHTML]=\"question.label\"></label>\n\t\t<div class=\"question__field form__field--input-container\"\n\t\t\t[attr.data-prepend]=\"question.prepend ? question.prepend : null\"\n\t\t\t[attr.data-append]=\"question.append ? question.append : null\"\n\t\t>\n\t\t\t<select id=\"{{question.key}}_answer\" name=\"{{question.key}}_answer\" class=\"question__select form__field--input\"\n\t\t\t\t[(ngModel)]=\"answer\" (ngModelChange)=\"onAnswer(field)\"\n\t\t\t\trequired #field=\"ngModel\"\n\t\t\t>\n\t\t\t\t<option value=\"\">[Select One]</option>\n\t\t\t\t<option *ngFor=\"let answer of question.answers\" value=\"{{answer.value}}\">{{answer.answer}}</option>\n\t\t\t</select>\n\t\t</div>\n\t\t<div *ngIf=\"field.invalid && (field.dirty || field.touched)\" class=\"form__field--error\">\n\t\t\t<p *ngIf=\"field.errors.required\">* Required</p>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/question-selection/question-selection.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/question-selection/question-selection.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".question__field:before {\n  content: attr(data-prepend); }\n\n.question__field:after {\n  content: attr(data-append); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcXVlc3Rpb24tc2VsZWN0aW9uL0M6XFxfZGV2XFxraW5hXFxXZWJzaXRlXFxfXFx3d3dcXEZ1bm5lbFxca2luYS1mdW5uZWwvc3JjXFxhcHBcXHF1ZXN0aW9uLXNlbGVjdGlvblxccXVlc3Rpb24tc2VsZWN0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtFO0VBRUMsMkJBQTJCLEVBQUE7O0FBRjVCO0VBTUMsMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9xdWVzdGlvbi1zZWxlY3Rpb24vcXVlc3Rpb24tc2VsZWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnF1ZXN0aW9uIHtcclxuXHQmLnNlbGVjdGlvbiB7XHJcblxyXG5cdH1cclxuXHJcblx0Jl9fZmllbGQge1xyXG5cdFx0JjpiZWZvcmUge1xyXG5cdFx0XHRjb250ZW50OiBhdHRyKGRhdGEtcHJlcGVuZCk7XHJcblx0XHR9XHJcblxyXG5cdFx0JjphZnRlciB7XHJcblx0XHRcdGNvbnRlbnQ6IGF0dHIoZGF0YS1hcHBlbmQpO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/question-selection/question-selection.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/question-selection/question-selection.component.ts ***!
  \********************************************************************/
/*! exports provided: QuestionSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionSelectionComponent", function() { return QuestionSelectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuestionSelectionComponent = /** @class */ (function () {
    function QuestionSelectionComponent() {
        this.questionAnswered = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.question = null;
        this.answer = '';
    }
    QuestionSelectionComponent.prototype.ngOnInit = function () {
    };
    QuestionSelectionComponent.prototype.onAnswer = function (field) {
        if (!field.invalid) {
            this.questionAnswered.emit({ 'question': this.question, 'answer': this.answer });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('question-answered'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], QuestionSelectionComponent.prototype, "questionAnswered", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], QuestionSelectionComponent.prototype, "question", void 0);
    QuestionSelectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'question-selection',
            template: __webpack_require__(/*! ./question-selection.component.html */ "./src/app/question-selection/question-selection.component.html"),
            styles: [__webpack_require__(/*! ./question-selection.component.scss */ "./src/app/question-selection/question-selection.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuestionSelectionComponent);
    return QuestionSelectionComponent;
}());



/***/ }),

/***/ "./src/app/question-text/question-text.component.html":
/*!************************************************************!*\
  !*** ./src/app/question-text/question-text.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"question form__field--row text\" [class.error]=\"field.invalid && (field.dirty || field.touched)\">\n\t<div class=\"question form__field--col form-col-{{question.style}}\">\n\t\t<label for=\"{{question.key}}_answer\" class=\"question__label form__field--label\" [innerHTML]=\"question.label\"></label>\n\t\t<div class=\"question__field form__field--input-container\"\n\t\t\t[attr.data-prepend]=\"question.prepend ? question.prepend : null\"\n\t\t\t[attr.data-append]=\"question.append ? question.append : null\"\n\t\t>\n\t\t\t<input id=\"{{question.key}}_answer\" name=\"{{question.key}}_answer\" type=\"{{type}}\" class=\"question__answer form__field--input\"\n\n\t\t\t\t[attr.min]=\"question.validator && question.validator.min >= 0 ? question.validator.min : null\"\n\t\t\t\t[attr.max]=\"question.validator && question.validator.max ? question.validator.max : null\"\n\t\t\t\t[attr.step]=\"question.validator && question.validator.step ? question.validator.step : null\"\n\t\t\t\t\n\t\t\t\t[(ngModel)]=\"answer\" (blur)=\"onAnswer(field)\"\n\t\t\t\trequired #field=\"ngModel\" />\n\t\t</div>\n\t\t<div *ngIf=\"field.invalid && (field.dirty || field.touched)\" class=\"form__field--error\">\n\t\t\t<p *ngIf=\"field.errors.required\">* Required</p>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/question-text/question-text.component.scss":
/*!************************************************************!*\
  !*** ./src/app/question-text/question-text.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".question__field:before {\n  content: attr(data-prepend); }\n\n.question__field:after {\n  content: attr(data-append); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcXVlc3Rpb24tdGV4dC9DOlxcX2Rldlxca2luYVxcV2Vic2l0ZVxcX1xcd3d3XFxGdW5uZWxcXGtpbmEtZnVubmVsL3NyY1xcYXBwXFxxdWVzdGlvbi10ZXh0XFxxdWVzdGlvbi10ZXh0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtFO0VBRUMsMkJBQTJCLEVBQUE7O0FBRjVCO0VBTUMsMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9xdWVzdGlvbi10ZXh0L3F1ZXN0aW9uLXRleHQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucXVlc3Rpb24ge1xyXG5cdCYudGV4dCB7XHJcblxyXG5cdH1cclxuXHJcblx0Jl9fZmllbGQge1xyXG5cdFx0JjpiZWZvcmUge1xyXG5cdFx0XHRjb250ZW50OiBhdHRyKGRhdGEtcHJlcGVuZCk7XHJcblx0XHR9XHJcblxyXG5cdFx0JjphZnRlciB7XHJcblx0XHRcdGNvbnRlbnQ6IGF0dHIoZGF0YS1hcHBlbmQpO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/question-text/question-text.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/question-text/question-text.component.ts ***!
  \**********************************************************/
/*! exports provided: QuestionTextComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionTextComponent", function() { return QuestionTextComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuestionTextComponent = /** @class */ (function () {
    function QuestionTextComponent() {
        this.questionAnswered = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.question = null;
        this.type = 'text';
        this.answer = '';
    }
    QuestionTextComponent.prototype.ngOnInit = function () {
    };
    QuestionTextComponent.prototype.ngOnChanges = function () {
        if (!this.question.validator) {
            return;
        }
        switch (this.question.validator.format) {
            case 'currency':
            case 'number':
                this.type = 'number';
                break;
        }
    };
    QuestionTextComponent.prototype.onAnswer = function (field) {
        if (!field.invalid) {
            this.questionAnswered.emit({ 'question': this.question, 'answer': this.answer });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('question-answered'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], QuestionTextComponent.prototype, "questionAnswered", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], QuestionTextComponent.prototype, "question", void 0);
    QuestionTextComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'question-text',
            template: __webpack_require__(/*! ./question-text.component.html */ "./src/app/question-text/question-text.component.html"),
            styles: [__webpack_require__(/*! ./question-text.component.scss */ "./src/app/question-text/question-text.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuestionTextComponent);
    return QuestionTextComponent;
}());



/***/ }),

/***/ "./src/app/thank-you/thank-you.component.html":
/*!****************************************************!*\
  !*** ./src/app/thank-you/thank-you.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kina-funnel__thankYou\">\n\t<p class=\"kina-funnel__thankYou--body\" [innerHTML]=\"thankYou.body\"></p>\n\t<button class=\"kina-funnel__thankYou--button\" (click)=\"onClick()\">{{thankYou.buttonText}}</button>\n</div>\n"

/***/ }),

/***/ "./src/app/thank-you/thank-you.component.scss":
/*!****************************************************!*\
  !*** ./src/app/thank-you/thank-you.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RoYW5rLXlvdS90aGFuay15b3UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/thank-you/thank-you.component.ts":
/*!**************************************************!*\
  !*** ./src/app/thank-you/thank-you.component.ts ***!
  \**************************************************/
/*! exports provided: ThankYouComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThankYouComponent", function() { return ThankYouComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ThankYouComponent = /** @class */ (function () {
    function ThankYouComponent() {
        this.submitted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.thankYou = null;
        this.onClick = function () {
            this.submitted.emit();
        };
    }
    ThankYouComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('submitted'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ThankYouComponent.prototype, "submitted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ThankYouComponent.prototype, "thankYou", void 0);
    ThankYouComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'thank-you',
            template: __webpack_require__(/*! ./thank-you.component.html */ "./src/app/thank-you/thank-you.component.html"),
            styles: [__webpack_require__(/*! ./thank-you.component.scss */ "./src/app/thank-you/thank-you.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ThankYouComponent);
    return ThankYouComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    api: 'http://dev.kinadistributions.com:8080/wp-json/kina/v1',
    lang: 'en'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\_dev\kina\Website\_\www\Funnel\kina-funnel\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map