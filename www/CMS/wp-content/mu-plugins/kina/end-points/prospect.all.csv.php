<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */

function kina_endpoint_prospect_allCsv($request) {
	$projects = kina_get_funnel_results_all();
	
	$import = array();
	$fields = kina_get_funnel_export_header();

	array_push($import, implode(',', $fields));
	foreach($projects as $project) {
		$record = array();
		foreach($fields as $field) {
			$record[$field] = $project[$field];
		}
		array_push($import, implode(',', $record));
	}

	return array(
		'status' => 'ok',
		'results' => $import
	);
}
