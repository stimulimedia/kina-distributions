<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */

function kina_endpoint_prospect_all($request) {
	$projects = kina_get_funnel_results_all();
	
	return array(
		'status' => 'ok',
		'results' => $projects
	);
}