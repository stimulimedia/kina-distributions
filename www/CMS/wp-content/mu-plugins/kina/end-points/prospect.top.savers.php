<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */

function kina_endpoint_prospect_topSavers($request) {
	$params = $request->get_params();

	if ($params && $params['limit']) {
		$limit = $params['limit'];
	} else {
		$limit = 5;
	}

	$projects = kina_get_funnel_results_winners($limit);
	
	return array(
		'status' => 'ok',
		'results' => $projects
	);
}