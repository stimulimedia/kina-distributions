<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */

function kina_endpoint_prospect_create($request) {
	$params = $request->get_params();
	if (!$params) {
		// TODO: Throw and error instead
		return;
	}

	$ID = kina_create_propsect($params);
		
	return array(
		'status' => 'ok',
		'results' => $ID
	);
}