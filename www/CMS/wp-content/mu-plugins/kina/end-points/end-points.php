<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */


include('prospect.create.php');
include('prospect.update.php');
include('prospect.add.labels.php');
include('prospect.all.php');
include('prospect.all.csv.php');
include('prospect.top.savers.php');
include('prospect.most.recent.php');

add_action( 'rest_api_init', function () {
	register_rest_route( 'kina/v1', '/prospect/create/', array(
		'methods' => 'POST',
		'callback' => 'kina_endpoint_prospect_create',
	) );
	register_rest_route( 'kina/v1', '/prospect/update/', array(
		'methods' => 'PUT',
		'callback' => 'kina_endpoint_prospect_update',
	) );
	register_rest_route( 'kina/v1', '/prospect/add-labels/', array(
		'methods' => 'POST',
		'callback' => 'kina_endpoint_prospect_addLabels',
	) );
	register_rest_route( 'kina/v1', '/prospect/all/', array(
		'methods' => 'GET',
		'callback' => 'kina_endpoint_prospect_all',
	) );
	register_rest_route( 'kina/v1', '/prospect/all-csv/', array(
		'methods' => 'GET',
		'callback' => 'kina_endpoint_prospect_allCsv',
	) );
	register_rest_route( 'kina/v1', '/prospect/top-savers/', array(
		'methods' => 'POST',
		'callback' => 'kina_endpoint_prospect_topSavers',
	) );
	register_rest_route( 'kina/v1', '/prospect/most-recent/', array(
		'methods' => 'POST',
		'callback' => 'kina_endpoint_prospect_mostRecent',
	) );
} );



/**
 * Use * for origin
 */
// add_action( 'rest_api_init', function() {
// 	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
// 	add_filter( 'rest_pre_serve_request', function( $value ) {
// 		header( 'Access-Control-Allow-Origin: *' );
// 		header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
// 		header( 'Access-Control-Allow-Credentials: true' );

// 		return $value;
// 	});
// }, 15 );



// TODO: Switch to this one later so it's more secure. Will do it as the next thing when I have time. https://joshpress.net/access-control-headers-for-the-wordpress-rest-api/
// /**
//  * Only from certain origins
//  */

// add_action( 'rest_api_init', function() {
// 	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
// 	add_filter( 'rest_pre_serve_request', function( $value ) {

// 		$origin = get_http_origin();
// 		var_dump($origin);
// 		if ( $origin && in_array( $origin, array(
// 				//define some origins!
// 			) ) ) {
// 			header( 'Access-Control-Allow-Origin: ' . esc_url_raw( $origin ) );
// 			header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
// 			header( 'Access-Control-Allow-Credentials: true' );
// 		}

// 		return $value;
		
// 	});
// }, 15 );