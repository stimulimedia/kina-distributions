<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */

function kina_endpoint_prospect_update($request) {
	$params = $request->get_params();
	if (!$params) {
		// TODO: Throw and error instead
		return;
	}

	// Look up the prospect by email address
	$args = array(
		'post_type' => 'kina_prospects',
		'page_title' => $params['prospect']['EmailAddress'],
		'posts_per_page' => -1
	);
	$query = new WP_Query($args);

	if ($query->have_posts()) {
		$ID = $query->posts[0]->ID;
	}

	// If the prospect doesn't exist, create it
	if (!$ID) {
		$ID = kina_create_propsect($params['prospect']);
	}

	// Add basic properties
	if ($params['response']['ProjectUrgency']) {
		update_post_meta( $ID, 'ProjectUrgency', $params['response']['ProjectUrgency'] ); 
	}
	if ($params['response']['ProjectCity']) {
		update_post_meta( $ID, 'ProjectCity', $params['response']['ProjectCity'] ); 
	}
	if ($params['response']['ProjectMonths']) {
		update_post_meta( $ID, 'ProjectMonths', $params['response']['ProjectMonths'] ); 
	}
	if ($params['response']['ProjectFloors']) {
		update_post_meta( $ID, 'ProjectFloors', $params['response']['ProjectFloors'] ); 
	}
	if ($params['response']['ProjectBudget']) {
		update_post_meta( $ID, 'ProjectBudget', $params['response']['ProjectBudget'] );
	}

	// Add equipment properties
	if ($params['response']['ProjectEquipment']) {
		update_post_meta( $ID, 'ProjectEquipment', $params['response']['ProjectEquipment'] ); 
	}
	if ($params['response']['ProjectEquipmentPurchase']) {
		update_post_meta( $ID, 'ProjectEquipmentPurchase', $params['response']['ProjectEquipmentPurchase'] ); 
	}
	if ($params['response']['ProjectEquipmentPurchasePrice']) {
		update_post_meta( $ID, 'ProjectEquipmentPurchasePrice', $params['response']['ProjectEquipmentPurchasePrice'] ); 
	}
	if ($params['response']['ProjectEquipmentMonthlyCost']) {
		update_post_meta( $ID, 'ProjectEquipmentMonthlyCost', $params['response']['ProjectEquipmentMonthlyCost'] ); 
	}
	
	// Add performance properties
	if ($params['response']['ProjectPicksPerDay']) {
		update_post_meta( $ID, 'ProjectPicksPerDay', $params['response']['ProjectPicksPerDay'] ); 
	}
	if ($params['response']['ProjectPicksPerDayNeed']) {
		update_post_meta( $ID, 'ProjectPicksPerDayNeed', $params['response']['ProjectPicksPerDayNeed'] ); 
	}
	if ($params['response']['ProjectConcurrentWorkFloors']) {
		update_post_meta( $ID, 'ProjectConcurrentWorkFloors', $params['response']['ProjectConcurrentWorkFloors'] ); 
	}
	if ($params['response']['ProjectsPerYear']) {
		update_post_meta( $ID, 'ProjectsPerYear', $params['response']['ProjectsPerYear'] ); 
	}

	$savings = kina_calculate_project_savings($params['response']);
	
	// Add calculate properties
	if ($savings) {
		update_post_meta( $ID, 'ProjectEquipmentMonthlyCost', $savings['ProjectEquipmentMonthlyCost'] ); 
		update_post_meta( $ID, 'TowerCraneMonthlyCost', $savings['TowerCraneMonthlyCost'] ); 
		update_post_meta( $ID, 'TotalProjectCost', $savings['TotalProjectCost'] ); 
		update_post_meta( $ID, 'ProjectFewerMonths', $savings['ProjectFewerMonths'] ); 
		update_post_meta( $ID, 'TowerCraneFewerMonths', $savings['TowerCraneFewerMonths'] ); 

		update_post_meta( $ID, 'NumberOfDecks', $savings['NumberOfDecks'] ); 
		update_post_meta( $ID, 'PriceOfSet', $savings['PriceOfSet'] ); 
		update_post_meta( $ID, 'MonthlyCostLTD', $savings['MonthlyCostLTD'] ); 

		update_post_meta( $ID, 'ProjectSavings', $savings['ProjectSavings'] ); 
		update_post_meta( $ID, 'CostPerPickTC', $savings['CostPerPickTC'] ); 
		update_post_meta( $ID, 'CostPerPickLTD', $savings['CostPerPickLTD'] );
	}

	
	return array(
		'status' => 'ok',
		'results' => array(
			'ID' => $ID,
			'Savings' => $savings
		)
	);
}