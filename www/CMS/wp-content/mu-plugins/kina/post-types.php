<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */


function kina_add_admin_menu() { 
	add_menu_page( 'KINA Distributions', 'KINA Distributions', 'edit_others_posts', 'kina_menu', '', 'dashicons-admin-generic', 21 );
}	
add_action( 'admin_menu', 'kina_add_admin_menu' ); 


function kina_create_post_types() {
	register_post_type( 'kina_prospects',
		array(
			'labels' => array(
				'name' => __( 'Prospects' ),
				'singular_name' => __( 'Prospect' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'kina_prospects',
				'with_front' => false
			),
			'show_ui' => true,
			'show_in_menu' => 'kina_menu',
			'menu_position' => 20,
			'menu_icon' => 'dashicons-admin-generic',
			'exclude_from_search' => true,
		)
	);

	register_post_type( 'kina_appointments',
		array(
			'labels' => array(
				'name' => __( 'Appointments' ),
				'singular_name' => __( 'Appointment' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'kina_appointments',
				'with_front' => false
			),
			'show_ui' => true,
			'show_in_menu' => 'kina_menu',
			'menu_position' => 20,
			'menu_icon' => 'dashicons-admin-generic',
			'exclude_from_search' => true,
		)
	);
}
add_action( 'init', 'kina_create_post_types' ); 
