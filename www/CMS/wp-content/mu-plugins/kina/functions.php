<?php

function kina_create_propsect($params) {
	$post = array(
		'post_title' => $params['prospect']['EmailAddress'],
		'post_content' => '**NOT USED**',
		'post_author' => 1,
		'post_status' => 'publish',
		'post_type' => 'kina_prospects',
	);
	
	$ID = wp_insert_post( $post );
	
	update_post_meta( $ID, 'CompanyName', $params['prospect']['CompanyName'] ); 
	update_post_meta( $ID, 'FirstName', $params['prospect']['FirstName'] ); 
	update_post_meta( $ID, 'LastName', $params['prospect']['LastName'] ); 
	update_post_meta( $ID, 'PhoneNumber', $params['prospect']['PhoneNumber'] ); 
	update_post_meta( $ID, 'EmailAddress', $params['prospect']['EmailAddress'] );

	return $ID; 	
}

function kina_get_funnel_export_header() {
	$header = array(
		'CompanyName',
		'FirstName',
		'LastName',
		'PhoneNumber',
		'EmailAddress',
		'ProjectUrgency',
		'ProjectCity',
		'ProjectMonths',
		'ProjectFloors',
		'ProjectBudget',
		'ProjectEquipment',
		'ProjectEquipmentPurchase',
		'ProjectEquipmentMonthlyCost',
		'ProjectPicksPerDay',
		'ProjectPicksPerDayNeed',
		'ProjectFewerMonths',
		'ProjectConcurrentWorkFloors',
		'ProjectsPerYear',
		'ProjectSavings'
	);

	return $header;
}

function kina_get_funnel_results_all() {
	$args = array(
		'post_type' => 'kina_prospects',
		'orderby' => 'date',
		'order' => 'ASC',
		'posts_per_page' => -1
	);
	$query = new WP_Query($args);

	$projects = array();
	foreach ($query->posts as $post) {
		$result_item = array();
		$result_item['ID'] = $post->ID;

		foreach(get_post_meta($post->ID) as $key => $value) {
			if ($key[0] !== '_' && $value[0]) {
				$result_item[$key] = $value[0];
			}
		}

		array_push($projects, $result_item);
	}
	
	return $projects;
}

function kina_get_funnel_results_recent($limit) {
	$args = array(
		'post_type' => 'kina_prospects',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => $limit
	);
	$query = new WP_Query($args);

	$projects = array();
	foreach ($query->posts as $post) {
		$result_item = array();
		$result_item = get_post_meta($post->ID);
		$result_item['ID'] = $post->ID;

		array_push($projects, $result_item);
	}
	
	return $projects;
}

function kina_get_funnel_results_winners($limit) {
	$args = array(
		'post_type' => 'kina_prospects',
		'orderby' => 'meta_value_num',
		'meta_key' => 'ProjectSavings',
		'order' => 'DESC',
		'posts_per_page' => $limit
	);
	$query = new WP_Query($args);

	$projects = array();
	foreach ($query->posts as $i => $post) {
		$result_item = array();
		$result_item = get_post_meta($post->ID);
		$result_item['ID'] = $post->ID;

		array_push($projects, $result_item);
	}

	return $projects;
}

function kina_calculate_project_savings($project) {
	$SAVINGS_FACTOR = .3;
	$REDUCTION_FACTOR = .2;
	$DAYS_PER_WEEK = 6;
	$WEEKS_PER_MONTH = 4;
	$DECK_THRESHOLD = 6;
	$DECK_FACTOR = .8;

	$PRICE_HOIST = 149787;
	$PRICE_DECK = 53700;
	$PRICE_ROOF_MOUNT = 14700;
	$PRICE_DRY_WALL_RACK = 0;
	$PRICE_PIPING_RACK = 0;

	// Constants
	// TODO: Tweak these numbers to make sure they are accurate
	$TowerCraneOperatorMonthly = 80000/12;
	$TowerCraneSpotterMonthly = 40000/12;
	$TowerCraneInsuranceMonthly = 10000;


	// Calculate the monthly cost of equipment if the prospect purchased the tower crane
	$ProjectEquipmentMonthlyCost = ($project['ProjectEquipmentPurchase'] === 'purchase') ? number_format($project['ProjectEquipmentPurchasePrice'] / $project['ProjectMonths'], 2) : $project['ProjectEquipmentMonthlyCost'];

	// Calculate the total cost the tower crane incurs each month
	$TotalMonthlyCost = 
		$ProjectEquipmentMonthlyCost + $TowerCraneOperatorMonthly +
		$TowerCraneSpotterMonthly + $TowerCraneInsuranceMonthly;

	// Calculate the total cost the tower crane incurs over the entire project
	$TotalProjectCost = $TotalMonthlyCost * $project['ProjectMonths'];

	// Calculate how many months the LTD can save the overal project
	$ProjectFewerMonths = (1 - ($project['ProjectPicksPerDay']/$project['ProjectPicksPerDayNeed'])) * $SAVINGS_FACTOR * $project['ProjectMonths'];

	// Calculate how many months before the end of the project the tower crane can come off the building
	// ** Not sure of the accuracy of this
	$TowerCraneFewerMonths = ($project['ProjectMonths'] - $ProjectFewerMonths) * $REDUCTION_FACTOR;

	// Calculate the total savings for the project
	$TotalProjectSavings = $TotalMonthlyCost * ($ProjectFewerMonths + $TowerCraneFewerMonths);

	// Calculate the cost per pick for the tower crane
	$CostPerPickTC = number_format($TotalProjectCost / ($project['ProjectPicksPerDay'] * $DAYS_PER_WEEK * $WEEKS_PER_MONTH * $project['ProjectMonths']), 2);

	// Calculate the cost per pick for the LTD
	$NumberOfDecks = ($project['ProjectConcurrentWorkFloors'] > $DECK_THRESHOLD) ? round($project['ProjectConcurrentWorkFloors'] * $DECK_THRESHOLD) : $project['ProjectConcurrentWorkFloors'];
	$PriceOfSet = $PRICE_HOIST + ($PRICE_DECK * $NumberOfDecks) + $PRICE_ROOF_MOUNT;
	// TODO: Determine whether to add the racks

	$CostPerPickLTD = $PriceOfSet / ($project['ProjectPicksPerDay'] * $DAYS_PER_WEEK * $WEEKS_PER_MONTH * $project['ProjectMonths']);

	$LTDProjectCost = kina_calculate_monthly_payment($PriceOfSet) * $project['ProjectMonths'];


	return array(
		'ProjectEquipmentMonthlyCost' => $ProjectEquipmentMonthlyCost,
		'TowerCraneMonthlyCost' => $TotalMonthlyCost,
		'TotalProjectCost' => $TotalProjectCost,
		'ProjectFewerMonths' => $ProjectFewerMonths,
		'TowerCraneFewerMonths' => $TowerCraneFewerMonths,

		'NumberOfDecks' => $NumberOfDecks,
		'PriceOfSet' => $PriceOfSet,
		'MonthlyCostLTD' => $MonthlyCostLTD, 

		'ProjectSavings' => $TotalProjectSavings,
		'CostPerPickTC' => $CostPerPickTC,
		'CostPerPickLTD' => $CostPerPickLTD,
	);
}

function kina_calculate_monthly_payment($principal) {
	$interest = .075;
	$term = 60;

	$payment = floor($principal * $interest / (1 - (exp(1 / (1 + $interest), $term))));

    return $payment;
}