<?php


// Remove the <UL> container from navigations
function kina_nav_menu_args( $args = '' ) {
	$args['container'] = false;
	$args['items_wrap'] = '%3$s';
	$args['walker'] = new kina_minor_navigation_walker();
	
	return $args;
}
add_filter( 'wp_nav_menu_args', 'kina_nav_menu_args' );

class kina_minor_navigation_walker extends Walker_Nav_Menu {
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output )
    {
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<nav class=\"navigation\">\n";
	}

	//add the description to the menu item output
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $post;

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		// Remove all the classes we don't want.
		for($i = count($classes) - 1; $i >= 0; $i--) {
			if (stripos($classes[$i], 'menu-item') !== false || stripos($classes[$i], 'page-item') !== false) {
				unset($classes[$i]);
			}
		}
		$classes[] = 'navigation__item';

		if ($post->ID == $item->object_id) { $classes[] = 'active'; }
		if ($args->has_children) { $classes[] = 'has-children'; }

		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		$class_names = join( ' ', $classes );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
		
		$output .= $indent . '<div' . $class_names .'>';
 
 
		$atts = array();
		$atts['class']  = 'navigation__item--link';
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		/** This filter is documented in wp-includes/post-template.php */
		$title = apply_filters( 'the_title', $item->title, $item->ID );
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . $title . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</div>\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</nav>\n";
	}
}

// Add Your Menu Locations
function kina_navigation_register_nav_menus() {
	register_nav_menus(
		array(  
			'social_navigation' => __( 'Social Navigation' ),
		)
	);
}
add_action( 'init', 'kina_navigation_register_nav_menus' );
