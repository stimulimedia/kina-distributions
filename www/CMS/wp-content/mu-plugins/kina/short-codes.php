<?php

// [kina_lead_form foo="foo-value"]
function add_shortcode_kina_lead_form( $atts ) {
	$atts = shortcode_atts(
		array(
			'description' => "In less than 5 minutes, find out how many hundreds of thousands of dollars you can save by using the LTD® in your next high rise construction project?",
			'language' => "en"
		), $atts );

	$output = '<kina-root language="' .$atts['language']. '"></kina-root>';

	$version = '?v=1.1.1';
	$folder = '/wp-content/mu-plugins/kina/kina-funnel/';
	$front = '<script type="text/javascript" src="' . $folder;
	$back = $version. '"></script>';

	$scripts = array();
	$files = glob(__dir__.'/kina-funnel/*.{js,css}', GLOB_BRACE);
	foreach($files as $file) {
		$filename = explode('/', $file);
		array_push($scripts, end($filename));
	}

	$scripts = array('runtime.js', 'es2015-polyfills.js', 'polyfills.js', 'styles.js', 'vendor.js', 'main.js');

	foreach($scripts as $script) {
		$output .= $front . $script . $back;
	}

	// $output .= '<script type="text/javascript" src="runtime.js"></script><script type="text/javascript" src="es2015-polyfills.js" nomodule></script><script type="text/javascript" src="polyfills.js"></script><script type="text/javascript" src="styles.js"></script><script type="text/javascript" src="vendor.js"></script><script type="text/javascript" src="main.js"></script>';

	return $output;
}
add_shortcode( 'kina_lead_form', 'add_shortcode_kina_lead_form' );