<?php
/**
 * Plugin Name: KINA Distributions Features
 * Description: This Plugin loads all the features necessary for the MUFG CMS to function
 * Author: KINA Distributions
 */


include('kina/navigation.php');
include('kina/post-types.php');
include('kina/custom-fields/custom-fields.php');
include('kina/end-points/end-points.php');
include('kina/short-codes.php');
include('kina/functions.php');
