const { watch, series, parallel, src, dest } = require('gulp');
const del = require('del');


const cleanFromSite = function() {
	return del(['../CMS/wp-content/mu-plugins/kina/kina-p2p/*.*'], { force: true });
}

const copyToSite = function(cb) {
	return src('kina-p2p/dist/kina-p2p/*.*')
    	.pipe(dest('../CMS/wp-content/mu-plugins/kina/kina-p2p0/'));
}

exports.build = series(
	cleanFromSite,
	copyToSite
);