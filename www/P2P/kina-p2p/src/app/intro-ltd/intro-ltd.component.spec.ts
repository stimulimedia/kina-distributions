import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroLtdComponent } from './intro-ltd.component';

describe('IntroLtdComponent', () => {
  let component: IntroLtdComponent;
  let fixture: ComponentFixture<IntroLtdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroLtdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroLtdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
