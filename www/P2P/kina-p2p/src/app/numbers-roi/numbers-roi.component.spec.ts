import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumbersRoiComponent } from './numbers-roi.component';

describe('NumbersRoiComponent', () => {
  let component: NumbersRoiComponent;
  let fixture: ComponentFixture<NumbersRoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumbersRoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumbersRoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
