import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'pricing',
	templateUrl: './pricing.component.html',
	styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {
	constructor() { }

	ngOnInit() {
		for (let {item, index} of this.items.map((item, index) => ({ item, index }))) {
			this.qty[index] = item.qty;
		}
		this.onUpdate();
	}

	@Input()
	savings = 0;
	pricing = 0;
	payoffProjects = 0;

	qty = {};
	includeRoofMount = true;
	roofMountPrice = 18375;

	items = [
		{
			'id': 1,
			'name': 'LTD® Series Hoist',
			'image': '/assets/images/ltd/hoist-w-roof-mount.jpg',
			'min': 1,
			'max': 10,
			'price': (187234 + 24150), // Hoist + Power Station
			'qty': 1
		},
		{
			'id': 2,
			'name': 'LTD® Series Compatible Deck',
			'image': '/assets/images/ltd/ltd-decks.jpg',
			'min': 1,
			'max': 50,
			'price': 67125,
			'qty': 7
		},
		{
			'id': 3,
			'name': 'LTD® Series Piping and Rebar Rack',
			'image': '/assets/images/ltd/piping-rack.png',
			'min': 1,
			'max': 10,
			'price': 13125,
			'qty': 1
		},
		{
			'id': 4,
			'name': 'LTD® Series Drywall Rack',
			'image': '/assets/images/ltd/drywall-rack.png',
			'min': 1,
			'max': 10,
			'price': 13125,
			'qty': 1
		}
	];


	onUpdate() {
		this.pricing = 0;

		for (let {item, index} of this.items.map((item, index) => ({ item, index }))) {
			item.qty = this.qty[index];
			this.pricing += (item.price * item.qty);
		}

		if (this.includeRoofMount) {
			this.pricing += (this.roofMountPrice * this.items[0].qty);
		}

		this.payoffProjects = Math.ceil(this.pricing / this.savings);
	}
}
