import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'purchase-intent',
  templateUrl: './purchase-intent.component.html',
  styleUrls: ['./purchase-intent.component.scss']
})
export class PurchaseIntentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  requestFinancing = false;

}
