import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseIntentComponent } from './purchase-intent.component';

describe('PurchaseIntentComponent', () => {
  let component: PurchaseIntentComponent;
  let fixture: ComponentFixture<PurchaseIntentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseIntentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseIntentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
