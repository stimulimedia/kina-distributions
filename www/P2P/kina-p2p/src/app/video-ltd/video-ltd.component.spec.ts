import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoLtdComponent } from './video-ltd.component';

describe('VideoLtdComponent', () => {
  let component: VideoLtdComponent;
  let fixture: ComponentFixture<VideoLtdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoLtdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoLtdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
