import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroKinaComponent } from './intro-kina.component';

describe('IntroKinaComponent', () => {
  let component: IntroKinaComponent;
  let fixture: ComponentFixture<IntroKinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroKinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroKinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
