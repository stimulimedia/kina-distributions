import * as Hammer from 'hammerjs';
import { Expo } from 'gsap/all';

declare var TweenMax: any;

export class kinap2p {
	constructor() {
		this.init();
	}

	mainSection = null;
	sections = null;
	active = [0, 0];

	init() {
		this.mainSection = document.getElementsByClassName('kina-p2p__main')[0];

		this.bindSwipeEvents();
		this.positionScreens();
	}

		bindSwipeEvents() {
			// Bind swipe events to the container
			var h = new Hammer(document.getElementsByClassName('kina-p2p')[0]);
			h.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
			h
				.on('swipe', this.onSwipe)
				.on('swipeleft', this.onSwipeLeft.bind(this))
				.on('swiperight', this.onSwipeRight.bind(this))
				.on('swipeup', this.onSwipeUp.bind(this))
				.on('swipedown', this.onSwipeDown.bind(this))
			;
		}

		positionScreens() {
			this.sections = [];

			// Grab all the screens
			for (let mod of [].slice.call(document.getElementsByClassName('main-section'))) {
				// Get the data element tha describes what position it's in.
				var grid = mod.getAttribute('data-grid');
				if (grid) {
					grid = grid.split(',');
					mod.setAttribute('style', 'transform: translate3D(' +(100 * grid[0]) + '%, ' + (100 * grid[1]) + '%, 0);');

					if (!this.sections[grid[0]]) { this.sections[grid[0]] = []; }
					this.sections[grid[0]][grid[1]] = mod;
				}
			}

			console.log(this.sections);
		}

	// This is for diagonal swipe from top-right only. That will bring the notes out
	onSwipe(event) {
		console.log('onSwipe', event);

		// Determine if the swipe is at a diagnal from top right at a 45 degree angle of close to it.

	}

	onSwipeLeft(event) {
		this.getSection(this.active[0] + 1, 0);
		this.updatePosition();
	}

	onSwipeRight(event) {
		this.getSection(this.active[0] - 1, 0);
		this.updatePosition();
	}

	onSwipeUp(event) {
		this.getSection(this.active[0], this.active[1] + 1);
		this.updatePosition();
	}

	onSwipeDown(event) {
		this.getSection(this.active[0], this.active[1] - 1);
		this.updatePosition();
	}

		getSection(col, row) {
			var section = null;

			if (!this.sections[col]) {
				return;
			} else {
				section = this.sections[col][row];
			}

			if (section) {
				this.active = [col, row];
			}
		}

		updatePosition() {
			TweenMax.to(this.mainSection, .4, {
				xPercent: -(100 * this.active[0]),
				yPercent: -(100 * this.active[1]),
				ease: Expo.easeInOut
			});
		}
}
