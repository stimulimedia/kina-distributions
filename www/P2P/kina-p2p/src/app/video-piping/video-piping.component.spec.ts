import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPipingComponent } from './video-piping.component';

describe('VideoPipingComponent', () => {
  let component: VideoPipingComponent;
  let fixture: ComponentFixture<VideoPipingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPipingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPipingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
