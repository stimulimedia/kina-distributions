import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VideoLtdComponent } from './video-ltd/video-ltd.component';
import { VideoPipingComponent } from './video-piping/video-piping.component';
import { IntroKinaComponent } from './intro-kina/intro-kina.component';
import { IntroLtdComponent } from './intro-ltd/intro-ltd.component';
import { NumbersSavingsComponent } from './numbers-savings/numbers-savings.component';
import { NumbersRoiComponent } from './numbers-roi/numbers-roi.component';
import { PricingComponent } from './pricing/pricing.component';
import { PurchaseIntentComponent } from './purchase-intent/purchase-intent.component';
import { NotesComponent } from './notes/notes.component';

@NgModule({
  declarations: [
    AppComponent,
    VideoLtdComponent,
    VideoPipingComponent,
    IntroKinaComponent,
    IntroLtdComponent,
    NumbersSavingsComponent,
    NumbersRoiComponent,
    PricingComponent,
    PurchaseIntentComponent,
    NotesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
