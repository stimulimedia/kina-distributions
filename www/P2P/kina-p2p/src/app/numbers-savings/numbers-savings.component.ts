import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'numbers-savings',
	templateUrl: './numbers-savings.component.html',
	styleUrls: ['./numbers-savings.component.scss']
})
export class NumbersSavingsComponent implements OnInit {
	constructor() { }

	ngOnInit() {
		for (let {field, index} of this.fields.map((field, index) => ({ field, index }))) {
			this.values[index] = field.default;
		}
		this.onUpdate();
	}

	savings = 0;
	values = {};

	fields = [
		{
			'id': 'budget',
			'label': 'Project Budget',
			'type': 'number',
			'min': 0,
			'max': 50,
			'step': .1,
			'prefix': '$',
			'suffix': 'M',
			'default': 1
		},
		{
			'id': 'crane-costs',
			'label': 'Crane Monthly Cost',
			'type': 'number',
			'min': 0,
			'max': 30,
			'step': .1,
			'prefix': '$',
			'suffix': 'K',
			'default': 1
		},
		{
			'id': 'project-floors',
			'label': 'Project Floors',
			'type': 'number',
			'min': 0,
			'max': 50,
			'step': 1,
			'prefix': null,
			'suffix': 'Floors',
			'default': 12
		},
		{
			'id': 'project-months',
			'label': 'Project Months',
			'type': 'number',
			'min': 0,
			'max': 50,
			'step': 1,
			'prefix': null,
			'suffix': 'Months',
			'default': 24
		},
		{
			'id': 'crane-picks',
			'label': 'Crane Picks/Day',
			'type': 'number',
			'min': 0,
			'max': 50,
			'step': 1,
			'prefix': null,
			'suffix': null,
			'default': 10
		}
	];

	onUpdate() {
		this.savings = 0;

		var savingsValues = {};

		for (let {field, index} of this.fields.map((field, index) => ({ field, index }))) {
			savingsValues[field.id] = this.values[index];
		}

		// var calc = new savingsCalculator(savingsValues);
		// this.savings = calc.savings;
	}
}
