import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumbersSavingsComponent } from './numbers-savings.component';

describe('NumbersSavingsComponent', () => {
  let component: NumbersSavingsComponent;
  let fixture: ComponentFixture<NumbersSavingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumbersSavingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumbersSavingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
