import { Component, AfterViewInit } from '@angular/core';
import { kinap2p } from './kina-p2p';

@Component({
	selector: 'kina-p2p',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent implements AfterViewInit {
	title = 'kina-p2p';
	p2p = null;

	activeScreen = 0;
	savings = 250000;

	constructor() {
	}

	ngAfterViewInit():void {
		this.initApp();
		this.p2p = new kinap2p();
	}

	initApp() {

	}
}
