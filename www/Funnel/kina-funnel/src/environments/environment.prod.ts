export const environment = {
	production: true,
	api: 'http://kinadistributions.com/wp-json/kina/v1',
	lang: 'en'
};
