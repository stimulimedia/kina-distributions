import { Component, Input, OnInit } from '@angular/core';

import { FunnelService } from './../funnel.service';
import { Config } from './../config';
import { environment } from './../../environments/environment';

@Component({
	selector: 'funnel',
	templateUrl: './funnel.component.html',
	styleUrls: ['./funnel.component.scss']
})
export class FunnelComponent implements OnInit {

	// *******************************************************************
	// Initialization
	// *******************************************************************
	
	constructor(private funnelService:FunnelService) { }

	ngOnInit() {
		if (!this.language) { this.language = environment.lang; }
		this.config = Config.SETTINGS[this.language];

		let questions = this.funnelService.getQuestions('root', this.language);
		this.questionGroups['root'] = questions;

		this.nextButton = this.config.nextButton.next;
	}

	@Input()
	language;

	// This allows us to iterate over object keys in the view
	objectKeys = Object.keys;


	// *******************************************************************
	// Properties
	// *******************************************************************

	config = null;
	prospect = null;
	answers = null;
	questionGroups = {};
	nextButton = '';
	thankYou = null;
	finish = null;

	response = {};


	// *******************************************************************
	// Methods
	// *******************************************************************

	onQuestionAnswered(response) {
		if (response.question.field) {
			this.response[response.question.field] = response.answer;
		}

		switch(response.question.type) {
			case 'selection':
				for (let answer in response.question.answers) {
					if (response.question.answers[answer].value === response.answer) {
						this.processActions(response.question, response.question.answers[answer], response.question.answers[answer].actions);
						return;
					}
				}
			break;
			default:
				this.processActions(response.question, response.answer, response.question.actions);
			break;
		}
	}

		processActions(question, answer, actions) {
			//console.log('processAction', question, answer, actions);

			for(let action in actions) {
				let a = actions[action].split(':');
				switch(a[0]) {
					case 'store':
						this.storeOption(question, answer);
					break;
					case 'label':
						this.labelOption(question, answer, a[1]);
					break;
					case 'set':
						this.setProperty(question, answer, a);
					break;
					case 'setNext':
						this.setNext(a[1]);
					break;
					case 'multiply':
						this.multiply(question, answer, a);
					break;
					case 'forward_to_decision_maker':
						this.forwardToDecisionMaker();
					break;
					case 'thank_you':
						this.showThankYou(a[1]);
					break;
					case 'add':
						this.addQuestionSet(question, a[1]);
					break
				}
			}
		}

			storeOption(question, answer) {
				console.log('storeOption', this.prospect, this.response);

				// TODO: Call the createContact service method
				this.funnelService.updateProspect(this.prospect, this.response).subscribe((res)=>{
					console.log("Updated a prospect");
				});
			}

			labelOption(question, answer, label) {
				// TODO: Add labels to the prospect profile. This is comma delimited.
				//console.log('labelOption', question, answer);
			}

			setProperty(question, answer, property) {
				this.response[property[1]] = property[2];
			}

			setNext(next) {
				// NOTE: I decided to not do this, because it's confusing if the
				// user clicks "Finish", then there's another step after. If this
				// works out, I'll come back in and remove all this.
				//this.nextButton = this.config.nextButton[next];
			}

			multiply(question, answer, property) {
				if (!this.response[property[2]] || !this.response[property[3]]) {
					return;
				}
				this.response[property[1]] = this.response[property[2]] * this.response[property[3]];
			}

			forwardToDecisionMaker() {
				console.log('Displaying forward to decision maker');
			}

			showThankYou(thankYou) {
				this.thankYou = this.config.thankYou[thankYou];
				console.log(this.thankYou);
			}

			addQuestionSet(originalQuestion, root) {
				// Check the original question for the root and see if the
				// user went back and answered a question they already answered.
				// If they did, we have to remove any questions added after
				// that and then we can add the new question set.
				this.questionGroups = this.findQuestionByKey(originalQuestion.key);

				var roots = root.split(',');
				for (let index in roots) {
					var root = roots[index];
					var questions = this.funnelService.getQuestions(root, this.language);

					// Replace any variables in the questin labels
					// TODO: Move this to its own method
					for (let question in questions) {
						var regex = /{{([a-zA-Z0-9]*)}}/g,
							matches = questions[question].label.match(regex);

						for (let match in matches) {
							// TODO: Add support for any propery.
							questions[question].label = questions[question].label
								.replace(/{{ProjectEquipment}}/g, this.response['ProjectEquipment'])
							;
						}

					}

					this.questionGroups[root] = questions;
				}
			}

				findQuestionByKey(key) {
					let questionGroups = {};

					for (let root in this.questionGroups) {
						questionGroups[root] = this.questionGroups[root];

						for (let index in this.questionGroups[root]) {
							if (this.questionGroups[root][index].key === key) {
								return questionGroups;
							}
						}
					}
					return false;
				}


	// This is the initial contact creation handler
	onProspectSubmitted(prospect) {
		this.prospect = prospect;

		// TODO: Call the createContact service method
		this.funnelService.createProspect(this.prospect).subscribe((res)=>{
			console.log("Created a prospect");
		});
	}


	// This button doesn't really do anything. It just serves as
	// bait to make the user blur any text field that might be at
	// the end of a question group. LOL
	onNextClick() {
		console.log('Thanks for clicking next! :)');

		// TODO: Log an event to GA
	}


	// This is the final submission button
	onSubmitted() {
		// TODO: Do the final save
		console.log('submitted', this.prospect, this.response);

		// Show the finished message
		this.finish = this.config.finish;
	}
}
