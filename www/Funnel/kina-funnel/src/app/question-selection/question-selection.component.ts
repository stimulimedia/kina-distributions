import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
	selector: 'question-selection',
	templateUrl: './question-selection.component.html',
	styleUrls: ['./question-selection.component.scss']
})
export class QuestionSelectionComponent implements OnInit {
	constructor() { }

	ngOnInit() {

	}

	@Output('question-answered') questionAnswered = new EventEmitter();

	@Input()
	question = null;

	answer = '';

	onAnswer(field) {
		if (!field.invalid) {
			this.questionAnswered.emit({ 'question': this.question, 'answer': this.answer });
		}
	}
}
