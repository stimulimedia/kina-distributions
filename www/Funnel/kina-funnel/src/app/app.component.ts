import { Component, ElementRef } from '@angular/core';

@Component({
	selector: 'kina-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	constructor(private el:ElementRef) {
		if (el.nativeElement.getAttribute('language')) {
			this.language = el.nativeElement.getAttribute('language');
		}
	}

	language:String = 'en';
}
