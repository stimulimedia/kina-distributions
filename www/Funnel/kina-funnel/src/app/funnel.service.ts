import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Config } from './config';
import { environment } from './../environments/environment';

@Injectable({
	providedIn: 'root'
})

export class FunnelService {
	constructor(private httpClient: HttpClient) {

	}

	getQuestions(root, language) {
		console.log(language);
		return Config.SETTINGS[language].questions[root];
	}

	createProspect(prospect) {
		return this.httpClient.post(`${environment.api}/prospect/create/`, prospect);
	}

	updateProspect(prospect, response) {
		return this.httpClient.put(`${environment.api}/prospect/update/`, { 'prospect': prospect, 'response': response});
	}
}
