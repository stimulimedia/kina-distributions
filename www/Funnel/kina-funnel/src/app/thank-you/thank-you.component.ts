import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
	selector: 'thank-you',
	templateUrl: './thank-you.component.html',
	styleUrls: ['./thank-you.component.scss']
})
export class ThankYouComponent implements OnInit {
	constructor() { }

	ngOnInit() {

	}

	@Output('submitted') submitted = new EventEmitter();

	@Input()
	thankYou = null;

	onClick = function() {
		this.submitted.emit();
	}
}
