import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Config } from './../config';
import { Prospect } from './../prospect';
import { environment } from './../../environments/environment';

@Component({
	selector: 'customer-info',
	templateUrl: './customer-info.component.html',
	styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent implements OnInit {
	constructor() { }

	ngOnInit() {
		this.config = Config.SETTINGS[this.language];
		this.labels = this.config.customerInfo.fields;
	}

	@Output('submitted')
	submitted = new EventEmitter();

	@Input()
	language:string = null;

	@Input()
	key:string = null;

	@Input()
	title:string = null;

	@Input()
	buttonText:string = null;

	@Input()
	successText:string = null;

	config = null;
	labels = null;
	prospect:Prospect = new Prospect();
	isSubmitted = false;

	isValidPhoneNumber(phone) {
		// const PHONE_REGEXP = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
		// phone = phone.match(/\d+/g).map(Number);
		// return PHONE_REGEXP.test(phone);

		// TODO: I have to come in and validate phone number, but it needs to
		// support Mexico and US at the moment.
		return true;
	}

	isValidEmailAddress(email) {
		const REGEXP = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
		return REGEXP.test(email);
	}

	isFormValid() {
		return this.prospect.CompanyName &&
			this.prospect.FirstName &&
			this.prospect.LastName &&
			this.prospect.PhoneNumber &&
				this.isValidPhoneNumber(this.prospect.PhoneNumber) &&
			this.prospect.EmailAddress &&
				this.isValidEmailAddress(this.prospect.EmailAddress);
	}

	onSubmit() {
		if (this.isFormValid()) {
			this.isSubmitted = true;
			this.submitted.emit({ 'prospect': this.prospect });
		}
	}
}
