import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'finished',
  templateUrl: './finished.component.html',
  styleUrls: ['./finished.component.scss']
})
export class FinishedComponent implements OnInit {
	constructor() { }

	ngOnInit() {

	}

	@Input()
	finish = null;
}
