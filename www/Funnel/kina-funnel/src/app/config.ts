export class Config {
	static SETTINGS = {
		'en': {
			'title': 'KINA Distributions LTD® Series',
			'introduction': 'How would you feel if the LTD<sup>®</sup> could save you <strong>over $100k</strong> on your high rise construction project?',
			'subIntroduction': 'Find out in less than 5 minutes.',
			'instructions': 'Before we get started, tell us who you are.',

			'customerInfo': {
				'fields': {
					'company': 'Company Name',
					'firstName': 'First Name',
					'lastName': 'Last Name',
					'phoneNumber': 'Phone Number',
					'emailAddress': 'Email Address'
				},
				'buttonText': 'Let\'s Get Started!',
				'successText': 'Awesome! Let\'s save you some money!'
			},
			'nextButton': {
				'next': 'Next',
				'finish': 'Finish'
			},
			'thankYou': {
				'mailOnly': {
					'body': 'Awesome! Thank you for your interest in the LTD<sup>®</sup> Series. Please click below to be sent our detailed spec and receive information, offers, events, and more.',
					'buttonText': 'Click to subscribe and request info',
				},
				'renter': {
					'body': 'Thank you for your interest in the LTD<sup>®</sup> Series! Please click below to receive your results and receive information, offers, events, and more.',
					'buttonText': 'Click to subscribe and receive your results'
				},
				'buyer': {
					'body': 'Thank you for your interest in the LTD<sup>®</sup> Series! Please click below to receive your results and receive information, offers, events, and more.',
					'buttonText': 'Click to subscribe and receive your results'
				}
			},
			'finish': {
				'title': 'Thank you for your time!',
				'subtitle': 'We will contact you shortly.'
			},
			'forwardInfo': {
				'title': 'Great! We\'ll let them know. Enter their info below.',
				'fields': {
					'company': 'Company Name',
					'firstName': 'First Name',
					'lastName': 'Last Name',
					'phoneNumber': 'Phone Number',
					'emailAddress': 'Email Address'
				},
				'buttonText': 'Send!'
			},

			'questions': {
				'root': [
					{
						'key': 'urgency',
						'type': 'selection',
						'style': '100',
						'label': 'How soon will your high rise construction project start?',
						'field': 'ProjectUrgency',
						'answers': [
							{
								'answer': 'Within 1 year',
								'value': '1 year',
								'actions': ['label:BuyerInHeat', 'add:construction_project']
							},
							{
								'answer': 'Between 1 to 3 years',
								'value': '1-3 years',
								'actions': ['label:BuyerInPower', 'add:construction_project']
							},
							{
								'answer': 'More than 3 years',
								'value': '3+ years',
								'actions': ['label:PotentialBuyerInPower', 'add:construction_project']
							},
							{
								'answer': 'I do not have a project at this time',
								'value': 'None',
								'actions': ['add:no_construction_project']
							}
						]
					}
				],
				'construction_project': [
					{
						'key': 'city',
						'type': 'text',
						'style': '100',
						'label': 'In what city is your high rise construction project site?',
						'field': 'ProjectCity',
						'prepend': '',
						'append': '',
						'actions': []
					},
					{
						'key': 'months',
						'type': 'text',
						'style': '25',
						'label': 'How long will it take to complete your project?',
						'field': 'ProjectMonths',
						'prepend': '',
						'append': 'months',
						'validator': { 'format': 'number', 'min': 1, 'max': 120 },
						'actions': []
					},
					{
						'key': 'floors',
						'type': 'text',
						'style': '25',
						'label': 'How many floors will your project have?',
						'field': 'ProjectFloors',
						'prepend': '',
						'append': 'floors',
						'validator': { 'format': 'number', 'min': 0, 'max': 200 },
						'actions': ['add:high_rise_project']
					}
				],
				'high_rise_project': [
					{
						'key': 'budget',
						'type': 'text',
						'style': '25',
						'label': 'In order to best estimate your savings, please tell us the total estimated project budget in millions (eg. $1.2 Million)',
						'prepend': '$',
						'append': 'Million',
						'field': 'ProjectBudget',
						'validator': { 'format': 'currency', 'min': 1, 'max': 200, 'step': .1 },
						'actions': []
					},
					{
						'key': 'equipmentPurchase',
						'type': 'selection',
						'style': '100',
						'label': 'Are you planning to purchase or rent a tower crane?',
						'field': 'ProjectEquipmentPurchase',
						'answers': [
							{
								'answer': 'Purchase',
								'value': 'purchase',
								'actions': ['store', 'set:ProjectEquipment:tower crane', 'add:equipment_purchase,high_rise_remaining','setNext:finish']
							},
							{
								'answer': 'Rent',
								'value': 'rent',
								'actions': ['store', 'set:ProjectEquipment:tower crane', 'add:equipment_rent,high_rise_remaining','setNext:finish']
							},
							{
								'answer': 'I don\'t need a tower crane',
								'value': 'none',
								'actions': ['store', 'add:different_equipment']
							}
						]
					}
				],
				'equipment_purchase': [
					{
						'key': 'equipmentPurchasePrice',
						'type': 'text',
						'style': '25',
						'label': 'How much will the {{ProjectEquipment}} cost?',
						'prepend': '$',
						'append': '',
						'field': 'ProjectEquipmentPurchasePrice',
						'validator': { 'format': 'currency', 'min': 0, 'max': 1000000 },
						'actions': []
					}
				],
				'equipment_rent': [
					{
						'key': 'equipmentMonthlyCost',
						'type': 'text',
						'style': '25',
						'label': 'How much will the {{ProjectEquipment}} cost on a monthly basis?',
						'prepend': '$',
						'append': 'per month',
						'field': 'ProjectEquipmentMonthlyCost',
						'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
						'actions': []
					}
				],
				'different_equipment': [
					{
						'key': 'equipment',
						'type': 'text',
						'style': '100',
						'label': 'What equipment do you plan to use to move materials between floors?',
						'prepend': '',
						'append': '',
						'field': 'ProjectEquipment',
						'actions': ['add:different_equipment_cost,high_rise_remaining','setNext:finish']
					}
				],
				'different_equipment_cost': [
					{
						'key': 'equipmentPurchasePrice',
						'type': 'text',
						'style': '25',
						'label': 'How much will the {{ProjectEquipment}} cost on a monthly basis?',
						'prepend': '$',
						'append': 'per month',
						'field': 'ProjectEquipmentPurchasePrice',
						'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
						'actions': []
					}
				],
				'high_rise_remaining': [
					{
						'key': 'picksPerDay',
						'type': 'text',
						'style': '25',
						'label': 'How many loads of materials are you realistically planning to move per day with the {{ProjectEquipment}}?',
						'field': 'ProjectPicksPerDay',
						'prepend': '',
						'append': 'loads of material',
						'validator': { 'format': 'number', 'min': 0, 'max': 500 },
						'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
					},
					{
						'key': 'picksPerDayNeeded',
						'type': 'selection',
						'style': '25',
						'label': 'How many times more loads of materials would <em>you like</em> to move per day?',
						'prepend': '',
						'append': 'more loads',
						'field': 'ProjectPicksPerDayNeedMultiplier',
						'answers': [
							{
								'answer': '2 times',
								'value': '2',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '3 times',
								'value': '3',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '4 times',
								'value': '4',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '5 times',
								'value': '5',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '6 times',
								'value': '6',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '7 times',
								'value': '7',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '8 times',
								'value': '8',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '9 times',
								'value': '9',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '10 times',
								'value': '10',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							}
						]
					},
					{
						'key': 'concurrentWorkFloors',
						'type': 'text',
						'style': '25',
						'label': 'On how many floors could you potentially have work being done at the same time?',
						'prepend': '',
						'append': 'floors',
						'field': 'ProjectConcurrentWorkFloors',
						'validator': { 'format': 'number', 'min': 0, 'max': 50 },
						'actions': []
					},
					{
						'key': 'projectsPerYear',
						'type': 'text',
						'style': '25',
						'label': 'How many projects do you do per year on average?',
						'prepend': '',
						'append': 'projects per year',
						'field': 'ProjectsPerYear',
						'validator': { 'format': 'number', 'min': 1, 'max': 50 },
						'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
					}
				],
				'decision_maker': [
					{
						'key': 'decisionMaker',
						'type': 'selection',
						'style': '100',
						'label': 'For your convienience, should we forward the results to someone who has final say on the project plan?',
						'answers': [
							{
								'answer': 'No thanks!',
								'value': 'No',
								'actions': ['store', 'thank_you:buyer']
							},
							{
								'answer': 'It\'s me',
								'value': 'Me',
								'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
							},
							{
								'answer': 'Yes please!',
								'value': 'Yes',
								'actions': ['store', 'label:Influencer', 'forward_to_decision_maker']
							}
						]
					}
				],


				'no_construction_project': [
					{
						'key': 'whatYouDo',
						'type': 'selection',
						'style': '100',
						'label': 'Please tell us more about yourself. What do you do?',
						'field': 'AboutProspect',
						'answers': [
							{
								'answer': 'I rent construction equipment',
								'value': 'None',
								'actions': ['store', 'label:RentalCompany,DecisionMaker', 'thank_you:renter']
							},
							{
								'answer': 'I am a commercial real estate developer',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							},
							{
								'answer': 'I am an architect',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							},
							{
								'answer': 'I am a civil (or other) engineer',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							},
							{
								'answer': 'Other',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							}
						]
					}
				],


				'equipment_renter': [
					{
						'type': 'selection',
						'style': '100',
						'label': 'Test question?',
						'answers': [
							{
								'answer': 'Test answer',
								'value': 'None',
								'actions': ['store', 'equipment_renter']
							}
						]
					}
				]
			}
		},

		'es': {
			'title': 'KINA Distributions LTD® Series',
			'introduction': '¿Qué opinarías de que el LTD<sup>®</sup> te pudiera ahorrar <strong>más de $100k USD</strong> en tu proyecto de construcción de torres?',
			'subIntroduction': 'Averígualo en menos  de 5 minutos.',
			'instructions': 'Antes de empezar, platícanos sobre ti.',

			'customerInfo': {
				'fields': {
					'company': 'Nombre de la Compañía ',
					'firstName': 'Nombre (s)',
					'lastName': 'Apellidos',
					'phoneNumber': 'Teléfono',
					'emailAddress': 'Dirección Email'
				},
				'buttonText': '¡Comencemos!',
				'successText': '¡Genial! ¡Vamos a ahorrarte dinero!'
			},
			'nextButton': {
				'next': 'Siguiente',
				'finish': 'Terminar'
			},
			'thankYou': {
				'mailOnly': {
					'body': '¡Genial! Gracias por tu interés en la Serie LTD<sup>®</sup>. Favor de dar click abajo para enviarte las especificaciones y recibir información, ofertas, eventos y más.',
					'buttonText': 'Suscribirme y solicitar info',
				},
				'renter': {
					'body': '¡Gracias por tu interés en la Serie  LTD<sup>®</sup>! Favor de dar click abajo para recibir tus resultados y recibir información, ofertas, eventos y más.',
					'buttonText': 'Suscríbete y recibe tus resultados'
				},
				'buyer': {
					'body': '¡Gracias por tu interés en la Serie  LTD<sup>®</sup>! Favor de dar click abajo para recibir tus resultados y recibir información, ofertas, eventos y más.',
					'buttonText': 'Suscríbete y recibe tus resultados'
				}
			},
			'finish': {
				'title': '¡Gracias por tu tiempo!',
				'subtitle': 'Lo contactaremos pronto.'
			},
			'forwardInfo': {
				'title': 'Great! We\'ll let them know. Enter their info below.',
				'fields': {
					'company': 'Company Name',
					'firstName': 'First Name',
					'lastName': 'Last Name',
					'phoneNumber': 'Phone Number',
					'emailAddress': 'Email Address'
				},
				'buttonText': 'Send!'
			},

			'questions': {
				'root': [
					{
						'key': 'urgency',
						'type': 'selection',
						'style': '100',
						'label': '¿Qué tan pronto comenzará tu proyecto de construcción de torres?',
						'field': 'ProjectUrgency',
						'answers': [
							{
								'answer': 'En los siguientes 12 meses',
								'value': '1 year',
								'actions': ['label:BuyerInHeat', 'add:construction_project']
							},
							{
								'answer': 'Entre 1 y 3 años',
								'value': '1-3 years',
								'actions': ['label:BuyerInPower', 'add:construction_project']
							},
							{
								'answer': 'Más de 3 años',
								'value': '3+ years',
								'actions': ['label:PotentialBuyerInPower', 'add:construction_project']
							},
							{
								'answer': 'No cuento con un proyecto en este momento',
								'value': 'None',
								'actions': ['add:no_construction_project']
							}
						]
					}
				],
				'construction_project': [
					{
						'key': 'city',
						'type': 'text',
						'style': '100',
						'label': '¿En qué ciudad vas a desarrollar tu proyecto de construccion de torres?',
						'field': 'ProjectCity',
						'prepend': '',
						'append': '',
						'actions': []
					},
					{
						'key': 'months',
						'type': 'text',
						'style': '25',
						'label': '¿Cuánto tiempo está programado que se tomará tu proyecto de construcción de torres para tenerlo finalizado?',
						'field': 'ProjectMonths',
						'prepend': '',
						'append': 'meses',
						'validator': { 'format': 'number', 'min': 1, 'max': 120 },
						'actions': []
					},
					{
						'key': 'floors',
						'type': 'text',
						'style': '25',
						'label': '¿Cuántos pisos tendrá tu proyecto?',
						'field': 'ProjectFloors',
						'prepend': '',
						'append': 'pisos',
						'validator': { 'format': 'number', 'min': 0, 'max': 200 },
						'actions': ['add:high_rise_project']
					}
				],
				'high_rise_project': [
					{
						'key': 'budget',
						'type': 'text',
						'style': '25',
						'label': 'Con el objetivo de ser más precisos en lo que te puedes ahorrar, por favor compártenos el presupuesto total estimado de tu proyecto en millones de USD (por ejemplo. $1.2 Millones)',
						'prepend': '$',
						'append': 'Millones',
						'field': 'ProjectBudget',
						'validator': { 'format': 'currency', 'min': 1, 'max': 200, 'step': .1 },
						'actions': []
					},
					{
						'key': 'equipmentPurchase',
						'type': 'selection',
						'style': '100',
						'label': '¿Estás planeando comprar o rentar una grúa torre?',
						'field': 'ProjectEquipmentPurchase',
						'answers': [
							{
								'answer': 'Comprar',
								'value': 'purchase',
								'actions': ['store', 'set:ProjectEquipment:grúa torre', 'add:equipment_purchase,high_rise_remaining','setNext:finish']
							},
							{
								'answer': 'Rentar',
								'value': 'rent',
								'actions': ['store', 'set:ProjectEquipment:grúa torre', 'add:equipment_rent,high_rise_remaining','setNext:finish']
							},
							{
								'answer': 'No necesito una grúa torre',
								'value': 'none',
								'actions': ['store', 'add:different_equipment']
							}
						]
					}
				],
				'equipment_purchase': [
					{
						'key': 'equipmentPurchasePrice',
						'type': 'text',
						'style': '25',
						'label': '¿Cuánto  cuesta el {{ProjectEquipment}}?',
						'prepend': '$',
						'append': '',
						'field': 'ProjectEquipmentPurchasePrice',
						'validator': { 'format': 'currency', 'min': 0, 'max': 1000000 },
						'actions': []
					}
				],
				'equipment_rent': [
					{
						'key': 'equipmentMonthlyCost',
						'type': 'text',
						'style': '25',
						'label': '¿Cuánto costaría el {{ProjectEquipment}} mensualmente?',
						'prepend': '$',
						'append': 'por mes',
						'field': 'ProjectEquipmentMonthlyCost',
						'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
						'actions': []
					}
				],
				'different_equipment': [
					{
						'key': 'equipment',
						'type': 'text',
						'style': '100',
						'label': '¿Qué equipo planeas usar para mover los materiales entre pisos?',
						'prepend': '',
						'append': '',
						'field': 'ProjectEquipment',
						'actions': ['add:different_equipment_cost,high_rise_remaining','setNext:finish']
					}
				],
				'different_equipment_cost': [
					{
						'key': 'equipmentPurchasePrice',
						'type': 'text',
						'style': '25',
						'label': '¿Cuánto costaría el {{ProjectEquipment}} mensualmente?',
						'prepend': '$',
						'append': 'por mes',
						'field': 'ProjectEquipmentPurchasePrice',
						'validator': { 'format': 'currency', 'min': 0, 'max': 100000 },
						'actions': []
					}
				],
				'high_rise_remaining': [
					{
						'key': 'picksPerDay',
						'type': 'text',
						'style': '25',
						'label': '¿Cuántas cargas de materiales estás planeando mover (de forma realista) con el {{ProjectEquipment}}?',
						'field': 'ProjectPicksPerDay',
						'prepend': '',
						'append': 'cargas de materiales',
						'validator': { 'format': 'number', 'min': 0, 'max': 500 },
						'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
					},
					{
						'key': 'picksPerDayNeeded',
						'type': 'selection',
						'style': '25',
						'label': '¿Cuántas cargas de materiales más <em>te gustaría</em> mover por día?',
						'prepend': '',
						'append': 'mas cargas',
						'field': 'ProjectPicksPerDayNeedMultiplier',
						'answers': [
							{
								'answer': '2 veces',
								'value': '2',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '3 veces',
								'value': '3',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '4 veces',
								'value': '4',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '5 veces',
								'value': '5',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '6 veces',
								'value': '6',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '7 veces',
								'value': '7',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '8 veces',
								'value': '8',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '9 veces',
								'value': '9',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							},
							{
								'answer': '10 veces',
								'value': '10',
								'actions': ['multiply:ProjectPicksPerDayNeed:ProjectPicksPerDayNeedMultiplier:ProjectPicksPerDay']
							}
						]
					},
					{
						'key': 'concurrentWorkFloors',
						'type': 'text',
						'style': '25',
						'label': '¿En cuántos pisos podrías potencialmente estar haciendo trabajos al mismo tiempo?',
						'prepend': '',
						'append': 'pisos',
						'field': 'ProjectConcurrentWorkFloors',
						'validator': { 'format': 'number', 'min': 0, 'max': 50 },
						'actions': []
					},
					{
						'key': 'projectsPerYear',
						'type': 'text',
						'style': '25',
						'label': 'En promedio, ¿cuántos proyectos haces por año?',
						'prepend': '',
						'append': 'proyectos por año',
						'field': 'ProjectsPerYear',
						'validator': { 'format': 'number', 'min': 1, 'max': 50 },
						'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
					}
				],
				'decision_maker': [
					{
						'key': 'decisionMaker',
						'type': 'selection',
						'style': '100',
						'label': 'For your convienience, should we forward the results to someone who has final say on the project plan?',
						'answers': [
							{
								'answer': 'No thanks!',
								'value': 'No',
								'actions': ['store', 'thank_you:buyer']
							},
							{
								'answer': 'It\'s me',
								'value': 'Me',
								'actions': ['store', 'label:DecisionMaker', 'thank_you:buyer']
							},
							{
								'answer': 'Yes please!',
								'value': 'Yes',
								'actions': ['store', 'label:Influencer', 'forward_to_decision_maker']
							}
						]
					}
				],


				'no_construction_project': [
					{
						'key': 'whatYouDo',
						'type': 'selection',
						'style': '100',
						'label': 'Nos gustaría conocerte mejor, dinos por favor  ¿a qué te dedicas?',
						'field': 'AboutProspect',
						'answers': [
							{
								'answer': 'Rento a mis clientes equipo de construcción',
								'value': 'None',
								'actions': ['store', 'label:RentalCompany,DecisionMaker', 'thank_you:renter']
							},
							{
								'answer': 'Soy un desarrollador comercial de bienes raíces ',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							},
							{
								'answer': 'Soy un arquitecto',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							},
							{
								'answer': 'Soy un ingeniero civil (o de otro tipo)',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							},
							{
								'answer': 'Otro',
								'value': 'None',
								'actions': ['store', 'thank_you:mailOnly']
							}
						]
					}
				],


				'equipment_renter': [
					{
						'type': 'selection',
						'style': '100',
						'label': 'Test question?',
						'answers': [
							{
								'answer': 'Test answer',
								'value': 'None',
								'actions': ['store', 'equipment_renter']
							}
						]
					}
				]
			}
		}
	}
}
