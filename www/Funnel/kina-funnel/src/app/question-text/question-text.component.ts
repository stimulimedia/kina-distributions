import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'question-text',
	templateUrl: './question-text.component.html',
	styleUrls: ['./question-text.component.scss']
})
export class QuestionTextComponent implements OnInit, OnChanges {
	constructor() { }

	ngOnInit() {

	}

	ngOnChanges() {
		if (!this.question.validator) { return; }

		switch(this.question.validator.format) {
			case 'currency': case 'number':
				this.type = 'number';
			break;
		}
	}

	@Output('question-answered') questionAnswered = new EventEmitter();

	@Input()
	question = null;

	type = 'text';
	answer = '';

	onAnswer(field) {
		if (!field.invalid) {
			this.questionAnswered.emit({ 'question': this.question, 'answer': this.answer });
		}
	}
}
