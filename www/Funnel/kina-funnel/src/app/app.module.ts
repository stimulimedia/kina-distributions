import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FunnelComponent } from './funnel/funnel.component';
import { QuestionSelectionComponent } from './question-selection/question-selection.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { QuestionTextComponent } from './question-text/question-text.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { ForwardComponent } from './forward/forward.component';
import { FinishedComponent } from './finished/finished.component';

@NgModule({
  declarations: [
    AppComponent,
    FunnelComponent,
    QuestionSelectionComponent,
    CustomerInfoComponent,
    QuestionTextComponent,
    ThankYouComponent,
    ForwardComponent,
    FinishedComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
