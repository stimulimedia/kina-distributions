const { watch, series, parallel, src, dest } = require('gulp');
const del = require('del');


const cleanFromSite = function() {
	return del(['../CMS/wp-content/mu-plugins/kina/kina-funnel/*.*'], { force: true });
}

const copyToSite = function(cb) {
	return src('kina-funnel/dist/kina-funnel/*.*')
    	.pipe(dest('../CMS/wp-content/mu-plugins/kina/kina-funnel/'));
}

exports.build = series(
	cleanFromSite,
	copyToSite
);